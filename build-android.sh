#!/bin/sh

rm builds/HEATCITY.apk
cd blue-dragon-app
tns build android --release --key-store-path ../certificates/heatcity.keystore --key-store-password bluedragon --key-store-alias heatcity --key-store-alias-password bluedragon --copy-to ../builds/HEATCITY.apk