import { Model } from '@mean-expert/model';
/**
 * @module ParentOrg
 * @description
 * Write a useful ParentOrg Model description.
 * Register hooks and remote methods within the
 * Model Decorator
 **/
@Model({
  hooks: {
    beforeSave: { name: 'before save', type: 'operation' }
  },
  remotes: {
    myRemote: {
      returns : { arg: 'result', type: 'object' },
      http    : { path: '/my-remote', verb: 'get' }
    }
  }
})

class ParentOrg {
  // LoopBack model instance is injected in constructor
  constructor(public model: any) {}

  // Example Operation Hook
  beforeSave(ctx: any, next: Function): void {
    console.log('ParentOrg: Before Save');
    next();
  }
  // Example Remote Method
  myRemote(next: Function): void {
    this.model.find(next);
  }
}

module.exports = ParentOrg;
