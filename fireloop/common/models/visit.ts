import { Model } from '@mean-expert/model';
import * as async from 'async';
import { FirebasePayloadInterface } from '../interfaces/firebase-payload.interface';
import { FirebaseMessage } from '../services/firebase';
import * as request from 'request';
import * as moment from 'moment';
/**
 * @module Visit
 * @description
 * Write a useful Visit Model description.
 * Register hooks and remote methods within the
 * Model Decorator
 **/
@Model({
  hooks: {
    afterSave: { name: 'after save', type: 'operation' },
    beforeSave: { name: 'before save', type: 'operation' },
    afterDelete: { name: 'after delete', type: 'operation' }
  },
  remotes: {
    myRemote: {
      returns : { arg: 'result', type: 'object' },
      http    : { path: '/my-remote', verb: 'get' }
    },
    deleteVisit: {
      accepts: {arg: 'visitId', type: 'string', required: true},
      returns : { root: true, type: 'object' },
      http    : { path: '/:id/deleteVisit', verb: 'post' }
    },
    mutualFriendsPosts: {
      accepts: [
             {arg: 'accountId', type: 'string', required: true},
             { arg: 'limit', type: 'number', required: false }
         ],
      returns : { root: true, type: 'object' },
      http    : { path: '/:id/mutualFriendsPosts', verb: 'post' }
    },
    friendsPosts: {
      accepts: [
             {arg: 'accountId', type: 'string', required: true},
             { arg: 'limit', type: 'number', required: false }
         ],
      returns : { root: true, type: 'object' },
      http    : { path: '/:id/friendsPosts', verb: 'post' }
    }
      
  }
})

class Visit {

  /** @type {WeatherServices} **/
  private weatherServices: {
    openweather: {
      appId: string
    }
  };

  // LoopBack model instance is injected in constructor
  constructor(public model: any) {}

  beforeSave(ctx: any, next: Function): void {
    console.log('Visit: Before Save');
      if (ctx.isNewInstance) {
         // this.getWeatherData(ctx,next);
      async.waterfall([
        (cb: Function) => {
          this.getWeatherData(ctx, cb);
        },
        (cb: Function) => {
          this.closeLastVisit(ctx, cb);
        }
      ],(err) => {
        next(err);
      })
      } else {
      this.checkVisit(ctx, next);
      }
  }

  // Example Operation Hook
  afterSave(ctx: any, next: Function): void {
    if(ctx.instance && ctx.instance.validity === 50 && !ctx.instance.ownerNotified && !ctx.instance.notified ) {
      this.createAccountNotification(ctx.instance, next);
    } else if (ctx.instance && ctx.instance.validity === 100 && !ctx.instance.notified ) {
      this.createFriendsNotifications(ctx.instance, next);
    } else {
      next();
    }
  }

  closeLastVisit(ctx: any, next: Function) {
    var Visit = this.model.app.loopback.getModel('Visit');
    async.waterfall([
      // Get Last visit
      (cb: Function) => {
        Visit.find({
          order: 'entry DESC',
          limit: 1
        }, cb)
      },
      // Verify if last visit has exit, if not close it
      (visits: any[], cb: Function) => {
        if(visits && Array.isArray(visits) && visits.length > 0) {
          let visit = visits[0];
          if(visit.entry && !visit.exit) {
            let now = moment(ctx.instance.entry);
            let previous = moment(visit.entry);
            if(now.diff(previous, 'hours') >= 6 ) {
              visit.exit = moment(visit.entry).add(6, 'hours').toDate();
            } else {
              visit.exit = ctx.instance.entry;
            }
            visit.save();
          }
        } else {
          console.log('The user does not have previous visit');
        }
        cb();
      }
    ], err => {
      if(err) console.log('Closing last visit Error: ' + err);
      next();
    })
  }

  checkVisit(ctx: any, next: Function): void {
    if (
      ctx &&
      ctx.data &&
      ctx.data &&
      ctx.data.locationId &&
      ctx.currentInstance &&
      ctx.currentInstance.locationId &&
      ctx.data.locationId != ctx.currentInstance.locationId
    ) {
      let notification = this.model.app.loopback.getModel('Notification');
      let location = this.model.app.loopback.getModel('Location');
      let notificationTypes = this.model.app.get('notificationTypes');
      async.waterfall([
        (cb: Function) => {
          let filter = {
            where: {
              id: ctx.data.locationId
            },
            include: 'establishment'
          }
          location.find(filter, (err: Error, data: any) => {
            if (err) {
              return cb(err);
            }
            if (data && data.length) {
              return cb(null, data[0].toJSON())
            }
            return cb();
          });
        },
        (location: any, cb: Function) => {
          let account = this.model.app.loopback.getModel('Account');
          account.findById(ctx.currentInstance.accountId, (err: Error, data: any) => {
            if (err) {
              return cb(err);
            }
            return cb(null, location, data);
          });
        },
        (location: any, account: any, cb: Function) => {
          let data = {
            text:  `${location.establishment.name} want to share?`,
            establishmentId: location.establishment.id
          };
          let whereFilter = {
            and: [
              { visitId: ctx.data.id },
              { accountId: ctx.data.accountId },
              { type: notificationTypes.visit }
            ]
          };
          notification.updateAll(whereFilter, data, (err: Error, data: any) => {
            if (err) {
              return cb(err);
            }
            return cb(null, location, account);
          })
        },
        (location: any, account: any, cb: Function) => {
          let data = {
            text:  `${account.firstName || account.displayName} is at ${location.establishment.name} now`,
            establishmentId: location.establishment.id
          };
          let whereFilter = {
            and: [
              { visitId: ctx.data.id },
              { accountId: ctx.data.accountId },
              { type: notificationTypes.friendVisit }
            ]
          };
          notification.updateAll(whereFilter, data, (err: Error, data: any) => {
            if (err) {
              return cb(err);
            }
            return cb()
          })
        }
      ], (err: any, data: any) => {
        return next();
      });
    }else {
      return next();
    }
  }

  afterDelete(ctx: any, next: Function): void {
    if (!(ctx && ctx.where && ctx.where.id)) {
      return next();
    }
    let notification = this.model.app.loopback.getModel('Notification');
    notification.destroyAll({
      visitId: ctx.where.id
    }, (err: Error, info: any) => {
      if (err) {
        console.log('Error when delete notification: ', err);
      }else {
        console.log('Notification deleted succesfully', info);
      }
      return next();
    });
  }

  // Example Remote Method
  myRemote(next: Function): void {
    this.model.find(next);
  }

  public getWeatherData (ctx: any, next: Function) : void {
    if (!(ctx.instance && ctx.instance.locationId)) {
      let error = new Error();
      error.message = 'missing locationId';
      error.name = "LocationId is required."
      return next(error);
    }
      let lat: number = 0.0, lng: number = 0.0;
      let Location = this.model.app.loopback.getModel('Location');
      Location.findById(ctx.instance.locationId, (err:any, location:any) => {
         if (err) {
            return next(err);
         } else if (
        !location ||
        !location.geoLocation ||
        !Array.isArray(location.geoLocation.coordinates) ||
        location.geoLocation.coordinates.length !== 2
      ) {
        return next();
      }
      lat = location.geoLocation.coordinates[1];
         lng = location.geoLocation.coordinates[0];
         this.weatherServices = this.model.app.get('weatherServices');
         let uri: string = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&APPID=${this.weatherServices.openweather.appId}&units=imperial`;
         request.get(uri, (err: Error, response: any, body: any) => {
            if (err) {
               return next(err);
            }
            let data = JSON.parse(body);
            let weather = {
          temp: data.main.temp,
          status: data.weather[0].description
         }
            ctx.instance.weather = weather;
            next();
         });
      });
   }

  createAccountNotification(instance: any, next: Function) {
    let notificationTypes = this.model.app.get('notificationTypes');
    async.waterfall([
      //get Location
      (cb: Function) => {
        this.model.app.models.Location.findById(instance.locationId, {include: 'establishment'}, cb);
      },
      //create notification for the account
      (location: any, cb: Function) => {
        let data: any = {
          ownerId: instance.accountId,
          accountId: instance.accountId,
          visitId: instance.id,
          type: notificationTypes.visit
        }
        if(location && location.establishment()) {
          data.establishmentId = location.establishment().id;
        }
        this.model.app.models.Notification.create(data, cb);
      }
    ], (err: Error, notification: any) => {
      if (err) {
        console.log(err);
        console.log('Error: It was not possible to create visit notification');
      } else {
        instance.ownerNotified = true;
        instance.save();
      }
      return next();
    })
  }

  createFriendsNotifications(instance: any, next: Function): void {
    let notificationTypes = this.model.app.get('notificationTypes');
      let notification = this.model.app.loopback.getModel('Notification');
    async.waterfall([
      //get Location
      (cb: Function) => {
        this.model.app.models.Location.findById(instance.locationId, {include: 'establishment'}, cb);
      },
      //pass establishmentId
      (location: any, cb: Function) => {
        let establishmentId;
        if(location && location.establishment()) {
          establishmentId = location.establishment().id;
          cb(null, establishmentId);
        } else {
          cb(true);
        }
      },
      //getAccount
      (establishmentId: any, cb: Function) => {
        this.model.app.models.Account.findById(instance.accountId, (err: Error, account: any) => {
          if(err) return cb(err);
          if(account && account.settings && account.settings.shareLocation) {
            cb(null, establishmentId);
          } else {
            instance.notified = true;
            instance.save();
            cb(true);
          }
        });
      },
      //get Account friends
      (establishmentId: any, cb: Function) => {
        this.model.app.models.Friend.find({
          where: {
            accountId: instance.accountId,
            approved: true
          }
        }, (err: Error, friends: any[]) => {
          if(err) return cb(err);
          cb(null, friends, establishmentId);
        });
      },
      //create a notification for each friend
      (friends: any[], establishmentId: string, cb: Function) => {
        if(friends && Array.isArray(friends)) {
          async.eachSeries(
            friends,
            (friend: any, callback: Function) => {
              let data: any = {
                ownerId: friend.friendId,
                accountId: instance.accountId,
                visitId: instance.id,
                type: notificationTypes.friendVisit
              }
              if(establishmentId) {
                data.establishmentId = establishmentId
              }
              this.model.app.models.Notification.create(data, (err: Error, data: any) => {
                if (err) {
                  return callback(err);
                }
                callback();
              })
            },
            (err: Error) => {
              if(err) return cb(err);
              cb();
            }
          )
        } else {
          cb();
        }
      },
      (cb: Function) => {
        instance.notified = true;
        instance.save();
        cb();
      }
    ], (err: Error) => {
      if (err) {
        console.log(err);
        console.log('Error: It was not possible to create visit notification');
      }
      return next();
    });
  }

   deleteVisit(visitId:string, next: Function): void {
    async.waterfall([
      //Get Vist instance
      (next: Function) => {
        this.model.findById(visitId, next);
      },
      //Get points
      (visit: any, next: Function) => {
        console.log('deleting visit!!!!!!, ', visit.id);
            visit.updateAttributes({deleted:true, updatedAt: moment().toISOString(), deletedAt: moment().toISOString()}, next);
      }
    ], (err, res) => {
      if(err) return next(err);
      return next(null, res);
    });
   }

   friendsPosts(accountId:string, limit: number = 100, next: Function): void {
      let friends = this.model.app.loopback.getModel('Friend');
     var Visit = this.model.app.loopback.getModel('Visit');
      async.waterfall([
      (cb: Function) => {
        this.model.app.models.Friend.find({
          where: {
            approved: true,
            accountId: accountId
          }
        },cb);
      },
      (mFriends: any[], cb: Function) => {
        let result: any[] = []
        if(Array.isArray(mFriends) && mFriends.length > 0) {
          async.eachSeries(
            mFriends,
            (mFriend: any, callback: Function) => {
              Visit.find({
                 include: ["account", { "location": "establishment" },
                            { "relation": "comments", "scope": { "fields": ["accountId", "comment"] } },
                            { "relation": "likes", "scope": { "fields": ["accountId", "id"] } }],
                 where: { or: [{ accountId: mFriend.friendId, validity: 100, visible:1 }, { accountId: mFriend.friendId, validity: 100, visible: 2 }] }
              }, (err: Error, resvisits: any) => {
                     //            console.log(resvisits);
               if(err) 
               {
                  console.log(err);
                  return callback(err);
               }
               if (Array.isArray(resvisits) && resvisits.length > 0) {
                  async.eachSeries(resvisits, (visit, cb) => { result.push(visit); cb();}, (error: Error) => { });
                  //result.push(resvisits);
               }
                  callback();
                });
            },
            (err: Error) => {
              if(err) return cb(err);
              cb(null,result);
            }
          );
        } else {
          cb(null, result);
        }
      }
    ],(err: Error, result: any) => {
      if(err) return next(err);
         // sort the results before returning
        if (result.length >0)
         {
            result.sort(function(a:any, b:any){
           var alc = a["createdAt"],
               blc = b["createdAt"];
           return alc < blc ? 1 : alc > blc ? -1 : 0;
            });
         }
         if(result.length > limit) {
             result = result.slice(0, limit);
         }
      next(null, result);
    });
  }
   mutualFriendsPosts(accountId:string, limit: number = 100, next: Function): void {
      let friends = this.model.app.loopback.getModel('Friend');
     var Visit = this.model.app.loopback.getModel('Visit');
      async.waterfall([
      (cb: Function) => {
        friends.mutualFollowers(accountId ,cb);
      },
      (mFriends: any[], cb: Function) => {
        let result: any[] = []
        if(Array.isArray(mFriends) && mFriends.length > 0) {
          async.eachSeries(
            mFriends,
            (mFriend: any, callback: Function) => {
              Visit.find({
                 include: ["account", { "location": "establishment" },
                            { "relation": "comments", "scope": { "fields": ["accountId", "comment"] } },
                            { "relation": "likes", "scope": { "fields": ["accountId", "id"] } }],
                 where: { or: [{ accountId: mFriend.id, validity: 100, visible:1 }, { accountId: mFriend.id, validity: 100, visible: 2 }] }
              }, (err: Error, resvisits: any) => {
               if(err) return callback(err);
                  if (Array.isArray(resvisits) && resvisits.length > 0) {
                               async.eachSeries(resvisits, (visit, cb) => { result.push(visit); cb();}, (error: Error) => { });
                     //result.push(resvisits);
                  }
                  callback();
                });
            },
            (err: Error) => {
              if(err) return cb(err);
              cb(null,result);
            }
          );
        } else {
          cb(null, result);
        }
      }
    ],(err: Error, result: any) => {
      if(err) return next(err);
         // sort the results before returning
        if (result.length >0)
         {
            result.sort(function(a:any, b:any){
           var alc = a["createdAt"],
               blc = b["createdAt"];
           return alc < blc ? 1 : alc > blc ? -1 : 0;
            });
         }
         if(result.length > limit) {
             result = result.slice(0, limit);
         }
      next(null, result);
    });
  }



}

module.exports = Visit;
