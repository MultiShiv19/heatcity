import { Model } from '@mean-expert/model';
import * as async from 'async';
import * as moment from 'moment';
/**
 * @module Account
 * @description
 * Write a useful Account Model description.
 * Register hooks and remote methods within the
 * Model Decorator
 **/
@Model({
  hooks: {
    beforeSave: { name: 'before save', type: 'operation' },
    afterSave: { name: 'after save', type: 'operation' },
    afterLogin: { name: 'login', type: 'afterRemote' }
  },
  remotes: {
    searchFacebookFriendsAccounts: {
      accepts: [{arg: 'accountId', type: 'string'}, {arg: 'searchInput', type: 'string'}],
      returns : { root: true, type: 'array' },
      http    : { path: '/searchFacebookFriendsAccounts', verb: 'post' }
    },
    validCode: {
      accepts: [{arg: 'code', type: 'string'}],
      returns: { root: true, type: 'boolean' },
      http: { path: '/validCode', verb: 'get'}
    },
    sendSMSInvitation: {
      accepts: [{arg: 'accountId', type: 'string'}, {arg: 'phones', type: '[string]'}],
      returns: { root: true, type: 'boolean' },
      http: { path: '/sendSMSInvitation', verb: 'post'}
    },
    addFriendsSearch: {
      accepts: [{arg: 'accountId', type: 'string'}, {arg: 'search', type: 'string'}, {arg: 'limit', type: 'number'}],
      returns: { root: true, type: 'array' },
      http: { path: '/addFriendsSearch', verb: 'get'}
    },
    friendsSearch: {
      accepts: [{arg: 'accountId', type: 'string'}, {arg: 'search', type: 'string'}, {arg: 'limit', type: 'number'}],
      returns: { root: true, type: 'array' },
      http: { path: '/friendsSearch', verb: 'get'}
    },
    shareToAllFriends: {
      accepts: [{arg: 'accountId', type: 'string'}, {arg: 'establishmentId', type: 'string'}],
      returns: { root: true, type: 'array' },
      http: { path: '/shareToAllFriends', verb: 'post'}
    },
    closeHanginVisits: {
      accepts: [{arg: 'accountId', type: 'string'}],
      returns: { root: true, type: 'array' },
      http: { path: '/closeHanginVisits', verb: 'get'}
    },
    mutualFollowers: {
      accepts: {arg: 'accountId', type: 'string'},
      returns: { root: true, type: 'array' },
      http: { path: '/mutualFollowers', verb: 'get'}
    },
  }
})

class Account {
  public validCodeCharacters: number[] = [
    35,36,37,38,42,43,48,49,50,51,52,53,54,55,56,57,61,65,66,
    67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,
    86,87,88,89,90,97,98,99,100,101,102,103,104,105,106,107,
    108,109,110,111,112,113,114,115,116,117,118,119,120,121,
    122
  ];
  // LoopBack model instance is injected in constructor
  constructor(public model: any) {}

  /**
   * @method beforeSave
   **/
  beforeSave(ctx: any, next: Function): void {
    if(ctx.isNewInstance) {
      this.createCode(ctx, next);
    } else {
      next();
    }
  }

  /**
   * @method afterSave
   **/
  afterSave(ctx: any, next: Function): void {
    if(
        ctx.isNewInstance &&
        ctx.instance      &&
        ctx.instance.id   &&
        ctx.instance.invitedBy
      ) {
      this.createFriendRelation(ctx, next);
    } else {
      next();
    }
  }



  /**
   * @method afterLogin
   **/
  afterLogin(ctx: any, instance: any, next: Function) {
      this.model.app.models.AccessToken.find({
      where: {
        userId: instance.userId,
        ttl: { gt: 1 }, 
        id: { nin: [instance.id ]}
        }
      },(err: Error, atokens: any[]) => {
      if (err) console.log(err);
      if(err) return next(err);
      if(Array.isArray(atokens) && atokens.length > 0) {
        for(let i = 0; i < atokens.length; i++) {
          let accessToken = atokens[i];
          accessToken.ttl = 1;
          console.log(accessToken);
          accessToken.save();
        }
        next();
      } else {
        console.log("no atokens");
        next();
      }
    });
  }
  /**
   * @method createFriendRelation
   **/
  createFriendRelation(ctx: any, next: Function): void {
    async.waterfall([
      // get invitedBy account
      (cb: Function) => {
        this.model.findOne({
          where: {
            code: ctx.instance.invitedBy
          }
        }, cb);
      },
      //create Friend relation
      (account: any, cb: Function) => {
        if(account && account.id) {
          this.model.app.models.Friend.create({
            accountId: account.id,
            friendId: ctx.instance.id,
            approved: false
          }, cb);
        } else {
          cb();
        }
      }
    ], (err: Error) => {
      if(err) {console.log('Error: ',err)};
      next();
    })
  }

  /**
   * @method createCode
   **/
  createCode(ctx: any, next: Function) {
    let code = '';
    for(let i = 0; i < 5; i++) {
      let char = Math.floor((Math.random() * this.validCodeCharacters.length));
      code += String.fromCharCode(this.validCodeCharacters[char]);
    }
    this.model.find({
      where:{
        code: code
      }
    }, (err: Error, accounts: any[]) => {
      if(err) return next(err);
      if(Array.isArray(accounts) && accounts.length > 0) {
        console.log('Fail there is already a user with this code! ', code)
        return this.createCode(ctx, next);
      } else {
        console.log('Success! new code added to account', code);
        ctx.instance.code = code;
        return next();
      }
    });
  }

  /**
   * @method validCode
   **/
  validCode(code: string, next: Function): void {
    console.log('Im here!');
    this.model.find({
      where: {
        code: code
      }
    }, (err: Error, accounts: any[]) => {
      console.log('Now here');
      if(err) return next(err);
      if(Array.isArray(accounts) && accounts.length > 0) {
        console.log('finally here!');
        return next(null, true);
      } else {
        return next(null, false);
      }
    });
  }

  /**
   * @method searchFacebookFriendsAccounts
   **/
  searchFacebookFriendsAccounts(accountId: string, searchInput: string, next: Function): void {
    async.waterfall([
      //Get user account
      (cb: Function) => {
        this.model.findById(accountId,cb);
      },
      //Get accounts through SocialCredential usign the facebookIds
      (account: any, cb: Function) => {
        if(
          account &&
          account.fbFriendList &&
          Array.isArray(account.fbFriendList) &&
          account.fbFriendList.length > 0
        ) {
          this.model.app.models.SocialCredential.find({
            where: {
              or: account.fbFriendList
            },
            include: 'account',
          }, cb);
        } else {
          cb(null, []);
        }
      },
      //Verify if the current user is already friend with each account
      (socialCredentials: any, cb: Function) => {
        let accounts: any[] = [];
        if(Array.isArray(socialCredentials) && socialCredentials.length > 0) {
          async.eachSeries(
            socialCredentials,
            (socialCredential: any, callback: Function) => {
              if(
                socialCredential.account() &&
                socialCredential.account().id &&
                socialCredential.account().displayName &&
                (socialCredential.account().displayName).toLowerCase().search(searchInput.toLowerCase()) != -1
              ) {
                this.model.app.models.Friend.find({
                  where: {
                    accountId: accountId,
                    friendId: socialCredential.account().id
                  }
                },(err: Error, friends: any[]) => {
                  if(err) return callback(err);
                  if(Array.isArray(friends) && friends.length == 0) {
                    accounts.push(socialCredential.account());
                  }
                  callback();
                })
              } else {
                callback();
              }
          },
          (err: Error) => {
            if(err) return cb(err);
            cb(null, accounts);
          });
        } else {
          cb(null,accounts);
        }
      }
    ], (err: Error, accounts: any[]) => {
      if(err) return next(err);
      next(null, accounts);
    })
  }

  /**
   * @method sendSMSInvitation
   **/
  sendSMSInvitation(accountId: string, phones: string[], next: Function) {
    let twilioPhone = this.model.app.get('twilio').numbers.test;
    console.log('Sending invitation SMS');
    async.waterfall([
      //get account with provided id
      (cb: Function) => {
        this.model.findById(accountId, cb);
      },
      //send invitation SMS to each phone in the array phones.
      (account: any, cb: Function) => {
        async.eachSeries(
          phones,
          (phone: string, callback: Function) => {
            this.model.app.models.Twilio.send(
              {
                type: 'sms',
                to: '+'+phone,
                from: twilioPhone,
                body: account.displayName + ' has invited you to join HEATCITY!. Your access code is: ' + account.code
              },(err: Error, res: any) => {
                if(err) return callback(err);
                callback();
              }
            )
          },
          (err: Error) => {
            if(err) return cb(err);
            cb();
          }
        );
      }
    ],
    (err: Error) => {
      if(err) return next(err);
      next(null, true);
    })
  }

  /**
   * @method addFriendsSearch
   **/
  addFriendsSearch(accountId: string, search: string, limit: number, next: Function): void {
    async.waterfall([
      (cb: Function) => {
        this.model.find({
          where: {
                or: [
                  {
                    displayName: { 
                      like: '.*' + (search ? search : '') + '.*', 
                      options: 'i'
                    }
                  },
                  {
                    username: { 
                      like: '.*' + (search ? search : '') + '.*', 
                      options: 'i'
                    }
                  }
                ],
                id: {
                  nlike: accountId
                }
            }
        }, cb);
      },
      (accounts: any[], cb: Function) => {
        let result: any[] = []
        if(Array.isArray(accounts) && accounts.length > 0) {
          async.eachSeries(
            accounts,
            (account: any, callback: Function) => {
              if(account.id != accountId) {
                this.model.app.models.Friend.find({
                  where: {
                    accountId: accountId,
                    friendId: account.id
                  }
                }, (err: Error, friends: any[]) => {
                  if(err) return callback(err);
                  if(Array.isArray(friends) && friends.length == 0) {
                    result.push(account);
                  }
                  callback();
                });
              } else {
                callback();
              }
            },
            (err: Error) => {
              if(err) return cb(err);
              cb(null,result);
            }
          );
        } else {
          cb(null, result);
        }
      }
    ], (err: Error, result: any[]) => {
      if(err) return next(err);
      if(result.length > limit) {
        result = result.slice(0, limit);
      }
      next(null, result);
    });
  }

  /**
   * @method friendsSearch
   **/
  friendsSearch(accountId: string, search: string, limit: number, next: Function): void {
    async.waterfall([
      (cb: Function) => {
        this.model.find({
          where: {
                or: [
                  {
                    displayName: { 
                      like: '.*' + (search ? search : '') + '.*', 
                      options: 'i'
                    }
                  },
                  {
                    username: { 
                      like: '.*' + (search ? search : '') + '.*', 
                      options: 'i'
                    }
                  }
                ],
                id: {
                  nlike: accountId
                }
            }
        }, cb);
      },
      (accounts: any[], cb: Function) => {
        let result: any[] = []
        if(Array.isArray(accounts) && accounts.length > 0) {
          async.eachSeries(
            accounts,
            (account: any, callback: Function) => {
              if(account.id != accountId) {
                this.model.app.models.Friend.find({
                  where: {
                    approved: true,
                    accountId: accountId,
                    friendId: account.id
                  }
                }, (err: Error, friends: any[]) => {
                  if(err) return callback(err);
                  if(Array.isArray(friends) && friends.length > 0) {
                    result.push(account);
                  }
                  callback();
                });
              } else {
                callback();
              }
            },
            (err: Error) => {
              if(err) return cb(err);
              cb(null,result);
            }
          );
        } else {
          cb(null, result);
        }
      }
    ], (err: Error, result: any[]) => {
      if(err) return next(err);
      if(result.length > limit) {
        result = result.slice(0, limit);
      }
      next(null, result);
    });
  }

  /**
   * @method mutualFollowers
   **/
  mutualFollowers(accountId: string, next: Function): void {
    console.log("in mutual followers");
    async.waterfall([
      (cb: Function) => {
        this.model.app.models.Friend.find({
          where: {
            approved: true,
            mutual: true,
            accountId: accountId
          }
        },cb);
      },
      (mFriends: any[], cb: Function) => {
        let result: any[] = []
        if(Array.isArray(mFriends) && mFriends.length > 0) {
          async.eachSeries(
            mFriends,
            (mFriend: any, callback: Function) => {
              this.model.app.models.Account.findOne({
                where: {
                    id: mFriend.friendId
                }
              }, (err: Error, resaccount: any) => {
               if(err) return callback(err);
                  result.push(resaccount);
                  callback();
                });
            },
            (err: Error) => {
              if(err) return cb(err);
              cb(null,result);
            }
          );
        } else {
          cb(null, result);
        }
      }
    ],(err: Error, result: any) => {
      if(err) return next(err);
      next(null, result);
    });
  }

  /**
   * @method shareToAllFriends
   **/
  shareToAllFriends(accountId: string, establishmentId: string, next: Function): void {
    async.waterfall([
      (cb: Function) => {
        this.model.app.models.Friend.find({
          where: {
            approved: true,
            accountId: accountId
          },
          include: 'friend'
        },cb);
      },
      (friends: any[], cb: Function) => {
        if(Array.isArray(friends) && friends.length > 0) {
          let shares = friends.map((friend) => {
            return {
              accountId: accountId,
              friendId: friend.friend().id,
              establishmentId: establishmentId
            };
          });
          this.model.app.models.Share.create(shares, cb);
        } else {
          cb({message: 'user does not have friends'});
        }
      }
    ],(err: Error, result: any) => {
      if(err) return next(err);
      next(null,result);
    });
  }

  /**
   * @method addFriendsSearch
   **/
  closeHanginVisits(accountId: string, next: Function) {
    async.waterfall([
      //Get all visits with more than 6 hours of difference from now
      (cb: Function) => {
        let now = moment().subtract(6, 'hours').toISOString();
        this.model.app.models.Visit.find({
          where: {
            accountId: accountId,
            entry: {lt: now}
          }
        }, cb);
      },
      (visits: any[], cb: Function) => {
        if(visits && Array.isArray(visits)) {
          visits.forEach((visit) => {
            let now = moment();
            if(!visit.exit && visit.entry) {
              let visitEntry = moment(visit.entry);
              if(now.diff(visitEntry, 'hours') >= 6) {
                visit.exit = visitEntry.add(6,'hours').toDate();
                visit.save();
              }
            }
          });
          cb(null, visits);
        }
      }
    ], (err, res) => {
      if(err) {
        console.log(err);
        return next(err);
      }
      next(null, res);
    })
  }
}

module.exports = Account;
