import { Model } from '@mean-expert/model';
import * as async from 'async';
import { FirebasePayloadInterface } from '../interfaces/firebase-payload.interface';
import { FirebaseMessage } from '../services/firebase';
/**
 * @module Share
 * @description
 * Write a useful Share Model description.
 * Register hooks and remote methods within the
 * Model Decorator
 **/
@Model({
  hooks: {
    afterSave: { name: 'after save', type: 'operation' }
  },
  remotes: {
    myRemote: {
      returns : { arg: 'result', type: 'object' },
      http    : { path: '/my-remote', verb: 'get' }
    }
  }
})

class Share {
  // LoopBack model instance is injected in constructor
  constructor(public model: any) {}

  // Example Operation Hook
  afterSave(ctx: any, next: Function): void {
    if (ctx && !ctx.isNewInstance) {
      return next();
    }
    if (ctx.instance && process.env.NODE_ENV != 'testing') {
      this.createNotification(ctx.instance, next);
    }else {
      next();
    }
  }
  
  // Example Remote Method
  myRemote(next: Function): void {
    this.model.find(next);
  }

  createNotification(instance: any, next: Function): void {
    let notificationTypes = this.model.app.get('notificationTypes');
		let notification = this.model.app.loopback.getModel('Notification');
    let data = {
      ownerId: instance.friendId,
      accountId: instance.accountId,
      establishmentId: instance.establishmentId,
      shareId: instance.id,
      type: notificationTypes.establishmentShare
    }
    notification.create(data, (err: Error, data: any) => {
      if (err) {
        console.log('It wasnot possible to create share notification');
      }
      return next();
    })
  }
}

module.exports = Share;
