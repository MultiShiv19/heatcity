export interface StatsInterface {
    gender?: {
        female: number,
        male: number
    },
    crowd?: {
        current: number
    },
    visited?: {
        today: number,
        thisWeek:number,
        thisMonth:number,
        thisYear:number,
        total: number
    },
    timeSpent?: {
        today: number,
        thisWeek:number,
        thisMonth:number,
        thisYear:number
    },
    time?: {
        min: number,
        max: number,
        average: number
    },
    heatHours?: Array<any>,
    ages?: Array<any>,
    displayAges?: boolean
};


export interface GraphStatsInterface {
  visitCount? : { [key:string]:number; } ,
  timeSpent? : { [key:string]:number; } ,
}

export interface DemographicsStatsInterface {
  today?: {
    ages?: Array<any>,
    gender?: {
        female: number,
        male: number
    }
  }
  week?: {
    ages?: Array<any>,
    gender?: {
        female: number,
        male: number
    }
  }
  month?: {
    ages?: Array<any>,
    gender?: {
        female: number,
        male: number
    }
  }
  year?: {
    ages?: Array<any>,
    gender?: {
        female: number,
        male: number
    }
  }
}

export interface DemographicsGraphInterface {
  total?: {
    ages?: Array<any>,
    gender?: {
        female: number,
        male: number
    }
  }

export interface VisitStats {
  count?: number
  }
}
