'use strict';
module.exports = {
    db: {
        name: 'db',
        connector: 'mongodb',
        url: 'mongodb://blue-dragon-db:blue-dragon-db@127.0.0.1:27017/blue-dragon-staging'
    },
    storage: {
        keyFilename: require('path').resolve(__dirname, 'gcs/key.json')
    },
    twilio: {
        name: 'twilio',
        connector: 'loopback-connector-twilio',
        accountSid: 'AC8192a793af7b6415fcd403bbb5faa1ec',
        authToken: '84a2ee5b3d10be6cf60690e69bfb34bd'
    }
};
