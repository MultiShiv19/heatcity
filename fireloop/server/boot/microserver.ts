import { BootScript } from '@mean-expert/boot-script';
import {TopicInterface} from '../../common/interfaces/topic.interface';
import * as io from 'socket.io';

@BootScript()
class MicroServer {
    /**
     * @prop {any} server MicroService WebSocket Server
     * @type {any}
     **/
    //private server: any = io(4200, { transports: ['websocket'] });
    /**
     * @param {any} app Server Application
     **/
    constructor(private app: any) {
        /*
        this.server.on('connection', (socket: any) => {
            console.log('SOCKET CONNECTION: ', socket.id);
            socket.on(`FETCH:ESTABLISHMENTS:${process.env.NODE_ENV}`, (data: any) => {
                socket.broadcast.emit(`ON:FETCH:ESTABLISHMENTS:${process.env.NODE_ENV}`, data);
            });
            socket.on(`SEND:ESTABLISHMENTS:${process.env.NODE_ENV}`, (data: any) => {
                socket.broadcast.emit(`ON:ESTABLISHMENTS:BATCH:${process.env.NODE_ENV}`, data);
            });
            socket.on(`AREA:UPDATE:${process.env.NODE_ENV}`, (data: any) => {
                socket.broadcast.emit(`ON:AREA:UPDATE:${process.env.NODE_ENV}`, data);
            });
            socket.on('disconnect', () => {
                console.log('SOCKET DISCONNECTION: ', socket.id);
            });
        });
        */
    }
}

module.exports = MicroServer;
