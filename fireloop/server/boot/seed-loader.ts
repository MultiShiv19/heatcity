import { BootScript } from '@mean-expert/boot-script';
import * as async from 'async';

@BootScript()
class SeedLoader {
    constructor(app: any) {
        let Account     = app.models.Account;
        let Role        = app.models.Role;
        let RoleMapping = app.models.RoleMapping;
        let seed        = (<any>Object).assign({ enabled: false }, app.get('seed'));
        if (!seed.enabled) return;
        // asyncrouns process flow control
        async.waterfall([
            function buildRoles(next: Function) {
                async.each(seed.roles, function(role, next) {
                    var query = { name: role };
                    Role.findOrCreate({ where: query }, query, next);
                }, (err: any) => {
                    return next(err);
                });
            },
            function getAdminRole (next: Function) {
                Role.findOne({ where: { name: '$admin' } }, next);
            },
            function getAdminAccounts(role: any, next: Function) {
                async.each(seed.admins, (admin: any, next: Function) => {
                    admin.roles = [role];
                    Account.findOrCreate({
                        where: { email: admin.email }
                    }, admin, next);
                });
            }
        ], err => { if (err) console.error(err); });
    }
}

module.exports = SeedLoader;
