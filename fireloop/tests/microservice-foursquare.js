'use strict';
/*

TODO: Migrate TESTS TO NEW TRANSPORTATION  (JC)

var should = require('chai').should();
var kafka = require('kafka-node');
var config = require('../server/config.json');
var Producer = kafka.Producer;
var Consumer = kafka.Consumer;
var Client = kafka.Client;
var topics = {
    request: 'requestUpdate' + process.env.NODE_ENV,
    response: 'responseUpdate' + process.env.NODE_ENV,
    update: 'updateArea' + process.env.NODE_ENV
};

var areaObject = {
    foursquare: {
        keys: [{
            secretId: 'STZO1JGDDWM4AJUDQL4PWZGHFLSMBKD3V3J25HIUALVKWIO5',
            clientId: 'VBIF2VYUBHKDGYYHTFTM2RSLOO054PSAD0HNAGWN5AP1GMRW'
        }],
        photosLimit: 2
    },
    geogrid:
    [
        [
            {lat: 20.6037373, lng: -103.4070646}
        ]
    ],
    areaId: '5841f303d429621066d86040'
};

describe('Foursquare Microservice test', function() {
    it('Should get message from responseUpdate topic', function(done) {
        this.timeout(30000);
        var client = new Client(config.kafka.url, config.kafka.namespace);
        var offset = new kafka.Offset(client);
        var producer = new Producer(client);
        producer.createTopics(
            [topics.resquest, topics.response],
            false,
            function(err, data) {}
        );
        offset.fetchLatestOffsets([topics.response], function(err, data) {
            should.not.exist(err);
            should.exist(data);

            var lastOffset = data ? data[topics.response]['0'] : 0;
            var consumer = new Consumer(
                client,
                [{
                    topic: topics.response,
                    partition: 0,
                    offset: lastOffset
                }],
                {autoCommit: false, fromOffset: true}
            );

            consumer.on('message', function(message) {
                var messageArray = JSON.parse(message.value);
                messageArray.should.be.a('array');
                for (var i = 0; i < messageArray.length; i++) {
                    messageArray[i].should.have.property('name');
                    messageArray[i].should.have.property('visits');
                    messageArray[i].should.have.property('photos');
                    messageArray[i].should.have.property('location');
                    messageArray[i].should.have.property('categories');
                    messageArray[i].should.have.property('areaId');
                }
                consumer.close(function() {});
                client.close(function() {});
                done();
            });

            consumer.on('error', function(err) {
                should.not.exist(err);
            });

            producer.send([{
                topic: topics.request,
                messages: JSON.stringify(areaObject)
            }], function(err, result) {
                should.not.exist(err);
                should.exist(result);
            });
        });
    });
});
*/
