'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('EstablishmentPhoto relations', function() {
    describe('EstablishmentPhoto belongs to Establishment', function() {
        it('error if GET establishmentPhoto.establishment dont work',
        function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 89,
                        lng: 16
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishmentPhoto);
            }

            function createEstablishmentPhoto(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                var fileName = 'file-' + Date.now();
                api.post('/establishments/' + res.body.id + '/photos')
                .send({
                    expiresAt: new Date(),
                    file: {file: fileName, path: '/path/to/file/' + fileName},
                    accountId: account.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEstablishmentFromEstablishmentPhoto);
            }

            function getEstablishmentFromEstablishmentPhoto(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get(
                    '/establishmentPhotos/' + res.body.id + '/establishment'
                )
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });

    describe('EstablishmentPhoto belongs to Account', function() {
        it('error if GET establishmentPhoto.account dont work',
        function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 77,
                        lng: 66
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishmentPhoto);
            }

            function createEstablishmentPhoto(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                var fileName = 'file-' + Date.now();
                api.post('/establishments/' + res.body.id + '/photos')
                .send({
                    expiresAt: new Date(),
                    file: {file: fileName, path: '/path/to/file/' + fileName},
                    accountId: account.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getAccountFromEstablishmentPhoto);
            }

            function getAccountFromEstablishmentPhoto(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get('/establishmentPhotos/' + res.body.id + '/account')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
});
