'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('Chat section', function() {
    it('POST chatChannel.messages should work.', function(done) {
        var token, accountId;
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    accountId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            api.post('/locations')
                .send({
                    location: {
                        lat: 20.656948,
                        lng: -103.398025
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createChatChannel);
        }

        function createChatChannel(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/chatChannels')
                .send({
                    name: Date.now(),
                    description: Date.now(),
                    accountId: accountId,
                    locationId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createMessage);
        }

        function createMessage(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/chatChannels/' + res.body.id + '/messages')
                .send({
                    text: Date.now(),
                    accountId: accountId
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('POST chatChannel.messages without accountId should\'nt work.',
    function(done) {
        var token, accountId;
        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    accountId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            api.post('/locations')
                .send({
                    location: {
                        lat: 20.656948,
                        lng: -103.398025
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createChatChannel);
        }

        function createChatChannel(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/chatChannels')
                .send({
                    name: Date.now(),
                    description: Date.now(),
                    accountId: accountId,
                    locationId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createMessage);
        }

        function createMessage(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/chatChannels/' + res.body.id + '/messages')
                .send({
                    text: Date.now()
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(422, inspectError);
        }

        function inspectError(err, res) {
            if (err) return done(err);
            var errorCodes = JSON.parse(res.error.text).error.details.codes;
            errorCodes.should.have.property('accountId');
            done();
        }
    });

    it('GET chatChannel.account should work.', function(done) {
        var token, accountId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    accountId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            api.post('/locations')
                .send({
                    location: {
                        lat: 20.656948,
                        lng: -103.398025
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createChatChannel);
        }

        function createChatChannel(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/' + accountId + '/chatChannels')
                .send({
                    name: Date.now(),
                    description: Date.now(),
                    locationId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, getAccount);
        }

        function getAccount(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/chatChannels/' + res.body.id + '/account')
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });

    it('GET message.account should work.', function(done) {
        var token, accountId;

        api.post('/accounts')
            .send({
                email: Date.now() + '@test.com',
                password: '123'
            })
            .expect(200, login);

        function login(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, function(err, res) {
                    if (err) {
                        return done(err);
                    }
                    token = res.body.id;
                    accountId = res.body.userId;
                    createLocation(err, res);
                });
        }

        function createLocation(err, res) {
            api.post('/locations')
                .send({
                    location: {
                        lat: 20.656948,
                        lng: -103.398025
                    }
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createChatChannel);
        }

        function createChatChannel(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/chatChannels')
                .send({
                    name: Date.now(),
                    description: Date.now(),
                    accountId: accountId,
                    locationId: res.body.id
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, createMessage);
        }

        function createMessage(err, res) {
            if (err) {
                return done(err);
            }
            api.post('/chatChannels/' + res.body.id + '/messages')
                .send({
                    text: Date.now(),
                    accountId: accountId
                })
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, getAccount);
        }

        function getAccount(err, res) {
            if (err) {
                return done(err);
            }
            api.get('/messages/' + res.body.id + '/account')
                .set({'Authorization': token})
                .query({'access_token': token})
                .expect(200, done);
        }
    });
});
