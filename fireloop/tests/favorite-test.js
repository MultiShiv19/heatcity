'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('Favorite retalions', function() {
    describe('Favorite belongs to Account', function() {
        it('error if GET favorite.account dont work', function(done) {
            var account, token, establishment;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 56,
                        lng: 16
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for favorite',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                establishment = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/accounts/' + account.id + '/favorites')
                .send({
                    establishmentId: establishment.id,
                    favoriteListId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getAccountFromFavorite);
            }

            function getAccountFromFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('accountId');
                api.get('/favorites/' + res.body.id + '/account')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
    describe('Favorite belongs to FavoriteList', function() {
        it('error if GET favorite.favoriteList dont work', function(done) {
            var account, token, establishment;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 50,
                        lng: 94
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for favorite',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                establishment = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/accounts/' + account.id + '/favorites')
                .send({
                    establishmentId: establishment.id,
                    favoriteListId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFavoriteListFromFavorite);
            }

            function getFavoriteListFromFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('favoriteListId');
                api.get('/favorites/' + res.body.id + '/favoriteList')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
    describe('Favorite belongs to Establishment', function() {
        it('error if GET favorite.establishment dont work', function(done) {
            var account, token, establishment;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 7,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for favorite',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                establishment = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/accounts/' + account.id + '/favorites')
                .send({
                    establishmentId: establishment.id,
                    favoriteListId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEstablishmentFromFavorite);
            }

            function getEstablishmentFromFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('establishmentId');
                api.get('/favorites/' + res.body.id + '/establishment')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
});
