'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('MusicList relations', function() {
    describe('MusicList belongs to Account', function() {
        it('error if musicList.account dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for musicList',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createMusicList);
            }

            function createMusicList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/musicLists')
                .send({
                    accountId: account.id,
                    name: 'Music List - ' + Date.now()
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getAccountFromMusicList);
            }

            function getAccountFromMusicList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get('/musicLists/' + res.body.id + '/account')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });

    describe('MusicList belongs to Establishment', function() {
        it('error if musicList.establishment dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for musicList',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createMusicList);
            }

            function createMusicList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/musicLists')
                .send({
                    accountId: account.id,
                    name: 'Music List - ' + Date.now()
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEstablishmentFromMusicList);
            }

            function getEstablishmentFromMusicList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get('/musicLists/' + res.body.id + '/establishment')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
});
