'use strict';
var should = require('chai').should();
var supertest = require('supertest');
var api = supertest('http://localhost:3000/api');

describe('Establishment relations', function() {
    describe('Establishment belongs to Account', function() {
        it('error if GET establishment.account dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 1,
                        lng: 2
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getAccountFromEstablishment);
            }

            function getAccountFromEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get('/establishments/' + res.body.id + '/account')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });

    describe('Establishment has many EstablishmentPhotos', function() {
        it('error if POST establishment.photos dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 3,
                        lng: 4
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishmentPhoto);
            }

            function createEstablishmentPhoto(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                var fileName = 'file-' + Date.now();
                api.post('/establishments/' + res.body.id + '/photos')
                .send({
                    expiresAt: new Date(),
                    file: {file: fileName, path: '/path/to/file/' + fileName},
                    accountId: account.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if POST establishment.photos work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: res.body.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 3,
                        lng: 4
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishmentPhoto);
            }

            function createEstablishmentPhoto(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                var fileName = 'file-' + Date.now();
                api.post('/establishments/' + res.body.id + '/photos')
                .send({
                    expiresAt: new Date(),
                    file: {file: fileName, path: '/path/to/file/' + fileName}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if GET establishment.photos dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 5,
                        lng: 6
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishmentPhoto);
            }

            function createEstablishmentPhoto(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                var fileName = 'file-' + Date.now();
                api.post('/establishments/' + res.body.id + '/photos')
                .send({
                    expiresAt: new Date(),
                    file: {file: fileName, path: '/path/to/file/' + fileName},
                    accountId: account.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEstablishmentPhotoFromEstablishment);
            }

            function getEstablishmentPhotoFromEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get(
                    '/establishments/' + res.body.establishmentId + '/photos'
                )
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });

    describe('Establishment has many Promotion', function() {
        it('error if POST establishment.promotions dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 18,
                        lng: 23
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createPromotion);
            }

            function createPromotion(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/promotions')
                .send({
                    name: 'Promotion-' + Date.now(),
                    description: 'Test promotion',
                    amount: 1
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if GET establishment.promotions dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 21,
                        lng: 12
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createPromotion);
            }

            function createPromotion(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/promotions')
                .send({
                    name: 'Promotion-' + Date.now(),
                    description: 'Test promotion',
                    amount: 1
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getPromotionsFromEstablishment);
            }

            function getPromotionsFromEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('establishmentId');
                api.get(
                    '/establishments/' +
                    res.body.establishmentId +
                    '/promotions'
                )
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });

    describe('Establishment has many Event',  function() {
        it('error if POST establishment.events dont work',  function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 44,
                        lng: 56
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEvent);
            }

            function createEvent(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/events')
                .send({
                    accountId: account.id,
                    name: 'Event-' + Date.now(),
                    description: 'Test event'
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if POST establishment.events work',  function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 44,
                        lng: 56
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEvent);
            }

            function createEvent(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/events')
                .send({
                    name: 'Event-' + Date.now(),
                    description: 'Test event'
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(422, inspectError);
            }

            function inspectError(err, res) {
                if (err) return done(err);
                var errorCodes = JSON.parse(res.error.text).error.details.codes;
                errorCodes.should.have.property('accountId');
                done();
            }
        });

        it('error if GET establishment.events dont work',  function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 39,
                        lng: 46
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEvent);
            }

            function createEvent(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/events')
                .send({
                    accountId: account.id,
                    name: 'Event-' + Date.now(),
                    description: 'Test event'
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getEventsFromEstablishment);
            }

            function getEventsFromEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('establishmentId');
                api.get(
                    '/establishments/' + res.body.establishmentId + '/events'
                )
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });

    describe('Establishment has many MusicList', function() {
        it('error if POST establishment.musicLists dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createMusicList);
            }

            function createMusicList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/musicLists')
                .send({
                    accountId: account.id,
                    name: 'Music List - ' +  Date.now()
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if POST establishment.musicLists work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createMusicList);
            }

            function createMusicList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/musicLists')
                .send({
                    name: 'Music List - ' + Date.now()
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(422, inspectError);
            }

            function inspectError(err, res) {
                if (err) return done(err);
                var errorCodes = JSON.parse(res.error.text).error.details.codes;
                errorCodes.should.have.property('accountId');
                done();
            }
        });

        it('error if GET establishment.musicLists dont work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createMusicList);
            }

            function createMusicList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/musicLists')
                .send({
                    accountId: account.id,
                    name: 'Music List - ' + Date.now()
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getMusicListsFromEstablishment);
            }

            function getMusicListsFromEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('establishmentId');
                api.get(
                    '/establishments/' +
                    res.body.establishmentId +
                    '/musicLists'
                )
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });

    describe('Establishment has many Share', function() {
        it('error if POST establishment.shares dont work', function(done) {
            var account, friendAccount, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                friendAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/shares')
                .send({
                    accountId: account.id,
                    friendId: friendAccount.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if POST establishment.shares work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/shares')
                .send({})
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(422, inspectError);
            }

            function inspectError(err, res) {
                if (err) return done(err);
                var errorCodes = JSON.parse(res.error.text).error.details.codes;
                errorCodes.should.have.property('accountId');
                errorCodes.should.have.property('friendId');
                done();
            }
        });

        it('error if GET establishment.shares dont work', function(done) {
            var account, friendAccount, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, createSecondAccount);

            function createSecondAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts')
                .send({email: Date.now() + '@test2.com', password: '123'})
                .expect(200, loginAccount);
            }

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                friendAccount = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 63,
                        lng: 70
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createShare);
            }

            function createShare(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/shares')
                .send({
                    accountId: account.id,
                    friendId: friendAccount.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getSharesFromEstablishment);
            }

            function getSharesFromEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                res.body.should.have.property('establishmentId');
                api.get(
                    '/establishments/' + res.body.establishmentId + '/shares'
                )
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });

    describe('Establishment has many Favorite', function() {
        it('error if POST establishment.favorites  dont work', function(done) {
            var account, token, establishment;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 39,
                        lng: 46
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                establishment = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' + Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + establishment.id + '/favorites')
                .send({
                    accountId: account.id,
                    favoriteListId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });

        it('error if POST establishment.favorites work', function(done) {
            var account, token;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 39,
                        lng: 46
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + res.body.id + '/favorites')
                .send({})
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(422, inspectError);
            }

            function inspectError(err, res) {
                if (err) return done(err);
                var errorCodes = JSON.parse(res.error.text).error.details.codes;
                errorCodes.should.have.property('accountId');
                errorCodes.should.have.property('favoriteListId');
                done();
            }
        });

        it('error if GET establishment.favorites dont work', function(done) {
            var account, token, establishment;
            api.post('/accounts')
            .send({email: Date.now() + '@test.com', password: '123'})
            .expect(200, loginAccount);

            function loginAccount(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                account = res.body;
                api.post('/accounts/login')
                .send({
                    email: account.email,
                    password: '123'
                })
                .expect(200, createLocation);
            }

            function createLocation(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                token = res.body;
                api.post('/locations')
                .send({
                    location: {
                        lat: 39,
                        lng: 46
                    }
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createEstablishment);
            }

            function createEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments')
                .send({
                    accountId: account.id,
                    locationId: res.body.id,
                    name: 'Establishment-' + Date.now(),
                    description: 'Just a test for estblishment',
                    email: 'establishment@test.com',
                    url: 'http://establishment.test.com',
                    pricing: 'test',
                    rating: 0,
                    categories: ['test', 'makingATest'],
                    operatesAt: {from: '9 a.m.', to: '5 p.m.'},
                    popularAt: {from: '10 a.m.', to: '1 p.m.'}
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavoriteList);
            }

            function createFavoriteList(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                establishment = res.body;
                api.post('/accounts/' + account.id + '/favoriteLists')
                .send({
                    name: 'Favorite List - ' +  Date.now(),
                    followersCount: 0
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, createFavorite);
            }

            function createFavorite(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.post('/establishments/' + establishment.id + '/favorites')
                .send({
                    accountId: account.id,
                    favoriteListId: res.body.id
                })
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, getFavoritesFromEstablishment);
            }

            function getFavoritesFromEstablishment(err, res) {
                if (err) return done(err);
                res.body.should.have.property('id');
                api.get('/establishments/' + establishment.id + '/favorites')
                .set('Authorization', token.id)
                .set('access_token', token.id)
                .expect(200, done);
            }
        });
    });
});
