import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth-guard.service';
import { UserProfileComponent } from './user-profile.component';
import { editUserComponent } from './edit-user/edit-user.component';
import { followingComponent } from './following/following.component';
import { followComponent } from './follow/follow.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { findFacebookFriendsComponent } from './find-facebook-friends/find-facebook-friends.component';
import { AddFriendsComponent } from './add-friends/add-friends.component';

const userProfileRoutes: Routes = [
    { path: 'user-profile/:id', component: UserProfileComponent, canActivate: [AuthGuard] },
    { path: 'edit-profile', component: editUserComponent, canActivate: [AuthGuard] },
    { path: 'following', component: followingComponent, canActivate: [AuthGuard] },
    { path: 'follow', component: followComponent, canActivate: [AuthGuard] },
    { path: 'user-setting', component: UserSettingsComponent, canActivate: [AuthGuard] },
    // { path: 'find-facebook', component: findFacebookFriendsComponent, canActivate: [AuthGuard] },
    { path: 'find-facebook', component: AddFriendsComponent, canActivate: [AuthGuard] }
    
    
];

export const userProfileRouting: ModuleWithProviders = RouterModule.forChild(userProfileRoutes);