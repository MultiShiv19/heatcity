import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import * as dialogs from "ui/dialogs";
import { FriendApi, AccountApi } from '../../shared/sdk/services/index';
import { LoopBackFilter } from '../../shared/sdk/models/BaseModels';
import { Friend } from '../../shared/sdk/models';
import { RouterExtensions } from "nativescript-angular/router";


@Component({
    selector: 'app-follow-card',
    moduleId: module.id,
    templateUrl: 'follow-card.component.html',
    styleUrls: ['follow-card.component.css']
})

export class followCardComponent implements OnInit {
    @Input() data;
    @Output() unfollow: EventEmitter<any> = new EventEmitter<any>();
    @Output() follow: EventEmitter<any> = new EventEmitter<any>();
    private userId = this._accountApi.getCurrentId();
    followShow = true;
    public defaultImage = 'https://scontent.xx.fbcdn.net/v/t1.0-1/c141.0.480.480/p480x480/10354686_10150004552801856_220367501106153455_n.jpg?oh=09dfd8cde07f291f3bcfe13ac50f5ae7&oe=5A39ADB3';
    constructor(private _friendApi: FriendApi,
        private _routerExtensions: RouterExtensions,
        private _accountApi: AccountApi) { }

    ngOnInit() { }

    public unfollowConfirm(data) {
        dialogs.confirm({
            cancelButtonText: 'cancel',
            okButtonText: 'unfollow',
            title: 'Are you sure you want to unfollow ' + data.displayName + '?',
        }).then((result) => {
            if (result) {
                this.unfollow.emit(data);
                this._friendApi.deleteById(data.friendshipId).subscribe();
                let filter: LoopBackFilter = {
                    where: { accountId: data.id, friendId: this.userId },
                }
                this._friendApi.findOne(filter).subscribe((following: any) => {
                    console.log(following);
                    if (following.mutual === true) {
                        this._friendApi.updateAttributes(following.id, { mutual: false }).subscribe((result) => {
                        }, (error) => {
                            alert(error.message);
                            console.error("Error While Updating Attributes -> " + error.message);
                        });
                    }
                }, (error) => {
                    alert(error.message);
                    console.error("Error While UnFollowing -> " + error.message);
                });
            }
        })
    }

    public followUserBack(data) {
        let friendObj = new Friend();
        friendObj.accountId = this.userId;
        friendObj.friendId = data.id;
        data.friendshipMutual = true;
        friendObj.approved = true;
        this._friendApi.create(friendObj).subscribe((result) => {
            this.follow.emit(data);
            this._friendApi.updateAttributes(data.friendshipId, { mutual: true }).subscribe();
        }, (error) => {
            alert(error.message);
            console.error("Error While Follow User Back -> " + error.message);
        });
    }


    public unfollowConfirmFromFollower(data) {
        dialogs.confirm({
            cancelButtonText: 'cancel',
            okButtonText: 'unfollow',
            title: 'Are you sure you want to unfollow ' + data.displayName + '?',
        }).then((result) => {
            if (result) {
                data.friendshipMutual = false;
                console.log(JSON.stringify(data));
                this.unfollow.emit(data);
                let filter: LoopBackFilter = {
                    where: { accountId: this.userId, friendId: data.id },
                    include: { "relation": "friend" },
                }
                this._friendApi.findOne(filter).subscribe((following: any) => {
                    console.log(JSON.stringify(following));
                    this._friendApi.deleteById(following.id).subscribe();
                    this._friendApi.updateAttributes(data.friendshipId, { mutual: false }).subscribe((result) => {
                    }, (error) => {
                        alert(error.message);
                        console.error("Error While Updating Attributes -> " + error.message);
                    })
                }, (error) => {
                    alert(error.message);
                    console.error("Error While Confirming UnFollow -> " + error.message);
                })
            }
        })
    }

    public followUser(data) {
        this.followShow = false;
        let friendObj = new Friend();
        friendObj.accountId = this.userId;
        friendObj.friendId = data.id;
        friendObj.approved = true;
        this._friendApi.create(friendObj).subscribe((result) => {
            this.follow.emit(data);
        }, (error) => {
            alert(error.message);
            console.error("Error While Following User -> " + error.message);
        });
    }

    public getAccountDetail(id) {
        this._routerExtensions.navigate(["/user-profile/" + id]);
    }


}
