import { Component, OnInit } from '@angular/core';
import { LoopBackFilter } from '../../shared/sdk/models/BaseModels';
import { AccountApi, FriendApi } from '../../shared/sdk/services';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterExtensions } from "nativescript-angular/router";
import { Subscription } from 'rxjs/Subscription';
import * as frameModule from "ui/frame";
import { isIOS } from "platform";

declare var com: any;
declare var FBSDKAppInviteContent: any;
declare var FBSDKAppInviteDialog: any;

@Component({
    selector: 'app-follow',
    moduleId: module.id,
    templateUrl: 'follow.component.html',
    styleUrls: ['follow.component.css']
})

export class followComponent implements OnInit {
    accountId: any;
    followList = [];
    users = [];
    count = 0;
    public friends = [];
    type = "";
    constructor(
        private accountApi: AccountApi,
        private routerExtensions: RouterExtensions,
        private route: ActivatedRoute, ) {
        this.route.params.subscribe((params) => {
            this.type = params['type'];
        });
    }
    ngOnInit() {
        /**  This 3 line are Hide Back Button From ActionBar */
        this.accountId = this.accountApi.getCurrentId();
        if (isIOS) {
            const controller = frameModule.topmost().ios.controller;
            // get the view controller navigation item
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);
        }

        const filter: LoopBackFilter = {
            include: [{ "relation": "friends", "scope": { "fields": ["Id"] } }]
        }
        this.accountApi
            .findById(this.accountId, filter)
            .subscribe((accoutDetail: any) => {
                if (accoutDetail.friends !== null)
                    this.friends = accoutDetail.friends;
                this.friends.push({ id: this.accountId });
            }, (error) => {
                alert(error.message);
                console.error("Error While Account Details -> " + error.message);
            });
        this.accountApi
            .find()
            .subscribe((accoutDetail: any) => {
                const data = accoutDetail;
                this.followList = [];
                data.forEach(follow => {
                    let isFriend: Boolean = false;
                    if (follow.photo !== undefined && follow.displayName !== undefined && follow.displayName !== "" &&
                        follow.firstName !== undefined && follow.lastName !== undefined && follow.photo !== null) {
                        this.friends.forEach(friend => {
                            if (follow.id === friend.id) {
                                isFriend = true;
                            }
                        })
                        if (!isFriend) {
                            follow.type = this.type;
                            this.followList.push(follow);
                        }
                    }
                });
                for (let i = 0; i < (this.followList.length < 12 ? this.followList.length : 12); i++) {
                    this.users.push(this.followList[i]);
                    this.count = i;
                }
            }, (error) => {
                alert(error.message);
                console.error("Error While Getting FollowList -> " + error.message);
            });
    }
    loadMoreItems() {
        const tempCouter = this.count;
        for (let i = tempCouter; i < tempCouter + 5 && i < this.followList.length; i++) {
            this.users[i] = this.followList[i];
            this.count = i;
        }
    }
    goToBack() {
        this.routerExtensions.back();
    }

    public followUser(data) {
        console.log(this.followList.findIndex((follow) => {
            return follow.id === data.id;
        }))
    }

    public findFB() {
        this.routerExtensions.navigate(['/find-facebook']);
    }
    public inviteAllFBFriends() {
        if (isIOS) {
            let appInviteDialog = FBSDKAppInviteDialog.alloc().init();
            if (appInviteDialog.canShow()) {
                let content = FBSDKAppInviteContent.alloc().init();
                content.appLinkURL = NSURL.URLWithString("http://heatcity.io/");
                content.appInvitePreviewImageURL = NSURL.URLWithString("http://heat.city/wp-content/themes/HeatTheme/images/heat_pink.png");
                appInviteDialog.content = content;
                appInviteDialog.delegate = this;
                appInviteDialog.show();
            } else {
                console.log('Invite dialog not available.');
            }
        } else {
            if (com.facebook.share.widget.AppInviteDialog.canShow()) {
                let content = new com.facebook.share.model.AppInviteContent.Builder()
                    .setApplinkUrl('http://heat.city/')
                    .setPreviewImageUrl('http://heat.city/wp-content/themes/HeatTheme/images/heat_pink.png')
                    .build();
                var act = app.android.foregroundActivity || app.android.startActivity;
                let appDialog = new com.facebook.share.widget.AppInviteDialog(act);
                appDialog.show(content);
            } else {
                console.log('Invite dialog not available.');
            }
        }

    }
}