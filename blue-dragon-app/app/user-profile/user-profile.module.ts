import { NgModule } from '@angular/core';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ShareModule } from "../share/share.module";
import { UserProfileComponent } from './user-profile.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { AddFriendsComponent } from './add-friends/add-friends.component';
import { userProfileRouting } from './user-profile.routing';
import { SharedModule } from '../shared/shared.module';
import { FriendsListModule } from '../friends-list/friends-list.module';
import { AppNotificationsModule } from '../app-notifications/app-notifications.module';

import { StorageNative } from '../shared/sdk/storage/storage.native';
import { NativeScriptUIListViewModule} from "nativescript-pro-ui/listview/angular";
import { editUserComponent } from './edit-user/edit-user.component';
import { followingComponent } from './following/following.component'
import { followCardComponent } from './follow-card/follow-card.component';
import { followComponent } from './follow/follow.component';
import { findFacebookFriendsComponent } from './find-facebook-friends/find-facebook-friends.component';

@NgModule({
    imports: [
        NativeScriptUIListViewModule,
        NativeScriptModule,
        NativeScriptFormsModule,
        userProfileRouting,
        ShareModule,
        SharedModule,
        FriendsListModule,
        AppNotificationsModule
    ],
    exports: [],
    declarations: [UserProfileComponent,
        editUserComponent,
        followingComponent,
        followCardComponent,
        followComponent,
        findFacebookFriendsComponent,
        UserSettingsComponent, AddFriendsComponent],
    providers: [StorageNative]
})
export class UserProfileModule { }
