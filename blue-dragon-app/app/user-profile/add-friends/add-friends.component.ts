import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, SimpleChange, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Account, Friend, FireLoopRef } from '../../shared/sdk/models';
import { RealTime, AccountApi, FriendApi } from '../../shared/sdk/services';
import { LoopBackConfig } from '../../shared/sdk';
import { BASE_URL, API_VERSION } from '../../shared/base.api';
import { AppNotificationsService } from '../../app-notifications/app-notifications.service';
import { SwissArmyKnife } from 'nativescript-swiss-army-knife';
import * as SocialShare from "nativescript-social-share";
import * as app from 'application';
import { isIOS } from 'platform';
import { Page } from 'ui/page';
import { ScrollView } from 'ui/scroll-view';
import { Image } from 'ui/image';
import { ImageSource, fromUrl, fromResource, fromFile } from "image-source";
import { LogoutService } from '../../shared/logout.service';
var contacts = require('nativescript-contacts');
import * as googleAnalytics from "nativescript-google-analytics";
// Android native package
declare var com: any;

// IOS Facebook SDK native class
declare var FBSDKGraphRequest: any;
declare var FBSDKLoginManager: any;
declare var FBSDKAppInviteContent: any;
declare var FBSDKAppInviteDialog: any;

//IOS native class
declare var NSURL: any;
declare var CFRunLoopGetMain: any;
declare var CFRunLoopPerformBlock: any;
declare var kCFRunLoopDefaultMode: any;
declare var CFRunLoopWakeUp: any;

let invokeOnRunLoop = (function () {
    if (isIOS) {
        var runloop = CFRunLoopGetMain();
        return function (func) {
            CFRunLoopPerformBlock(runloop, kCFRunLoopDefaultMode, func);
            CFRunLoopWakeUp(runloop);
        }
    }
}());

interface FriendRequestInterface {
    [key: string]: Account
}


@Component({
    selector: "AddFriendsComponent",
    templateUrl: "user-profile/add-friends/add-friends.component.html",
    styleUrls: ['user-profile/add-friends/add-friends-common.css', 'user-profile/add-friends/add-friends.component.css']
})
export class AddFriendsComponent implements OnInit, OnDestroy, AfterViewInit {
    public imagesLoadded: Array<boolean> = [];
    /** @type {Subscription[]} **/
    private subscriptions: Subscription[] = new Array<Subscription>();

    /** @type {Subscription[]} **/
    private searchInput: string = '';

    /** @type {Subscription} **/
    private addFriendsSearchSubscription: Subscription;

    /** @type {Account[]} **/
    private heatUsers: Account[];

    /** @type {Account[]} **/
    private fbFriends: Account[];

    /** @type {FriendRequestInterface} **/
    private sendRequestList: FriendRequestInterface = {};

    /** @type {string[]} **/
    private sendRequestListKeys: string[] = [];

    private counter: { typed: boolean, number: number } = {
        typed: false,
        number: 0
    };

    private counterInterval: any;

    private limit: number = 10;

    @Input('currentUser') currentUser: Account;

    @Output('closeFriendSearch') onClose: EventEmitter<boolean> = new EventEmitter<boolean>();


    /**
     * @method constructor
     * @param {Account} account Fireloop Account Service
     * @param {RealTime} realtime FireLoop RealTime Service
     **/
    constructor(
        private accountApi: AccountApi,
        private friendApi: FriendApi,
        private page: Page,
        private logoutService: LogoutService,
        private appNotifications: AppNotificationsService
    ) {
        //this.getFBFriends();
        // this.inviteAllFBFriends();
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        googleAnalytics.logView('add-friends');
    }

    /**
     * @method ngOnInit
     **/
    ngOnInit() {
        // this.search();
    }

    ngAfterViewInit(): void {
        let scrollView = this.page.getViewById<ScrollView>('heat-users');
        if (scrollView && SwissArmyKnife) {
            SwissArmyKnife.removeHorizontalScrollBars(scrollView);
        }
    }

    /**
     * @method search
     **/
    search(): void {
        this.searchHeatUsers();
    }

    /**
     * @method ngOnChanges
     **/
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['currentUser']) {
            this.currentUser = changes['currentUser'].currentValue;
        }
    }

    /**
     * @method startCounter
     **/
    startCounter() {
        if (!this.counterInterval) {
            this.counterInterval = setInterval(() => {
                if (this.counter.typed && this.counter.number < 4) {
                    this.counter.number += 1;
                } else {
                    this.counter.typed = false;
                    this.search();
                    clearInterval(this.counterInterval);
                    this.counterInterval = null;
                }
            }, 333);
        }
    }

    /**
     * @method searchHeatUsers
     **/
    searchHeatUsers(): void {
        let id = this.accountApi.getCurrentId();
        if (this.addFriendsSearchSubscription) {
            this.addFriendsSearchSubscription.unsubscribe();
        }
        this.addFriendsSearchSubscription = this.accountApi.addFriendsSearch(id, this.searchInput, this.limit).subscribe(
            (accounts: Account[]) => {
                this.heatUsers = accounts;
            },
            error => this.errorHandler(error)
        );
    }

    /**
     * @method loadMore
     **/
    loadMore(): void {
        this.limit += 10;
        this.searchHeatUsers();
    }

    /**
     * @method searchFBFriendsAccoutns
     **/
    searchFBFriendsAccoutns(): void {
        let id = this.accountApi.getCurrentId();
        if (this.searchInput && this.searchInput != '') {
            this.subscriptions.push(
                this.accountApi.searchFacebookFriendsAccounts(id, this.searchInput).subscribe(
                    (accounts: Account[]) => { this.fbFriends = accounts },
                    error => this.errorHandler(error)
                )
            );
        }
    }

    /**
     * @method inviteAllFBFriends
     **/
    inviteAllFBFriends(): void {
        let appInviteDialog;
        if (isIOS) {
            invokeOnRunLoop(() => {
                appInviteDialog = new FBSDKAppInviteDialog().alloc().init();;
                if (appInviteDialog.canShow()) {
                    let content = FBSDKAppInviteContent.alloc().init();
                    content.appLinkURL = NSURL.URLWithString("http://heat.city/");
                    content.appInvitePreviewImageURL = NSURL.URLWithString("http://heat.city/wp-content/themes/HeatTheme/images/heat_pink.png");
                    appInviteDialog.content = content;
                    appInviteDialog.delegate = this;
                    appInviteDialog.show();
                } else {
                    console.log('Invite dialog not available.');
                }
            });
        } else {
            if (com.facebook.share.widget.AppInviteDialog.canShow()) {
                let content = new com.facebook.share.model.AppInviteContent.Builder()
                    .setApplinkUrl('http://heat.city/')
                    .setPreviewImageUrl('http://heat.city/wp-content/themes/HeatTheme/images/heat_pink.png')
                    .build();
                var act = app.android.foregroundActivity || app.android.startActivity;
                let appDialog = new com.facebook.share.widget.AppInviteDialog(act);
                appDialog.show(content);
            } else {
                console.log('Invite dialog not available.');
            }
        }
    }

    /**
     * @method getFBFriends
     **/
    getFBFriends(): void {
        let graph;
        if (isIOS) {
            invokeOnRunLoop(() => {
                graph = new FBSDKGraphRequest({ graphPath: "me/friends", parameters: null });
                graph.startWithCompletionHandler((connection, result, error) => {
                    if (error) {
                        alert("Error! can not get user info!");
                    } else {
                        let fbFriends = result.valueForKey("data");
                        this.saveFBFriendList(fbFriends);
                    }
                })
            });
        } else {
            let graph = new com.facebook.GraphRequest.newMeRequest(
                com.facebook.AccessToken.getCurrentAccessToken(),
                new com.facebook.GraphRequest.GraphJSONObjectCallback({
                    onCompleted: (me, response) => {
                        if (response.getError() != null) {
                            console.log(response.getError());
                        } else {
                            let fbFriends = me.optString('friends');
                            fbFriends = JSON.parse(fbFriends).data;
                            this.saveFBFriendList(fbFriends);
                        }
                    }
                })
            );
            let bundle = graph.getParameters();
            bundle.clear();
            bundle.putString("fields", "id,friends");
            graph.setParameters(bundle);
            graph.executeAsync();
        }
    }

    /**
     * @method saveFBFriendList
     **/
    saveFBFriendList(fbFriends: any): void {
        let fbIds: any[] = [];
        if (Array.isArray(fbFriends)) {
            fbIds = fbFriends.map((fbFriend) => {
                if (fbFriend.id) {
                    return { facebookId: fbFriend.id };
                }
            });
        }
        let id = this.accountApi.getCurrentId();
        this.subscriptions.push(
            this.accountApi.updateAttributes(id, {
                fbFriendList: fbIds
            }).subscribe(
                (account: Account) => console.log('fbFriendsList updated!'),
                error => this.errorHandler(error)
                )
        );
    }

    /**
     * @method inviteAllPhoneContacts
     **/
    inviteAllPhoneContacts(): void {
        alert('Sending invitation to phone contacts');
        contacts.getAllContacts().then((args: any) => {
            let numbers: string[] = []
            if (args.data && Array.isArray(args.data)) {
                args.data.forEach((contact: any) => {
                    if (Array.isArray(contact.phoneNumbers) && contact.phoneNumbers.length > 0) {
                        contact.phoneNumbers.forEach(phone => {
                            if (phone.value.search(/\+.*/) == 0) {
                                numbers.push(phone.value);
                            }
                        });
                    }
                });
            }
            this.sendSMSInvitation(numbers);
        }, (err: Error) => console.log(err.message));
    }

    /**
     * @method inviteAllPhoneContacts
     **/
    sendSMSInvitation(numbers: string[]): void {
        let currentId = this.accountApi.getCurrentId();
        this.subscriptions.push(
            this.accountApi.sendSMSInvitation(currentId, numbers).subscribe(
                (res: boolean) => alert("Successfully invited all phone contacts!"),
                error => this.errorHandler(error)
            )
        );
    }

    /**
     * @method inviteExternalContacts
     **/
    inviteExternalContacts(): void {
        SocialShare.shareText(
            `${this.currentUser.displayName} has invited you to download HeatCity app. Signup using this link for IOS: https://goo.gl/Oo4EPS, for Android: https://goo.gl/vrM9oX`,
            "Send invitation with..."
        );
    }

    /**
     * @method inviteAllPhoneContacts
     **/
    addToSendRequestList(account: Account): void {
        if (this.sendRequestList[account.id]) {
            alert('User is already added to list.');
        } else {
            this.sendRequestList[account.id] = account;
            this.sendRequestListKeys = Object.keys(this.sendRequestList);
        }
        this.dismissKeyboard();
    }

    /**
     * @method inviteAllPhoneContacts
     **/
    sendFriendRequest(): void {
        let id = this.accountApi.getCurrentId();
        let friends = this.sendRequestListKeys.map((friendId: string) => {
            return {
                accountId: id,
                friendId: friendId,
                approved: true
            }
        });
        this.subscriptions.push(
            this.friendApi.create(friends).subscribe(
                (friends: Friend[]) => {
                    let friendsName = this.getRequestedFriendsName(this.sendRequestListKeys.length);
                    this.appNotifications.displayNotification(
                        'user-profile-notifications',
                        { text: 'Friend request sent to ' + friendsName, status: true }
                    );
                    this.sendRequestListKeys = [];
                    this.sendRequestList = {};
                    this.search();
                },
                error => this.errorHandler(error)
            )
        )
    }

    /**
     * @method getRequestedFriendsName
     **/
    getRequestedFriendsName(size: number): string {
        let friendsNames: string = '';
        if (this.sendRequestListKeys.length > 0) {
            let account = this.sendRequestList[this.sendRequestListKeys[0]];
            if (account.firstName) {
                friendsNames = account.firstName;
            } else if (account.displayName) {
                friendsNames = account.displayName.split(' ')[0];
            } else if (account.username) {
                friendsNames = account.username.split(' ')[0];
            }
        }
        for (let i = 1; i < this.sendRequestListKeys.length && i < size; i++) {
            let friend = this.sendRequestList[this.sendRequestListKeys[i]];
            if (friend.firstName) {
                friendsNames += ', ' + friend.firstName;
            } else if (friend.displayName) {
                friendsNames += ', ' + friend.displayName.split(' ')[0];
            } else if (friend.username) {
                friendsNames += ', ' + friend.username.split(' ')[0];
            }
        }
        return friendsNames;
    }

    /**
     * @method onModelChanged
     **/
    onModelChanged() {
        this.counter.number = 0;
        this.counter.typed = true;
        this.startCounter();
    }

    dismissKeyboard(): void {
        let searchInput: any = this.page.getViewById('search-friends');
        if (searchInput) {
            searchInput.dismissSoftInput();
        }
    }

    /**
     * @method close
     **/
    close() {
        if (this.counterInterval) {
            this.counter.typed = false;
            clearInterval(this.counterInterval);
            this.counterInterval = null;
        }
        this.dismissKeyboard();
        this.onClose.emit(true);
    }

    /**
     * @method ngOnDestroy
     **/
    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
        if (this.addFriendsSearchSubscription) {
            this.addFriendsSearchSubscription.unsubscribe();
        }
    }

    errorHandler(error: any): void {
        console.log('Add friends Error', JSON.stringify(error));
        if (error && error.statusCode === 401) {
            this.logoutService.logout();
        }
    }

    load(path: string, index: number) {
        let id = `img-${index}`;
        fromUrl(path).then((source: ImageSource) => {
            this.setSource(id, source)
        }, (err) => {
            let source = fromResource('bmarker_profile_m')
            this.setSource(id, source);
        })
    }

    setSource(id: string, source: ImageSource, tries = 3): void {
        setTimeout(() => {
            let img = this.page.getViewById<Image>(id);
            if (img) {
                img.imageSource = source;
            } else {
                if (tries >= 0) {
                    this.setSource(id, source, tries - 1);
                }
            }
        }, 20)
    }
}
