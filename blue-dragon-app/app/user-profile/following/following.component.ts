import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountApi, FriendApi } from '../../shared/sdk';
import { LoopBackFilter } from '../../shared/sdk/models/BaseModels';
import { RouterExtensions } from 'nativescript-angular';
import * as frameModule from "ui/frame";
import { isIOS } from "platform";
import { LoadingIndicator } from "nativescript-loading-indicator";

declare var  UITableViewCellSelectionStyle :any;
@Component({
    selector: 'app-following',
    moduleId: module.id,
    templateUrl: 'following.component.html',
    styleUrls: ['following.component.css']
})

export class followingComponent implements OnInit {
    private type = "";
    private dataArray = [];
    private userid = this._accountApi.getCurrentId();
    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
        loading: false,
        instance: new LoadingIndicator(),
        options: {
            message: 'Loading...',
            progress: 0,
            android: {
                indeterminate: true,
                cancelable: false,
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            },
            ios: {
                square: false,
                margin: 10,
                dimBackground: true,
                color: "#fff",
                mode: 'MBProgressHUDModeText'// see iOS specific options below
            }
        }
    };
    private id;
    private accountId;
    private self;

    constructor(private router: Router, private route: ActivatedRoute,
        private _accountApi: AccountApi,
        private _friendApi: FriendApi,
        private routerExtensions: RouterExtensions) {
        this.route.params
            .subscribe((params) => {
                if (params['data']) {
                    let data = JSON.parse(params['data']);
                    console.log(params['data']);
                    this.type = data.type;
                    this.accountId = data.accountId;
                }
            });
    }

    ngOnInit() {
        this.loader.instance.show(this.loader.options);
        if (this.type === "Following") {
            this.findFollowing();
        } else if (this.type === "Follower") {
            this.findFollwers();
        }
        if(this.userid === this.accountId) {
            this.self = true;
        } else {
            this.self = false;
        }
        /**  This 3 line are Hide Back Button From ActionBar */
        if (isIOS) {
            const controller = frameModule.topmost().ios.controller;
            // get the view controller navigation item
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);
        }
        this.loader.instance.hide();
    }

    actionUser(data) {
        if (this.type === "Following") {
            this.dataArray.splice(this.dataArray.findIndex((arraydata) => {
                return (data.friendshipId === arraydata.friendshipId);
            }), 1);
        } else if (this.type === "Follower") {
            let index = this.dataArray.findIndex((arraydata) => {
                return (data.friendshipId === arraydata.friendshipId);
            });
            this._friendApi.updateAttributes(this.dataArray[index].friendshipId, { mutual: false });
        }
    }

    public followUser(data) {
        console.log(data);
    }
    public listViewItemLoading(args): void {
        if (isIOS) {

            args.ios.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }

    public findFollwers() {
        let filter: LoopBackFilter = {
            where: { "friendId": this.accountId },
            include: ['account'],
        }
        this._friendApi.find(filter).subscribe((followers: any) => {
            let data = followers.map((follower) => {
                follower.account.friendshipId = follower.id;
                follower.account.friendshipMutual = follower.mutual;
                follower.account.type = 'follower';
                follower.account.self = this.self;
                return follower.account;
            });
            this.dataArray = data;
        }, (error) => {
            alert(error.message);
            console.error("Error While Finding Followers -> " + error.message);
            this.loader.instance.hide();
        });
    }

    public findFollowing() {
        let filter: LoopBackFilter = {
            where: { "accountId": this.accountId },
            include: { "relation": "friend" },
        }

        this._friendApi.find(filter).subscribe((following: any) => {
            let data = following.map((following) => {
                following.friend.friendshipId = following.id;
                following.friend.friendshipMutual = following.mutual;
                following.friend.type = 'following';
                following.friend.self = this.self;
                return following.friend;
            });
            this.dataArray = data;
        }, (error) => {
            alert(error.message);
            console.error("Error While Finding Followings -> " + error.message);
            this.loader.instance.hide();
        });
    }

    public goToBack() {
        this.routerExtensions.back();
    }

    public follow() {
        this.routerExtensions.navigate(['/follow', {type: "follow"}]);
    }
}