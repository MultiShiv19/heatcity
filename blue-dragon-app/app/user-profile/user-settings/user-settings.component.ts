import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterExtensions } from "nativescript-angular/router";
import { Subscription } from 'rxjs/Subscription';
import { Account } from '../../shared/sdk/models';
import { AccountApi } from '../../shared/sdk/services';
import { LoopBackConfig } from '../../shared/sdk';
import { BASE_URL, API_VERSION } from '../../shared/base.api';
import { openUrl } from 'utils/utils';
import { StorageNative } from '../../shared/sdk/storage/storage.native';
import { isIOS } from 'platform';
import * as app from 'application';
import * as googleAnalytics from "nativescript-google-analytics";
import { LogoutService } from '../../shared/logout.service';
var GPlaces = require("nativescript-google-places");
import { topmost } from "ui/frame";
import { LoadingIndicator } from "nativescript-loading-indicator";
import { AuthGuard } from "../../auth-guard.service"

const UNITS = {
    meters: 'meters',
    miles: 'miles'
}

@Component({
    selector: "UserSettingsComponent",
    templateUrl: "user-profile/user-settings/user-settings.component.html",
    styleUrls: ['user-profile/user-settings/user-settings-common.css', 'user-profile/user-settings/user-settings.component.css']
})
export class UserSettingsComponent {
    /** @type {Subscription[]} **/
    private subscriptions: Subscription[] = new Array<Subscription>();

    /** @type {Account} **/
    // @Input('account') account: any;
    public account: any;

    /** @type {EventEmitter<boolean>} **/
    // @Output('closeSettings') closeSettings: EventEmitter<any> = new EventEmitter<any>();
    public closeSettings: EventEmitter<any> = new EventEmitter<any>();


    /** @type {boolean} **/
    private showModal: boolean = false;

    /** @type {string} **/
    private searchAddress: string;

    private searchAddressResults: string[] = [];

    private exclution: { name?: string, address?: string, geopoint?: { lat: string, lng: string } } = {};

    private counter: { typed: boolean, number: number } = {
        typed: false,
        number: 0
    };

    private counterInterval: any;

    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                progress: 0,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'// see iOS specific options below
                }
            }
        };
    private accountSettings: {
        notifications?: boolean;
        locationServices?: boolean;
        shareLocation?: boolean;
        unit?: string;
    } = {
            notifications: true,
            locationServices: true,
            shareLocation: true,
            unit: UNITS.meters
        };

    constructor(
        private accountApi: AccountApi,
        private router: Router,
        private route: ActivatedRoute,
        private storageNative: StorageNative,
        private logoutService: LogoutService,
        private http: Http,
        private routerExtensions: RouterExtensions,
        private authGuard: AuthGuard,
    ) {
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        googleAnalytics.logView('user-settings');
        GPlaces.init({
            googleServerApiKey: 'AIzaSyAaGsoyNTNWIgJJwCUn9sagg3bvyvQbRfc',
            language: 'us',
            radius: '100000',
            location: '20.611854969256648,-103.4158559727287',
            errorCallback: (text) => { console.log(text) }
        });

        this.route.params
            .subscribe((params) => {
                if (params['account']) {
                    this.account = JSON.parse(params['account']);
                    // console.log(JSON.stringify(this.account));
                    try {
                        if (this.account.settings !== null && this.account.settings !== "" && this.account.settings !== undefined) {
                            this.accountSettings = this.account.settings;
                        }
                        this.accountSettings;

                    } catch (error) {
                        this.accountSettings;
                    }

                }
            });

        this.isSelected(this.accountSettings.unit);
    }


    public ngOnInit() {
        this.loader.instance.show(this.loader.options);
        if (this.account && this.account.settings)
            this.accountSettings = this.account.settings;

        if (topmost().ios) {
            // get the view controller navigation item
            const controller = topmost().ios.controller;
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);
        }
        this.loader.instance.hide();
    }
    public goToBack() {
        this.routerExtensions.back();
    }

    public close() {
        console.log("closeSetting")
    }


    public onNotificationsChange(value: boolean): void {
        this.accountSettings.notifications = value;
        this.updateAccountSettings();
    }


    public onLocationServicesChange(value: boolean): void {
        this.accountSettings.locationServices = value;
        this.updateAccountSettings();
    }


    public onShareLocationChange(value: boolean): void {
        this.accountSettings.shareLocation = value;
        this.updateAccountSettings();
    }

    public onTapPrivacyStatement() {
        openUrl('http://api.heat.city/privacy-policy.html')
    }


    public onTapTermsOfService() {
        openUrl('http://api.heat.city/tos.html');
    }

    isSelected(unit: string): boolean {
        let isSelected: boolean = false;
        if (unit == UNITS.meters) {
            return (!this.accountSettings.unit || this.accountSettings.unit === unit) ?
                true : false;
        } else {
            return this.accountSettings.unit === unit;
        }
    }

    onTapDistance(unit: string): void {
        this.accountSettings.unit = unit;
        this.isSelected(unit);
        this.updateAccountSettings();
    }


    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }



    public updateAccountSettings() {
        this.subscriptions
            .push(this.accountApi
                .updateAttributes(this.account.id, { settings: this.accountSettings })
                .subscribe((accountRes) => {
                    this.account = accountRes;
                    error => this.errorHandler(error);
                }, (error) => {
                    alert(error.message);
                    console.error("Error While Updating Account Settings -> " + error.message);
                    this.loader.instance.hide();
                })
            )
    }


    public logout() {
        this.subscriptions.push(
            this.accountApi.logout().subscribe(
                (res) => {
                    this.storageNative.remove('facebookToken');
                    this.storageNative.remove('invitationCode');
                    this.storageNative.remove('notifications');
                    this.router.navigate(['/login']);
                }, err => {
                    // alert(err.message);
                    console.error('Error While Logout ->' + err.message);
                    // this.storageNative.remove('facebookToken');
                    // this.storageNative.remove('invitationCode');
                    // this.storageNative.remove('notifications');
                    this.router.navigate(['/login'])
                })
        );
        const accountId = undefined;
        this.authGuard.loginAccount(accountId);
    }


    /* This Feature is not implemented */
    public saveFrequentPlace(): void {
        if (!this.exclution.name || !this.exclution.address) {
            alert("Please provide all fields to continue");
        } else {
            if (this.account.exclutions && Array.isArray(this.account.exclutions)) {
                this.account.exclutions.push(this.exclution);
            } else {
                this.account.exclutions = [this.exclution];
            }
            this.subscriptions.push(
                this.accountApi.updateAttributes(this.account.id, {
                    exclutions: this.account.exclutions
                }).subscribe(
                    (res: Account) => {
                        this.closeModal();
                    }, (error) => {
                        alert(error.message);
                        error => this.errorHandler(error)
                        console.error("Error While Saving Frequent Places -> " + error.message);
                        this.loader.instance.hide();
                    }

                    )
            );
        }
    }


    public getGeopoint() {
        let result = this.searchAddress.split(/\ /).join('+');
        this.subscriptions.push(
            this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${result}&key=AIzaSyAaGsoyNTNWIgJJwCUn9sagg3bvyvQbRfc`)
                .subscribe(
                (res: any) => {
                    if (res._body.results && Array.isArray(res._body.results) && res._body.results.length > 0) {
                        this.exclution.geopoint = res._body.results[0].geometry.location;
                    }
                    this.saveFrequentPlace();
                },
                (err: Error) => {
                    alert(err.message);
                    console.error('Error While Getting GeoPoint ->' + err.message);
                    this.loader.instance.hide();
                    this.saveFrequentPlace();
                }
                )
        );
    }


    public placeAutocomplete(): void {
        if (this.searchAddress != '') {
            GPlaces.queryAutoComplete(this.searchAddress, null).then((result) => {
                this.searchAddressResults = result.map((res) => {
                    return res.description;
                });
            });
        } else {
            this.searchAddressResults = [];
        }
    }


    public startCounter() {
        if (!this.counterInterval) {
            this.counterInterval = setInterval(() => {
                if (this.counter.typed && this.counter.number < 4) {
                    this.counter.number += 1;
                } else {
                    this.counter.typed = false;
                    this.placeAutocomplete();
                    clearInterval(this.counterInterval);
                    this.counterInterval = null;
                }
            }, 333);
        }
    }



    /**
     * @method onModelChanged
     **/
    onModelChanged() {
        this.exclution.address = this.searchAddress;
        this.counter.number = 0;
        this.counter.typed = true;
        this.startCounter();
    }

    /**
     * @method onModelChanged
     **/
    closeModal() {
        this.showModal = false;
        this.searchAddress = '';
        this.searchAddressResults = [];
        this.exclution = {};
    }



    errorHandler(error: any): void {
        console.log('User settings Error', JSON.stringify(error));
        if (error && error.statusCode === 401) {
            this.logoutService.logout();
        }
    }
}
