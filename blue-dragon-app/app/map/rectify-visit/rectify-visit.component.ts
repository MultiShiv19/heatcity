import { Component, Input, Output, OnChanges, OnDestroy, EventEmitter } from '@angular/core';
import { EstablishmentApi, VisitApi } from '../../shared/sdk/services';
import { Visit, Establishment } from '../../shared/sdk/models';
import { Page } from 'ui/page';
import { LogoutService } from '../../shared/logout.service';
import * as moment from 'moment';
import * as googleAnalytics from "nativescript-google-analytics";
import {View} from 'ui/core/view';

@Component({
  moduleId: module.id,
  selector: 'RectifyVisit',
  templateUrl: 'rectify-visit.component.html',
  styleUrls: ['rectify-visit-common.css']
})

export class RectifyVisitComponent implements OnChanges, OnDestroy {
  private establishments: Establishment[];

  @Input('visit') visit: Visit;
  @Output('reject') onReject: EventEmitter<Visit> = new EventEmitter<Visit>();
	@Output('rectify') onRectify: EventEmitter<any> = new EventEmitter<any>();
  @Output('close') onClose: EventEmitter<any> = new EventEmitter<any>();

  /**
   * @method constructor
   * @param {VisitApi} visitApi description
   **/
  constructor(
    private establishmentApi: EstablishmentApi,
    private visitApi: VisitApi,
    private logoutService: LogoutService,
    private page: Page
  ) {
    googleAnalytics.logView('rectify-visit');
  }

  ngOnChanges() {
    if(this.visit && this.visit.location && this.visit.location.establishment) {
      this.getNearEstablishments();
    } else {
      this.getVisitInstance();
    }
  }

  getVisitInstance() {
    this.visitApi.findById(this.visit.id, {
      include: {
        location: {
            relation: 'establishment',
            scope: {
                fields: [
                    'id',
                    'name',
                    'description',
                    'geoLocation',
                    'address',
                    'categories',
                    'locationId'
                ],
                include: 'photos'
            }
        }
      }
    }).subscribe(
      (visit: Visit) => {
        this.visit = visit;
        this.getNearEstablishments();
      },
      error => this.errorHandler(error)
    )
  }

  getNearEstablishments() {
    this.establishmentApi.find({
      where: {
        geoLocation: {
          nearSphere:{
            $geometry:{
              type: "Point",
              coordinates: this.visit.geoLocation.coordinates
            },
            $maxDistance: 100,
            $minDistance: 0
          }
        },
        id: {
          nlike: this.visit.location.establishment.id
        }
      }
    }).subscribe(
      (establishments: Establishment[]) => {
        this.establishments = establishments;
      },
      error => this.errorHandler(error)
    )
  }

  rectify(establishment: Establishment) {
    let gridLayout = <View>this.page.getViewById('rectify-component');
    this.removeLabel();
      if(gridLayout) {
          gridLayout.animate({
              opacity: 0,
              translate: { x: 0, y: 600},
              duration:500
          }).then(() => {
              this.onRectify.next({visit: this.visit, establishment: establishment});
          });
      } else {
        this.onRectify.next({visit: this.visit, establishment: establishment});
      }
  }

  reject() {
    let gridLayout = <View>this.page.getViewById('rectify-component');
    this.removeLabel()
    if(gridLayout) {
        gridLayout.animate({
            opacity: 0,
            translate: { x: 0, y: 600},
            duration:500
        }).then(() => {
          this.onReject.next(this.visit);
        });
    } else {
      this.onReject.next(this.visit);
    }
  }

  close() {
    let gridLayout = <View>this.page.getViewById('rectify-component');
    this.removeLabel()
    if(gridLayout) {
        gridLayout.animate({
            opacity: 0,
            translate: { x: 0, y: 600},
            duration:500
        }).then(() => {
          this.onClose.next();
        });
    } else {
      this.onClose.next();
    }
  }

  removeLabel() {
    let gridLayout = <View>this.page.getViewById('rectify-label');
    if(gridLayout) {
        gridLayout.animate({
            opacity: 0,
            duration:500
        })
    }
  }

  templateSelector = (item: any, index: number, items: any[]) => {
    return 'RECTIFY';
  }

  ngOnDestroy() {
    console.log('It works before the ngIf removes the element');
  }
  
  errorHandler(error: any): void {
    console.log('Rectify visit Error', JSON.stringify(error));
    if (error && error.statusCode === 401) {
      this.logoutService.logout();
    }
  }
}
