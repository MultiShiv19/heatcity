import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Animation } from "ui/animation";
import { AnimationCurve } from "ui/enums";
import * as gestures from 'ui/gestures';
import * as platform from 'platform';
import { topmost } from "ui/frame";

export interface CarouselItem {
    index: number,
    item: any
}

@Component({
    moduleId: module.id,
    selector: 'CardCarousel',
    templateUrl: 'card-carousel.component.html',
    styleUrls: ['card-carousel.component.css']
})

export class CardCarousel implements OnInit {

    @Input('pageWidth') public pageWidth: number;
    @Input('pageHeight') public pageHeight: number;

    /** @type {CarouselItem} */
    public previous: CarouselItem;

    /** @type {CarouselItem} */
    public current: CarouselItem;

    /** @type {CarouselItem} */
    public next: CarouselItem;

    /** @type {enabled: boolean} */
    private panning: { enabled: boolean } = { enabled: false };

    /** @type { Marker[] } */
    @Input('list') list: any[];

    /** @type { CarouselItem } */
    @Input('default') default: CarouselItem;

    /** @type { EventEmitter } */
    @Output('onItemFocus') onItemFocusEmitter: EventEmitter<CarouselItem> = new EventEmitter<CarouselItem>();

    /** @type { EventEmitter } */
    @Output('onItemTap') onItemTapEmitter: EventEmitter<CarouselItem> = new EventEmitter<CarouselItem>();

    @Output('onItemSwipeTop') onItemSwipeTop: EventEmitter<CarouselItem> = new EventEmitter<CarouselItem>();


    constructor() { }

    ngOnInit() {
        console.log('CAROUSEL LOADED');
        this.pageWidth = this.pageWidth ? this.pageWidth : platform.screen.mainScreen.widthDIPs;
        this.pageHeight = this.pageHeight ? this.pageHeight : platform.screen.mainScreen.heightDIPs;
    }

    ngOnChanges() {
        if (this.default) {
            this.current = this.default;
            let next_index: number = this.default.index + 1;
            this.next = {
                item: this.list[next_index],
                index: next_index
            };
        } else {
            this.current = {
                item: this.list[0],
                index: 0
            };
        }
        if (this.list)
            console.log(this.list.length);
    }

    onItemTap() {
        this.onItemTapEmitter.next(this.current);
    }

    onItemSwipe(args: gestures.SwipeGestureEventData): void {
        if (args.direction == gestures.SwipeDirection.up) {
            console.log(args.eventName, args.direction);
            this.onItemSwipeTop.next(this.current);
        }
    }
    /**
     * @method onItemPan
     * @return {void}
     **/
    onItemPan(args: gestures.PanGestureEventData) {
        if (
            args.state === gestures.GestureStateTypes.ended &&
            args.deltaY < -50 &&
            (args.deltaX < 15 || args.deltaX > -15)
        ) {
            args.view.translateX = 0;
            args.view.translateY = 0;
            args.view.animate({
                target: args.view,
                translate: { x: 0, y: 0 },
                duration: 50,
                curve: AnimationCurve.easeOut
            }).then(() => {
                this.onItemSwipeTop.next(this.current)
            })
            return;
        }
        if (this.panning.enabled || this.list.length < 2) {
            return;
        }
        let index: number = 0;
        /*
        console.log('PAGE WIDTH: ', this.pageWidth);
        console.log('DELTA: ', args.deltaX);
        console.log('YO: ', args.state.toString());
        */

        if (args.state === gestures.GestureStateTypes.began) {
			//alert(1);
            console.log('STARTED NAVIGATION');
        } else if (args.state === gestures.GestureStateTypes.changed) {
            args.view.animate({
                target: args.view,
                translate: { x: args.deltaX, y: 0 },
                duration: 0,
                curve: AnimationCurve.easeOut
            });
        } else if (args.state === gestures.GestureStateTypes.ended) {
            // swiping left to right.
            if (args.deltaX > (this.pageWidth / 2.5)) {
                args.view.animate({ target: args.view, translate: { x: this.pageWidth, y: 0 }, duration: 150, curve: AnimationCurve.easeOut })
                    .then(() => {
                        if (this.current) {
                            index = this.current.index - 1;
                            // when user swipes to the beginning, we start again from end.
                            if (index < 0) {
                                index = this.list.length - 1;
                            }
                            if(this.list[index].userData.establishment.id == this.current.item.userData.establishment.id) {
                                index -= 1;
                                    if (index < 0) {
                                    index = this.list.length - 1;
                                }
                            }
                        }
                        this.current = {
                            item: this.list[index],
                            index: index
                        };
                        this.onItemFocusEmitter.next(this.current);
                        args.view.translateX = -this.pageWidth;
                        return args.view.animate({ target: args.view, translate: { x: 0, y: 0 }, duration: 150, curve: AnimationCurve.easeOut });
                    });
                return;
            }
            // swiping right to left
            else if (args.deltaX < (-this.pageWidth / 2.5)) {
                args.view.animate({ target: args.view, translate: { x: -this.pageWidth, y: 0 }, duration: 150, curve: AnimationCurve.easeOut })
                    .then(() => {
                        if (this.current) {
                            index = this.current.index + 1;
                            // when user swipes to the beginning, we start again from end.
                            if (index === this.list.length) {
                                index = 0;
                            }
                            if(this.list[index].userData.establishment.id == this.current.item.userData.establishment.id) {
                                index += 1;
                                if (index === this.list.length) {
                                    index = 0;
                                }
                            }
                        }
                        this.current = {
                            item: this.list[index],
                            index: index
                        };
                        this.onItemFocusEmitter.next(this.current);
                        args.view.translateX = this.pageWidth;
                        return args.view.animate({ target: args.view, translate: { x: 0, y: 0 }, duration: 150, curve: AnimationCurve.easeOut });
                    });
                return;
            }
            // returning to center
            else {
                args.view.animate({ target: args.view, translate: { x: 0, y: 0 }, duration: 250, curve: AnimationCurve.easeOut })
            }
        }
    }
  }
