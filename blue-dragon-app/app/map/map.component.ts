import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, NgZone, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { isIOS } from "platform";
import { Router, ActivatedRoute } from "@angular/router";
import { Http } from '@angular/http';
import { PageRoute } from "nativescript-angular/router";
import geolocation = require('nativescript-geolocation');
import timer = require('timer');
import * as moment from 'moment';
import * as appSettings from "application-settings";
import { Moment } from 'moment';
import { Page } from 'ui/page';
//import { Observable } from 'rxjs/Observable';
import { Observable } from 'data/observable';
import { Subject } from 'rxjs/Subject';
import "rxjs/add/operator/switchMap";
import { Subscription } from 'rxjs/Subscription';
import { MapView, Marker, Polyline, Position } from 'nativescript-google-maps-sdk';
var dayStyle = require('./map-style.json');
var nightStyle = require('./map-style-night.json');
import { Image } from "ui/image";
import { ImageSource, fromUrl, fromResource, fromFile } from "image-source";
import {
    LoopBackFilter, Area, Establishment,
    EstablishmentPhoto, FireLoopRef, Location,
    FavoriteList, Visit, Notification
} from '../shared/sdk/models';
import {
    RealTime, AreaApi, EstablishmentApi, LocationApi,
    AccountApi, FavoriteListApi, VisitApi, LocationFixApi
} from '../shared/sdk/services';
import { Account } from '../shared/sdk/models';
import { StorageNative } from '../shared/sdk/storage/storage.native';
import { AnimationCurve } from "ui/enums";
var dialogs = require("ui/dialogs");
import { Accuracy } from "ui/enums";
import { LoopBackConfig } from '../shared/sdk';
import { DEFAULTS } from '../shared/config';
import { BASE_URL, API_VERSION } from '../shared/base.api';
import { CriteriaInterface } from './search/search.component';
import { CategoriesMap } from '../shared/category-marker.service';
import { RouterExtensions } from "nativescript-angular/router";
import { LoadingIndicator } from "nativescript-loading-indicator";
import { NotificationsService } from '../notifications/notifications.service';
import * as application from "application";
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import { CarouselItem } from './card-carousel/card-carousel.component';
//import * as googleAnalytics from "nativescript-google-analytics";
import { HistoryService } from './history/history.service';
import { VisitDetectionService } from './visit-detection/visit.detection.service';
import { MenuInfoInterface } from '../shared/interfaces/menu-info.interface';
import { LogoutService } from '../shared/logout.service';
import { View } from 'ui/core/view';
import { RadSideDrawerComponent, SideDrawerType } from "nativescript-pro-ui/sidedrawer/angular";
import { RadSideDrawer } from 'nativescript-pro-ui/sidedrawer';


import { topmost } from "ui/frame";
declare var UIImage: any;
declare var UIBarMetrics: any;
declare const UIBackgroundFetchResult: any;

//
// import { BackgroundGeolocation } from "nativescript-background-geolocation-lt";
import { BackgroundFetch } from "nativescript-background-fetch";
import BGService from "../lib/BGService";

export interface DistancesInterface {
    centerFromUser?: number,
    selectedEstablishmentFromUser?: number,
    closestEstablishmentFromUser?: number,
    closestEstablishmentFromCenter?: number,
}

export interface SettingsInterface {
    TRY_RECONNECTION_AFTER: number,
    AREA_DISTANCE: number,
    MAX_AREA_RESULTS: number,
    UPDATE_AFTER_DISTANCE: number,
    HOLD_CAMERA_FOR: number,
    WAIT_UNTIL_NEXT_MARKER: number,
    LOAD_NEARBY_MARKER: number
    WIPE_MARKERS: boolean,
    CUSTOM_MARKERS: boolean,
    CALCULATE_DISTANCES: boolean,
    SET_MIN_ZOOM: number,
    SET_MAX_ZOOM: number
}

export interface CardSettings {
    start: number[],
    end: number[],
    radius: number
}

export interface MapStates {
    CLEAN_LOAD: boolean,
    BACK_LOAD: boolean,
    LOCATION_LOAD: boolean,
    HISTORY_MAP_LOAD: boolean,
    HISTORY_LIST_LOAD: boolean,
    FARAWAY_SEARCH_LOAD: boolean
}

export interface VisitEstablishment {
    establishment: Establishment,
    visit?: Visit,
    index?: number
}

declare var GMSCoordinateBounds: any;
declare var GMSCameraUpdate: any;
declare var com: any;

@Component({
    moduleId: module.id,
    selector: 'app-map',
    templateUrl: 'map.component.html',
    styleUrls: ['map-common.css', 'map.component.css']
})

export class MapComponent extends Observable implements OnInit, AfterViewInit, OnDestroy {
    public hidden: boolean;

    public inactiveColor: string;
    public accentColor: string;
    public actionBarTitle: string = "Trending";
    public isInSearchMode: boolean = false;

    //VISIT DETECTION START
    // private _location: string;
    // private _counter: number;
    // private _message: string;
    // private _isMoving: boolean;
    // private _enabled: boolean;
    // private _paceButtonIcon: string;
    // private _motionActivity: string;
    // private _odometer: any;
    // private _mainMenuActivated: boolean;
    // private _isPowerSaveMode:boolean;
    //VISIT DETECTION END

    public onClickUser(args) {
        this.routerExtensions.navigate(['/user-profile/' + this.account.id], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickPosts(args) {
        this.routerExtensions.navigate(['/posts'], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickDiscover(args) {
        this.routerExtensions.navigate(['/map'], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickNotification(args) {
        this.routerExtensions.navigate(['/notification'], {
            clearHistory: true,
            animated: false
        });
    }


    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                progress: 0,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'// see iOS specific options below
                }
            }
        };




    /** @type {ElementRef} **/
    @ViewChild("MapView") private mapView: MapView;

    fromSearch: boolean = false;

    private positionBeforeNavigate: {
        zoom?: number,
        lat?: number,
        lng?: number,
    } = {}

    /** @type {SettingsInterface} **/
    private settings: SettingsInterface;

    private defaultZoom: number = 16;

    private locationFromSearch: { latitude?: number, longitude?: number } = {};

    /**
     * @type {SettingsInterface}
     * IMPORTANT:
     * The Following presets should not be manipulated without authorization.
     * Performance can drastically be improved or decreased depending on the
     * value set for each of the settings below.
     *
     * The preset setting is referenced into this.settings within ctor.
     **/
    private presets: {
        OPTIMAL: SettingsInterface,
        GOOD: SettingsInterface,
        POOR: SettingsInterface
    } = {
            // This is the configuration that allows smoother transitions
            // While navigating the map
            OPTIMAL: {
                TRY_RECONNECTION_AFTER: 1, //seconds
                AREA_DISTANCE: 1, // miles
                MAX_AREA_RESULTS: 30, // markers
                UPDATE_AFTER_DISTANCE: 1000, // meters
                LOAD_NEARBY_MARKER: 100, // meters
                HOLD_CAMERA_FOR: 1, // seconds,
                WAIT_UNTIL_NEXT_MARKER: 0, // milliseconds,
                WIPE_MARKERS: true, // True will completly wipe the markers, false will analyzse differences
                CUSTOM_MARKERS: true, // True will load custom marker icons
                CALCULATE_DISTANCES: true, // True will calculate distances on camera changes
                SET_MIN_ZOOM: 12,
                SET_MAX_ZOOM: 19.5
            },
            // This configuration will update in shorter distances but that requires more processes
            // So it will be slighly slower
            GOOD: {
                TRY_RECONNECTION_AFTER: 1, //seconds
                AREA_DISTANCE: 8, // miles
                MAX_AREA_RESULTS: 15, // markers
                UPDATE_AFTER_DISTANCE: 800, // meters
                LOAD_NEARBY_MARKER: 200, // meters
                HOLD_CAMERA_FOR: 0.5, // seconds,
                WAIT_UNTIL_NEXT_MARKER: 0, // milliseconds,
                WIPE_MARKERS: false, // True will completly wipe the markers, false will analyzse differences
                CUSTOM_MARKERS: true, // True will load custom marker icons
                CALCULATE_DISTANCES: true, // True will calculate distances on camera changes
                SET_MIN_ZOOM: 15.0,
                SET_MAX_ZOOM: 16.0
            },
            // This configuration will load even more markers into the map, and will
            // Update in a longer distance but will be a much slower, thus we will need
            // Disable markers analysis and set replace all instead, which helps for faster performance.
            POOR: {
                TRY_RECONNECTION_AFTER: 1, //seconds
                AREA_DISTANCE: 10, // miles
                MAX_AREA_RESULTS: 30, // markers
                UPDATE_AFTER_DISTANCE: 1500, // meters
                LOAD_NEARBY_MARKER: 200, // meters
                HOLD_CAMERA_FOR: 0.5, // seconds,
                WAIT_UNTIL_NEXT_MARKER: 0, // milliseconds,
                WIPE_MARKERS: true, // True will completly wipe the markers, false will analyzse differences
                CUSTOM_MARKERS: true, // True will load custom marker icons
                CALCULATE_DISTANCES: true, // True will calculate distances on camera changes
                SET_MIN_ZOOM: 15.0,
                SET_MAX_ZOOM: 16.0
            }
        }

    /** @type {LoadingIndicator} **/

    /** @type {MapStates} **/
    private states: MapStates = {
        CLEAN_LOAD: false,
        BACK_LOAD: false,
        LOCATION_LOAD: false,
        HISTORY_MAP_LOAD: false,
        HISTORY_LIST_LOAD: false,
        FARAWAY_SEARCH_LOAD: false
    };


    /** @type {Subscription[]} **/
    private subscriptions: Subscription[] = new Array<Subscription>();

    /** @type {Subscription} **/
    private subscription: {
        establishment?: Subscription,
        visit?: Subscription
    } = {};

    /** @type {FireLoopRef<Establishment>} **/
    private establishmentRef: FireLoopRef<Establishment>;

    /** @type {FireLoopRef<Visit>} **/
    private visitRef: FireLoopRef<Visit>;

    /** @type {Position} **/
    private positions: { user?: geolocation.Location, current?: Position, lastUpdated?: Position, lastPosition?: Position };

    /** @type {boolean} **/
    private watchingEvents: boolean = false;

    /** @type {boolean} **/
    private centered: boolean = true;

    /** @type {Establishment} **/
    private establishment: {
        selected?: Establishment,
        closestFromCenter?: Establishment,
        closestFromUser?: Establishment
    } = {
            selected: null,
            closestFromCenter: null,
            closestFromUser: null
        };

    /** @type {Marker} **/
    private highlightMarker: Marker;

    /** @type {Marker} **/
    private userMarker: Marker;

    /** @type {number} **/
    private watchId: number;

    /** @type {CriteriaInterface} **/
    private criteria: CriteriaInterface;

    /** @type {number} **/
    private cameraCounter: { swiped: boolean, number: number, timeout: any } = {
        swiped: false,
        number: 0,
        timeout: null
    };;

    /** @type {[key: string]: number} **/
    private distances: DistancesInterface = {
        centerFromUser: 0,
        selectedEstablishmentFromUser: 0,
        closestEstablishmentFromUser: 0,
        closestEstablishmentFromCenter: 0,
    };

    /** @type { CardSettings } */
    private cardSettings: CardSettings = {
        start: [20, 0, 0, 0],
        end: [100, 0, 0, 0],
        radius: 5
    };

    private isMapNew: boolean = false;

    /** @type {enabled: boolean} */
    private search: { enabled: boolean } = { enabled: false };

    private fromBack: boolean = false;

    /** @type {enabled: boolean} */
    private menu: { enabled: boolean } = { enabled: false };

    /** @type {enabled: boolean} */
    private history: { enabled: boolean } = { enabled: false };

    /** @type {enabled: boolean} */
    private historyList: { enabled: boolean } = { enabled: false };

    /** @type {enabled: boolean} */
    private share: { enabled: boolean } = { enabled: false };

    /** @type {enabled: boolean} */
    private notifications: { enabled: boolean } = { enabled: false };

    /** @type {enabled: boolean} */
    private cachedMarkers: { enabled: boolean } = { enabled: false };

    /** @type {enabled: boolean} */
    private centerEstablishment: { enabled: boolean } = { enabled: false };

    /** @type {enabled: boolean} */
    private tappedMarker: { enabled: boolean } = { enabled: false };

    /** @type {enabled: boolean} */
    private establishmentProfile: { enabled: boolean } = { enabled: false };

    private establishmentShare: Establishment;

    public isNight: boolean = false;

    /** @type {Marker[]} **/
    public markers: Marker[] = new Array<Marker>();

    /** @type {Visit[]} **/
    public visits: Visit[] = new Array<Visit>();

    public showOneEstablishment: boolean = false;

    /** @type {Subject<Position>} **/
    private locationSubject: Subject<Position> = new Subject<Position>();

    /** @type {Account} **/
    private account: Account;

    /** @type {Boolean} **/
    private isHistoryOwner: boolean;

    private showFriends: boolean = false;

    /** @type {Subscription} **/
    private locationSubscription: Subscription = new Subscription();

    /** @type {Subscription} **/
    private onReadySubscription: Subscription = new Subscription();

    private loadingEstablishments: boolean = false;

    private accountHistoryId: string;

    private visitHistoryId: string;

    private newNotifications: number = 0;

    private menuInformation: MenuInfoInterface = {};

    private showRectify: boolean = false;

    private visit: Visit;

    private hourSubscription: Subscription;

    private detectionCard: { enabled: boolean } = { enabled: false };

    private lastZoom: number = 0;

    private limit: number = 0;

    private searchOptions: { invalidArea: boolean, emptyArea: boolean, emptySearch: boolean } = {
        invalidArea: false,
        emptyArea: false,
        emptySearch: false
    }

    private blockCameraChange: boolean = false;

    private showBack: boolean = false;

    private detector: { visit: Visit, establishment: Establishment };

    private _shouldCenterWhenLocationReceived: boolean = false;
    private _previousMarkers: Marker[] = []

    /**
     * @method constructor
     * @param {Page} page NativeScript Page
     * @param {PageRouter} page NativeScript PageRouter
     * @param {AreaApi} area Area API Service
     * @param {AccountApi} accountApi Account API Service
     * @param {NgZone} zone Angular 2 Zone
     * @param {Router} router Angular 2 Router
     * @param {ActivatedRoute} route Angular 2 ActivatedRoute
     * @param {MapService} mapService Google Maps Service
     * @param {StorageNative} storage NativeScript Local Storage
     **/
    constructor(
        private page: Page,
        private pageRoute: PageRoute,
        private area: AreaApi,
        private accountApi: AccountApi,
        private visitApi: VisitApi,
        private establishmentApi: EstablishmentApi,
        private zone: NgZone,
        private router: Router,
        private route: ActivatedRoute,
        private categoriesMap: CategoriesMap,
        private storage: StorageNative,
        private routerExtensions: RouterExtensions,
        private notificationsService: NotificationsService,
        private locationFixApi: LocationFixApi,
        private historyService: HistoryService,
        private visitDetectionService: VisitDetectionService,
        private logoutService: LogoutService,
        private http: Http,
        private _changeDetectionRef: ChangeDetectorRef
    ) {
        super();
        // Set preset settings
        this.settings = this.presets.OPTIMAL;
        // Set loopBack configuration
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        //TOGO GOOGLE ANALYTICS
        //googleAnalytics.logView('map-live');
        // Lets setup our push visit detection service
        this.setupVisitDetectionListener();
        // Lets setup our push notifications service
        this.setupPushNotificationListener();
        // Lets setup our hardware back button
        this.setupBackButton();
        // Lets setup our map routing states
        this.setupStates();
        // Lets watch for location changes
        this.watch();
        // Lets watch for time changes and change map color
        this.watchHour();
        // Lets start the visit detection service
        this.visitDetectionService.startVisitDetector();


    }



    /**
     * @method setupStates
     * @return {void}
     * @description On every page route change, the map should load different information
     * depending in which state currently we are.
     *
     * The following are examples of current available states.
     *
     * - MapComponent.states.CLEAN_LOAD
     * - MapComponent.states.BACK_LOAD
     * - MapComponent.states.LOCATION_LOAD
     * - MapComponent.states.HISTORY_MAP_LOAD
     * - MapComponent.states.HISTORY_LIST_LOAD
     *
     * States will usually be loaded from this method, but won't be terminated here, instead These
     * are terminated within the different events supported by the map ui, depending on the user
     * interactions.
     **/
    setupStates() {
        console.log('SETTING UP MAP STATES');
        this.pageRoute
            .activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                console.log('params', JSON.stringify(params));
                if (params['accountHistoryId']) {
                    this.accountHistoryId = params['accountHistoryId'];
                    this.states.HISTORY_MAP_LOAD = true;
                    this.history.enabled = true; // TODO: Remove when states are implemented
                }

                if (params['establishment']) {
                    console.log('ESTABLISHMENT FROM PROFILE: ', params['establishment']);
                    this.establishmentProfile.enabled = true;
                    this.showOneEstablishment = true;
                    this.highLightEstablishment(JSON.parse(params['establishment']), null, 0);
                } else {
                    this.establishmentProfile.enabled = false;
                    this.showOneEstablishment = false;
                }

                // Get persisted locations, if any persisted in local storage
                // If current position does not exist then set new york as current position.
                this.positions = {
                    user: this.storage.get('userPosition') || 0,
                    current: this.storage.get('currentPosition') || Position.positionFromLatLng(40.7127837, -74.0059413)
                };

                // When the app loads for the first time after login, we don't have any
                // location data in current storage. So we set the location to NewYork.
                // But when after some time we get the current location, we should move
                // user to that location instead.
                if (!this._shouldCenterWhenLocationReceived) {
                    this._shouldCenterWhenLocationReceived = this.positions.user.latitude == undefined
                }

                console.log('STORAGED POSITIONS: ', JSON.stringify(this.positions));
                console.log('HISTORY ENABLED: ', this.history.enabled);
                if (!this.positionBeforeNavigate.lat) {
                    this.positionBeforeNavigate = this.storage.get('positionBeforeNavigate') || {};
                    this.storage.set('positionBeforeNavigate', {});
                }
                let current: Position;
                let zoom: number = this.positionBeforeNavigate.zoom || this.defaultZoom;
                if (this.positionBeforeNavigate.lat) {
                    current = (this.positionBeforeNavigate.lat && this.positionBeforeNavigate.lng) ? Position.positionFromLatLng(
                        this.positionBeforeNavigate.lat, this.positionBeforeNavigate.lng
                    ) : this.positions.current || null;
                } else {
                    // We need to handle navigation since components are cached and not re-created
                    // during navigations, therefore we need to manage if this is a new instance, or navigated page.
                    // But only the second time, when there is already an establishmentReference
                    current = (params['latitude'] && params['longitude']) ? Position.positionFromLatLng(
                        params['latitude'], params['longitude']
                    ) : this.positions.current || null;
                }
                let mapWaitInterval = timer.setInterval(() => {
                    if (this.mapView) {
                        console.log('MAP VIEW LOADED AFTER ROUTE NAVIGATION');
                        timer.clearInterval(mapWaitInterval);
                        this.loadUserMarker();
                        // The following state is needed when users tap on a specific establishment location button
                        // From the establishment profile.
                        if (params['centerEstablishment']) {
                            this.markers = [];
                            this.cachedMarkers.enabled = false;
                            this.states.LOCATION_LOAD = true;
                            this.centerEstablishment.enabled = true; // TODO: remove when states are implemented
                        }
                        // Load cached markers, this flow is mostly for back buttons that
                        // returns from other sections into the map view.
                        else if (this.markers.length > 0) {
                            this.cachedMarkers.enabled = true;
                            this.positions.lastUpdated = current;
                            this.states.BACK_LOAD = true; // This is most probably a back button, there are markers but not center location
                            //this.reloadCachedMarkers();
                            this.watchLocationEstablishments();
                            if (this.highlightMarker && !this.page.ios) {
                                let userData: any = this.highlightMarker.userData;
                                this.highlightMarker = new Marker();
                                //this.highlightMarker.icon = this.categoriesMap.getImageSource(userData.establishment.categories, true);
                                this.highlightMarker.icon = this.categoriesMap.getHcImageSource(userData.establishment.hcCategory, true);
                                this.highlightMarker.position = Position.positionFromLatLng(userData.establishment.geoLocation.coordinates[1], userData.establishment.geoLocation.coordinates[0]);
                                this.highlightMarker.userData = userData;
                                this.highlightMarker.zIndex = 10;
                                this.mapView.addMarker(this.highlightMarker);
                                // Calculate selected distance from user
                                this.distances.selectedEstablishmentFromUser = this.calculateDistance(
                                    this.highlightMarker.position,
                                    this.positions.user
                                );
                            }
                        }
                        // Load cached markers, this flow is mostly for back buttons that
                        // returns from other sections into the map view.
                        else {
                            // Some Clarity: This actually is the first entry to the map
                            // mostly when the app loads for the very first time.
                            // But also, there is a race condition since there are 2 triggers to load markers
                            // 1.- When the geolocation updates the position
                            // 2.- When a page changes.
                            console.log('LOADING MAP FOR FIRST TIME');
                            this.isMapNew = true;
                            this.loadUserMarker();
                        }
                        // Center at this point is super important to avoid race conditions
                        // Between onMapRady and this PageRoute event.
                        if (this.history.enabled) {
                            // this.expand(this.isHistoryOwner);
                            this.positionBeforeNavigate = {}
                            this.storage.set('positionBeforeNavigate', this.positionBeforeNavigate)
                            this.mapView.zoom = zoom;
                            this.historyService.getMarkers();
                        } else if (this.showBack) {
                            let criteria: CriteriaInterface = this.storage.get('lastSearchCriteria') || {};
                            if (criteria && !criteria.area) {
                                criteria.area = criteria.lastArea;
                                criteria.lastArea = '';
                            }
                            this.setCurrentPosition(current);
                            if (this.mapView) {
                                this.mapView.zoom = zoom;
                            }
                            this.fromBack = true;
                            this.watchLocationEstablishments(criteria);
                        } else {
                            this.center();
                        }
                        // This flow is mostly for forward navigation, from establishment/user profiles to map
                        if (params['latitude'] && params['longitude']) {
                            console.log('LOADING MARKERS FROM CURRENT LOCATION');
                            //if (this.establishmentRef && this.visitRef) {
                            console.log('LOADING RIGHT AWAY');
                            //this.markers = [];
                            // This should move the camera and onCameraChanged should do the rest
                            this.mapView.latitude = current.latitude;
                            this.mapView.longitude = current.longitude;
                        }
                        if (params['visitNotification']) {
                            let pushNotification = JSON.parse(params['visitNotification']);
                            let detector = this.storage.get('detector');
                            this.storage.set('detector', {});
                            if (
                                detector && detector.visit &&
                                detector.visit.id && pushNotification &&
                                pushNotification.instance.establishmentId == detector.establishment.id
                            ) {
                                this.detector = detector;
                                this.showVisitDetectionCard(this.detector.visit)
                            }
                            this.loadHistoryMode();
                        }
                    }
                }, 1);
            }
            );
    }
    /**
     * @method setupBackButton
     * @return {void}
     **/
    setupBackButton() {
        // Prevent navigate back if notifications or friends list are open
        if (!isIOS) {
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                if (this.router.isActive("/map", false)) {
                    if (this.showFriends) {
                        data.cancel = true;
                        this.zone.run(() => this.showFriends = false);
                    }
                    if (this.share.enabled) {
                        data.cancel = true;
                        this.zone.run(() => this.share.enabled = false);
                    }
                    if (this.notifications.enabled) {
                        data.cancel = true;
                        this.zone.run(() => this.notifications.enabled = false);
                    }
                    if (this.establishment.selected) {
                        data.cancel = true;
                        this.zone.run(() => {
                            this.back();
                        });
                    }
                    if (this.search.enabled) {
                        data.cancel = true;
                        this.zone.run(() => {
                            this.search.enabled = false;
                        })
                    }
                    console.log('going back!');
                }
            });
        }
    }
    /**
     * @method setupPushNotificationListener
     * @return {void}
     **/
    setupPushNotificationListener() {
        this.notificationsService.onNewPushNotification().subscribe((message: any) => {
            console.log('MESSAGE ' + JSON.stringify(message));
            if (message.type && message.type == 'VISIT') {
                // this.accountHistoryId = message.instance.account.id;
                this.loadHistoryMode();
            }
            if (message.type && message.type == 'FRIEND_REQUEST') {
                this.menu.enabled = true;
                this.openNotifications();
            }
        })
    }

    /**
     * @method setupVisitDetectionListener
     * @return {void}
     **/
    setupVisitDetectionListener() {
        this.visitDetectionService.onNewVisit().subscribe(
            (detector: { visit: Visit, establishment: Establishment }) => {
                this.detector = detector;
                this.storage.set('detector', detector);
                this.showVisitDetectionCard(detector.visit)
            }
        );
    }
    /**
      * @method onMapReady
      * @param {any} event Event
      * @return {void}
      **/
    onMapReady(event: any): void {
        console.log('MAP READY');
        this.mapView = event.object;
        // Set default zoom
        this.mapView.zoom = this.defaultZoom;
        // Load Current Location (if any)
        if (isIOS) {
            this.mapView.myLocationEnabled = true;
            this.mapView.gMap.setMinZoomMaxZoom(null, this.settings.SET_MAX_ZOOM);
            this.mapView.gMap.settings.indoorPicker = false;
            this.mapView.gMap.settings.compassButton = false;
            this.mapView.gMap.settings.myLocationButton = false;
            this.mapView.gMap.settings.rotateGestures = false;
        } else {
            this.mapView.gMap.setMyLocationEnabled(true);
            this.mapView.gMap.setIndoorEnabled(false);
            this.mapView.gMap.setBuildingsEnabled(false);
            // this.mapView.gMap.setMinZoomPreference(this.settings.SET_MIN_ZOOM);
            this.mapView.gMap.setMaxZoomPreference(this.settings.SET_MAX_ZOOM);
            let settings = this.mapView.gMap.getUiSettings();
            settings.setMapToolbarEnabled(false);
            settings.setCompassEnabled(false);
            settings.setRotateGesturesEnabled(false);
            settings.setTiltGesturesEnabled(false);
            settings.setZoomControlsEnabled(false);
            settings.setIndoorLevelPickerEnabled(false);
            settings.setMyLocationButtonEnabled(false);
        }
        // Set Map Style
        let currentTime = moment();
        let hour = currentTime.hour();
        if (
            hour >= 6 &&
            hour <= 17
        ) {
            this.mapView.setStyle(dayStyle);
            this.isNight = false;
        } else {
            this.mapView.setStyle(nightStyle);
            this.isNight = true;
        }
        this.getCurrentUserAccount();
    }
    /**
     * @method hold
     * @return {void}
     * @description
     * This method will force subscriptions on reconnections.
     * When FireLoop is unable to connect at once, it will reconnect
     * But the on.connect event is not being fired. This method fix that
     * issue while the SDK implement it out of the box.
     **
    hold(): void {
        // Force OnConnect
        let holding: number = 0;
        let to = timer.setInterval(() => {
            if (!this.watchingEvents && this.realTime.connection.isConnected() && holding === this.settings.TRY_RECONNECTION_AFTER) {
                this.establishmentRef = this.realTime.FireLoop.ref<Establishment>(Establishment);
                this.visitRef = this.realTime.FireLoop.ref<Visit>(Visit);
                this.watch();
                timer.clearInterval(to);
            } else if (!this.watchingEvents && this.realTime.connection.isConnected() && holding < this.settings.TRY_RECONNECTION_AFTER) {
                holding = holding + 1;
            } else if (this.watchingEvents && this.realTime.connection.isConnected()) {
                console.log('CONNECTED.');
            }
        }, 1000);
    }*/
    /**
     * @method watch
     * @return {void}
     **/
    watch(): void {
        if (this.showOneEstablishment) {
            return;
        }
        // This will mainly happen on navigation, because the location subscription already exists
        // But the map was wiped, so at this stage watching is really triggering a fist time
        // So the map can be populated, this only happens the second time watch is executed.
        if (this.locationSubscription) {
            this.locationSubscription.unsubscribe();
            console.log('AVOIDING LOCATION SUBSCRIPTION (POTENTIAL DUPLICATED EVENT)');
        }
        //this.watchingEvents = true;
        this.locationSubscription = this.locationSubject.subscribe((position: geolocation.Location) => {
            // The following condition, creates issues when driving fast, lets allow the user to update all the time
            // The location, regardless
            //let distance: number = this.calculateDistance(position, this.positions.current);
            if (this.centered && !this.tappedMarker.enabled && !this.search.enabled && !this.showBack) { // && (distance > 5 || this.markers.length === 0)) {
                //this.positions.lastUpdated = position;
                this.setUserPosition(position);
                this.setCurrentPosition(position);
                this.loadCurrentPosition();
                console.log('LOADING LOCATION ESTABLISHMENTS FROM WATCH');
                //this.watchLocationEstablishments();
                // this.onCameraChanged({ camera: position }) this is wrong because this way we dont previously pan the map, lets the map be panned and then will fire onCameraChanged
            } else {
                // Needed also here, not only in center() or onCameraChanged()...
                // Because once geolocation is updated and a marker is tapped, the map won't move, but we still need to know
                // If it is centered or not.
                // For instance, the previous condition will move the map, then it does not need to verify the center, since that will be DONE
                // Within the onCameraChanged flow.
                this.centered = this.verifyIfCenteredTo(position);
                this.setUserPosition(position);
            }

            if (this._shouldCenterWhenLocationReceived) {
                this._shouldCenterWhenLocationReceived = false;
                this.center();
            }
            // If there is a loaded establishment we need to calculate the distance
            if (this.establishment.selected) {
                this.distances.selectedEstablishmentFromUser = this.calculateDistance(this.highlightMarker.position, this.positions.user);
            }
            this.loadUserMarker();
        });
        this.watchLocation();
    }
    /**
     * @method closeMenu
     * @return {void}
     **/
    closeMenu(): void {
        if (this.showFriends || this.notifications.enabled) {
            return;
        }
        this.cameraMovementSettings(true);
        this.menu.enabled = false;
    }
    /**
     * @method closeMenu
     * @return {void}
     **/
    displayMenu(): void {
        this.menu.enabled = true;
        if (this.establishment.selected) {
            this.unloadCurrentEstablishment();
        }
        this.cameraMovementSettings(false);
    }
    /**
     * @method cameraMovementSettings
     * @param {boolean} enabled
     * @return {void}
     **/
    cameraMovementSettings(enabled: boolean = false): void {
        if (!isIOS && this.mapView) {
            let settings = this.mapView.gMap.getUiSettings();
            settings.setScrollGesturesEnabled(enabled);
        }
    }
    /**
     * @method back
     * @return {void}
     **/
    back(historyMode?: string): void {
        this.tappedMarker.enabled = false;
        if (this.search.enabled) {
            this.search.enabled = false;
        } else if (this.showOneEstablishment) {
            this.showOneEstablishment = false;
            this.routerExtensions.back();
        } else if (this.history.enabled) {
            this.menu.enabled = false;
            this.cameraMovementSettings(true);
            // Complex flow below, do not freely edit the following condition
            if (this.accountHistoryId) {
                this.accountHistoryId = null;
                this.visitHistoryId = null;
                if (this.routerExtensions.canGoBack()) {
                    this.routerExtensions.back();
                }
            } else {
                // IF not enabled: it wont return to the origin when trying to close the history mode
                // IF enabled:
                //this.loader.loading = true;
                //this.loader.instance.show(this.loader.options); <-- Duplicated lloader, the watch establishment
                if (historyMode && historyMode === 'map') {
                    this.historyService.refreshHistory();
                } else {
                    this.center();
                    this.loadCurrentPosition();
                    this.unloadCurrentEstablishment();
                    // Needs to be down here or won't allow this.center() to correctly work but before of
                    // this.watchLocationEstablishments() or won't work neither. (Picky flag)
                    this.history.enabled = false;
                    this.watchLocationEstablishments();
                }
            }
        } else {
            this.loadCurrentPosition();
            this.unloadCurrentEstablishment();
        }
        /**
         * This strange condition is duplicated
         *  else {
            if (this.search.enabled) {
                this.search.enabled = false;
            } else if (this.history.enabled) {
                this.menu.enabled = false;
                this.history.enabled = false;
                this.loadCurrentPosition();
                this.unloadCurrentEstablishment();
                this.watchLocationEstablishments();
            } else {
                this.loadCurrentPosition();
                this.unloadCurrentEstablishment();
            }
        } */
    }
    /**
     * @method center.
     * @return {void}
     **/
    center(): void {
        if (this.search.enabled && this.criteria && this.criteria.lastArea) {
            this.criteria.lastArea = '';
        }
        if (this.cachedMarkers.enabled || this.centerEstablishment.enabled) {
            this.centered = false;
            console.log('CENTERING TO CURRENT POSITION');
        } else {
            this.centered = true;
            this.tappedMarker.enabled = false;
            console.log('CENTERING TO USER POSITION');
            this.setCurrentPosition(this.positions.user);
            this.unloadCurrentEstablishment();
            if (this.mapView) {
                this.mapView.zoom = this.defaultZoom;
            }
        }
        this.loadCurrentPosition();
        if (this.history.enabled && this.accountHistoryId == null) {
            console.log('STATE: FROM HISTORY VIEW TO LIVE MAP VIEW');
            // If not enabled: won't return to origin
            // If enabled:
            // Used When:
            //    1) User is returning from history view to live view
            this.centered = true;
            this.setCurrentPosition(this.positions.user);
            //this.back(); <--- Creates issues because when returning from other section to history map view
        } /* else {
            let distance: number = this.calculateDistance(this.positions.current, this.positions.user);
            // Only update server calls every kilometer
            if (distance > this.settings.UPDATE_AFTER_DISTANCE) {
                // Needed to update map markers on re-center
                // Only if there are alredy markers
                // Otherwise the map is still to be loaded
                /** YEST BUT SHOULD BE DONE FROM ELSE WHERE, GENERATES BUGS ON BACK BUTTON :(
                if (this.markers.length > 0) {
                this.setCurrentPosition(this.positions.user);
                this.watchLocationEstablishments();
                this.removeCurrentEstablishmentMarkers();
                }

            }
        } */
    }
    /**
     * @method watchLocation
     * @return {void}
     **/
    watchLocation(): void {
        this.visitDetectionService.onNewUserPosition().subscribe(
            (res: { position: geolocation.Location, updateUser: boolean }) => {
                if (res.updateUser) {
                    this.setUserPosition(res.position);
                } else {
                    this.locationSubject.next(res.position);
                }
            }
        );
    }
    /**
     * @method verifyIfCenteredTo
     * @return {void}
     */
    verifyIfCenteredTo(position: Position): boolean {
        if (!this.mapView) {
            console.log('No mapview available at verifyIfCenteredTo');
            return;
        }
        let positionToSouthWest: number;
        let positionToNorthEast: number
        if (isIOS) {
            console.log('message')
            let bounds: {
                nearLeft: {
                    latitude: any,
                    longitude: any
                },
                nearRight: {
                    latitude: any,
                    longitude: any
                },
                farLeft: {
                    latitude: any,
                    longitude: any
                },
                farRight: {
                    latitude: any,
                    longitude: any
                }
            } = JSON.parse(JSON.stringify(this.mapView.gMap.projection.visibleRegion()));

            positionToSouthWest = Math.floor(this.calculateDistance(Position.positionFromLatLng(
                bounds.nearLeft.latitude,
                bounds.nearLeft.longitude
            ), position));

            positionToNorthEast = Math.floor(this.calculateDistance(Position.positionFromLatLng(
                bounds.farRight.latitude,
                bounds.farRight.longitude
            ), position));
        } else {
            let bounds: {
                southwest: {
                    latitude: any,
                    longitude: any
                },
                northeast: {
                    latitude: any,
                    longitude: any
                }
            } = this.mapView.gMap.getProjection().getVisibleRegion().latLngBounds;

            positionToSouthWest = Math.floor(this.calculateDistance(Position.positionFromLatLng(
                bounds.southwest.latitude,
                bounds.southwest.longitude
            ), position));

            positionToNorthEast = Math.floor(this.calculateDistance(Position.positionFromLatLng(
                bounds.northeast.latitude,
                bounds.northeast.longitude
            ), position));

            console.log('positionToNorthEast: ', positionToNorthEast);
            console.log('positionToSouthWest: ', positionToSouthWest);
        }

        let diff: number = positionToNorthEast - positionToSouthWest;

        if (diff > 3 || diff < -3) {
            // return this.mapView.zoom < 13 ? true : false;
            return false;
        } else {
            return true;
        }
    }
    /**
     * @method getScreenCoordinates
     * @return {void}
     */
    getScreenCoordinates(): number[][] {
        let coordinates: number[][] = [];
        if (isIOS) {
            let bounds: {
                nearLeft: {
                    latitude: any,
                    longitude: any
                },
                nearRight: {
                    latitude: any,
                    longitude: any
                },
                farLeft: {
                    latitude: any,
                    longitude: any
                },
                farRight: {
                    latitude: any,
                    longitude: any
                }
            } = JSON.parse(JSON.stringify(this.mapView.gMap.projection.visibleRegion()));
            console.log('message', JSON.stringify(bounds))
            coordinates.push([bounds.nearLeft.longitude, bounds.nearLeft.latitude]);
            coordinates.push([bounds.farLeft.longitude, bounds.farLeft.latitude]);
            coordinates.push([bounds.farRight.longitude, bounds.farRight.latitude]);
            coordinates.push([bounds.nearRight.longitude, bounds.nearRight.latitude]);
        } else {
            let bounds: {
                southwest: {
                    latitude: any,
                    longitude: any
                },
                northeast: {
                    latitude: any,
                    longitude: any
                }
            } = this.mapView.gMap.getProjection().getVisibleRegion().latLngBounds;
            if (bounds && bounds.southwest && bounds.northeast) {
                // This will create a geospatial polygon for searchng venues within device screen
                // push southwes
                coordinates.push([bounds.southwest.longitude, bounds.southwest.latitude]);
                // push northwest
                coordinates.push([bounds.southwest.longitude, bounds.northeast.latitude]);
                // push northeast
                coordinates.push([bounds.northeast.longitude, bounds.northeast.latitude]);
                // push southeast
                coordinates.push([bounds.northeast.longitude, bounds.southwest.latitude]);
            }
        }
        return coordinates;
    }
    /**
     * @method watchLocationEstablishments
     * @return {void}
     */
    watchLocationEstablishments(criteria: CriteriaInterface = null): void {
        if (this.search.enabled && criteria && criteria.area != null) {
            this.storage.set('lastSearchCriteria', criteria);
        }
        if (this.showOneEstablishment) {
            return;
        }

        // In History mode, we get the data from different places
        if (this.history.enabled || this.establishmentProfile.enabled) {
            return;
        }
        if (!this.mapView) {
            return;
        }

        if (this.mapView.zoom < this.settings.SET_MIN_ZOOM) {
            this.removeCurrentEstablishmentMarkers();
            this.unloadCurrentEstablishment();
            return;
        }

        if (this.subscription.establishment) {
            this.subscription.establishment.unsubscribe();
        }

        if (this.subscription.visit) {
            this.subscription.visit.unsubscribe();
        }

        if (criteria && (criteria.text !== '' || criteria.area !== '' || criteria.category !== '')) {
            this.criteria = criteria;
            if (this.search.enabled) {
                this.loader.instance.show(this.loader.options);
            }
            if (!criteria.area || criteria.area == '') {
                this.searchOptions.invalidArea = false;
            }
        }

        this.limit = (
            this.mapView.zoom <= 15 ? 25 :
                this.mapView.zoom <= 16 ? 15 : 15
        );

        let filter: LoopBackFilter = {
            where: {},
            limit: this.limit
        };

        this.loadingEstablishments = true;
        if (this.loader.loading) {
            this.loader.instance.hide();
        }
         this.loader.loading = true;
        // There is another call to the server within selecTCurrentLocation
        //this.loader.instance.show(this.loader.options); // Should be only loader, since here it loads from server.
        // Select current position, if user wrote a different area, will move to that area and set as current.
        this.selectCurrentLocation((current: Position, searched: boolean, area?: Area) => {
            console.log('STARTING REQUEST123', JSON.stringify(current));
            console.time('ESTABLISHMENT TIMER');
            //TOGO GOOGLE ANALYTICS
            /*googleAnalytics.startTimer('establishments-timer', {
                category: 'Fetch data',
                name: 'Fetch establishments'
            })*/
            // Add current position to query
            // if(this.search.enabled && this.criteria.area !== '')
            if (searched && area && area.id) {
                filter.where.geoLocation = {
                    geoWithin: {
                        $polygon: [
                            [area.southWest.lng, area.southWest.lat],
                            [area.southWest.lng, area.northEast.lat],
                            [area.northEast.lng, area.northEast.lat],
                            [area.northEast.lng, area.southWest.lat]
                        ]
                    }
                };
            } else {
                if (this.search.enabled) {
                    console.dir(this.userMarker);
                    let xs = this.userMarker.position;
                    let center = [
                        this.userMarker.position.longitude,
                        this.userMarker.position.latitude
                    ];
                    // distance to radians: divide the distance by the radius of
                    // the sphere (e.g. the Earth) in the same units as the distance
                    // measurement.
                    let radius = criteria && criteria.area == '' ? 5 : 20;
                    let searchArea = radius / 3963.2;
                    filter.where.geoLocation = {
                        geoWithin: {
                            $centerSphere: [center, searchArea]
                        }
                    };
                } else {
                    let x = this.getScreenCoordinates()
                    filter.where.geoLocation = { geoWithin: { $polygon: x } };

                }
            }
            // Order
            filter.order = 'rating DESC'
            console.log('STILL HISTORY ENABLED: ', this.history.enabled);

            filter.fields = [
                'id',
                'name',
                'description',
                'geoLocation',
                'address',
                'categories',
                'locationId',
                'pricing',
                'hcCategory'
            ];

            // if (this.criteria && this.criteria.category !== '') {
            //     filter.where.categories = { "like": '.*' + this.criteria.category + '.*', "options": "i" };
            // }

            filter.include = { relation: 'photos' };

            // Add search text criteria if any
            //            if (this.criteria && this.criteria.text !== '' && this.criteria.text != null) {
            //                filter.where.or = [
            //                    { name: { "like": '.*' + this.criteria.text + '.*', "options": "i" } },
            //                    { categories: { "like": '.*' + this.criteria.text + '.*', "options": "i" } }
            //                ]
            //                // filter.where.name = { "like": '.*' + this.criteria.text + '.*', "options": "i" } ;
            //            }

            if ((this.criteria && this.criteria.text !== '' && this.criteria.text != null) && (this.criteria && this.criteria.category !== '' && this.criteria.category != null)) {
                filter.where.and = [
                    { name: { "like": '.*' + this.criteria.text + '.*', "options": "i" } },
                    { hcCategory: this.criteria.category }
                ]
            } else if (this.criteria && this.criteria.text !== '' && this.criteria.text != null) {
                filter.where.or = [
                    { name: { "like": '.*' + this.criteria.text + '.*', "options": "i" } },
                    { hcCategory: this.criteria.category }
                ]
                // filter.where.name = { "like": '.*' + this.criteria.text + '.*', "options": "i" } ;
            } else if (this.criteria && this.criteria.category !== '' && this.criteria.category != null) {
                //filter.where.categories = { '"' + this.criteria.category + '"' };
                filter.where.hcCategory = this.criteria.category;
                // filter.where.name = { "like": '.*' + this.criteria.text + '.*', "options": "i" } ;
            }

            // Validation added because an error when connection is off
            /*if (!this.establishmentRef) {
                console.log('no establishmentRef');
                return;
            }*/

            //filter = {"where":{"geoLocation":{"geoWithin":{"$polygon":[[-74.0088629,40.7373582],[-74.0088629,40.7570384],[-73.9877916,40.7570384],[-73.9877916,40.7373582]]}},"hcCategory":"Dance"},"limit":15,"order":"rating DESC","fields":["id","name","description","geoLocation","address","categories","locationId","hcCategory"],"include":{"relation":"photos"}};
            console.log('FINAL FILTER: ', JSON.stringify(filter));
            // Listen for establishments around the selected position

            this.subscription.establishment = this.establishmentApi.find(filter).subscribe(
                (establishments: Establishment[]) => {
                    this.zone.run(() => {
                        console.timeEnd('ESTABLISHMENT TIlMER');
                        this.loader.instance.hide();
                    })
                    //TOGO GOOGLE ANALYTICS
                    //googleAnalytics.stopTimer('establishments-timer')
                    // IF not enabled: generates a bug when navigating different areas, only loads the area center.
                    // IF enabled: won't show the latest nearby area, fix: implement last criteria in search component
                    this.searchOptions.emptySearch = establishments.length === 0;
                    if (criteria && criteria.area) {
                        if (searched) {
                            this.criteria.lastArea = "" + criteria.area;
                        }
                        this.searchOptions.emptyArea = (!area.updates || area.updates === 0);
                        console.log('EMPTY AREA: ', this.searchOptions.emptyArea);
                        if (this.criteria) {
                            //                            this.criteria.area = null;
                        }
                        /*
                        if (this.searchOptions.emptyArea && this.criteria.lastArea) {
                            this.criteria.area = null;
                        }else {
                            this.criteria.area = '';
                        }
                        */
                    }
                    let visitsEstablishments: VisitEstablishment[] = establishments.map((establishment: Establishment, index: number) => ({ establishment, index }));
                    if (this.settings.WIPE_MARKERS) {
                        this.removeCurrentEstablishmentMarkers();
                        // console.log("====================================================================CALLED FROM HERE1");
                        this.loadEstablishmentMarkers(visitsEstablishments);
                    } else {
                        // console.log("====================================================================CALLED FROM HERE2");
                        this.loadEstablishmentMarkers(
                            this.removeCurrentEstablishmentMarkers(visitsEstablishments)
                        );

                    }
                    // console.log('1-- 2', establishments.length, this.isMapNew ? 'yes' : 'nel mijo');

                    if (this.isMapNew && establishments.length == 0) {
                        // this.setCurrentPosition(this.positions.user);
                        // this.loadCurrentPosition();
                        timer.setTimeout(() => {
                            this.watchLocationEstablishments();
                            this.isMapNew = false;
                        }, 200)
                    } else {
                        this.isMapNew = false;
                    }
                    // This fixes the search expand button, we need to move the camera
                    // But avoid doing a new query.
                    console.log('2--Searched', searched ? 'si' : 'no');
                    if (searched) {
                        this.blockCameraChange = true;
                        this.positions.lastUpdated = current;
                        this.setCurrentPosition(current);
                        this.loadCurrentPosition();
                    }
                },
                error => this.errorHandler(error)
            );
        });
    }

    /**
     * @method removeCurrentEstablishmentMarkers
     */
    removeCurrentEstablishmentMarkers(visitsEstablishments?: VisitEstablishment[]): any {
        if (visitsEstablishments) {
            console.log("************************************** visitEstablishments")
            // Remove markers that are already in the map but are not in the received establishments.
            this.markers = this.markers.filter((marker: Marker) => {
                let found: number = visitsEstablishments.map((visitEstablishment: VisitEstablishment) => visitEstablishment.establishment.id)
                    .indexOf(marker.userData.establishment.id);
                // If not found in the new array then don't keep and remove from map
                if (found === -1) {
                    console.log('OLD MARKER NOT FOUND IN NEW ARRAY: ', marker.userData.establishment.id);
                    this.mapView.removeMarker(marker);
                    return false;
                } else {
                    if (visitsEstablishments[found].visit) {
                        marker.userData.visit = visitsEstablishments[found].visit;
                    }
                    //establishments = establishments.slice(found, 1);
                    return true;
                }
            });
            // Remove received establishments that are already in the map and were received again.
            visitsEstablishments = visitsEstablishments.filter((visitEstablishment: VisitEstablishment) => {
                let found: number = this.markers.map((marker: Marker) => marker.userData.establishment.id)
                    .indexOf(visitEstablishment.establishment.id);
                // If this establishment is not loaded already then we keep it to load it later
                return (found === -1);
            });
            return visitsEstablishments;
        } else {
            console.log("************************************** ELSE PART OF REMOVE!!!")
            //this.mapView.removeAllMarkers();
            this.markers.forEach((marker: Marker) => this.mapView.removeMarker(marker));
            this.markers = [];
        }
    }
    /**
     * @methodhmentMarkers
     * @return {void}
     */
    loadEstablishmentMarkers(visitsEstablishments: VisitEstablishment[]): void {
        if (!this.mapView) {
            console.log('no mapview instance');
            return;
        }
        if (visitsEstablishments && visitsEstablishments.length > 0) {
            let visitEstablishment: VisitEstablishment = visitsEstablishments.shift();
            //1-- console.log('LOADING ESTABLISHMENT:', visitEstablishment.establishment.name);

            //            console.log('visitEstablishment.establishment');
            //            console.dir(visitEstablishment.establishment);

            let marker: Marker = new Marker();
            if (this.settings.CUSTOM_MARKERS) {
                //marker.icon = this.categoriesMap.getImageSource(visitEstablishment.establishment.categories);
                marker.icon = this.categoriesMap.getHcImageSource([visitEstablishment.establishment.hcCategory]);
            }
            marker.position = Position.positionFromLatLng(
                visitEstablishment.establishment.geoLocation.coordinates[1],
                visitEstablishment.establishment.geoLocation.coordinates[0]
            );

            //marker.title = visitEstablishment.establishment.name;
            //console.log('Establishment, NAME: ' + visitEstablishment.establishment.name + ',');
            marker.userData = { establishment: visitEstablishment.establishment, visit: visitEstablishment.visit, index: visitEstablishment.index };
            this.mapView.addMarker(marker);
            this.markers.push(marker);
            //marker.showInfoWindow();
            if (this.settings.WAIT_UNTIL_NEXT_MARKER > 0) {
                let to = timer.setTimeout(() => {
                    timer.clearTimeout(to);
                    // console.log("====================================================================CALLED FROM HERE3");
                    this.loadEstablishmentMarkers(visitsEstablishments);
                }, this.settings.WAIT_UNTIL_NEXT_MARKER);
            } else {
                // console.log("====================================================================CALLED FROM HERE4");
                this.loadEstablishmentMarkers(visitsEstablishments);
            }
        } else {
            console.log('NO MORE ESTABLISHMENTS TO LOAD');
            this.loader.loading = false;
            this.loadingEstablishments = false;
            this.loader.instance.hide();
            if (this.markers && this.markers.length > 0 && this.centered && !this.tappedMarker.enabled && !this.menu.enabled) {
                this.findNearByEstablishment(this.positions.current);
            }
            // On history mode we need to pan the map to one of the markers.
            if (this.history.enabled && this.markers && this.markers.length) {
                let newPos: Position = Position.positionFromLatLng(
                    this.markers[0].userData.establishment.geoLocation.coordinates[1],
                    this.markers[0].userData.establishment.geoLocation.coordinates[0]
                );

                this.setCurrentPosition(newPos);
                this.loadCurrentPosition();
                // this.findNearByEstablishment(newPos); <-- Let onCameraChange do this after detects the new position.
            }
            // Clean Cached Marker State to avoid future state issues
            if (this.cachedMarkers.enabled) {
                console.log('CACHED MARKERS STATE END');
                this.cachedMarkers.enabled = false;
            }
            // Clean Center to Establishment State to avoid future state issues
            if (this.centerEstablishment.enabled) {
                console.log('CENTER LOCATION STATE END');
                this.centerEstablishment.enabled = false;
            }
            // Move to faraway area on search update
            if (this.states.FARAWAY_SEARCH_LOAD) {
                console.log('FARAWAY STATE END');
                this.states.FARAWAY_SEARCH_LOAD = false;
                //this.loadCurrentPosition();
                this.positions.lastUpdated = this.positions.current;
            }
        }
    }

    /**
     * @methodhmentMarkers
     * @return {void}
     */
    private restoreEstablishmentMarkers(): void {
        if (!this.mapView) {
            console.log('no mapview instance');
            return;
        }
        if (!this._previousMarkers || this._previousMarkers.length <= 0) return;

        this._previousMarkers.forEach(marker => {
            this.mapView.addMarker(marker);
            this.markers.push(marker);
        })

    }

    private saveCurrentEstablishments(): void {
        this._previousMarkers = this.markers
    }

    /*
     * @method reloadCachedMarkers
     * @return {void}
     */
    reloadCachedMarkers(): void {
        console.log('LOADING CACHED MARKERS', this.markers);
        let brokenMarkers: Marker[] = this.markers.slice();
        this.markers = [];
        brokenMarkers.forEach((brokenMarker: Marker) => {
            let establishment: Establishment = brokenMarker.userData.establishment;
            let marker: Marker = new Marker();
            if (this.settings.CUSTOM_MARKERS) {
                //marker.icon = this.categoriesMap.getImageSource(establishment.categories);
                marker.icon = this.categoriesMap.getHcImageSource([establishment.hcCategory]);
            }
            marker.position = Position.positionFromLatLng(
                establishment.geoLocation.coordinates[1],
                establishment.geoLocation.coordinates[0]
            );
            marker.userData = brokenMarker.userData;
            this.mapView.addMarker(marker);
            this.markers.push(marker);
        });
        setInterval(() => this.cachedMarkers.enabled = false);
    }
    /**
     * @method selectCurrentLocation
     * Change from area only if user added a search criteria
     */
    selectCurrentLocation(next: Function): void {
        if (this.fromBack || this.fromSearch) {
            if (this.criteria && this.criteria.area == null) {
                this.criteria.area = '';
            }
        }
        console.log('SELECTING CURRENT LOCATION', JSON.stringify(this.criteria));
        if (!this.criteria || (this.criteria && this.criteria.area === '')) {
            console.log('PASSING CURRENT POSITION FROM MEMORY');
            this.fromSearch = false;
            next(this.positions.current, false);
        } else if (this.criteria && this.criteria.area != null && this.criteria.area !== '') {
            console.log('PASSING AREA POSITION FROM API');
            this.area.find({
                where: {
                    name: this.criteria.area
                }
            }).subscribe((areas: Area[]) => {
                this.searchOptions.invalidArea = false;
                if (Array.isArray(areas) && areas.length > 0) {
                    this.states.FARAWAY_SEARCH_LOAD = true;
                    let area: Area = areas.pop();
                    this.centered = false; // Needed to tell the map we are in a new area.
                    // Needed because this time the update was not due camera change, so only there
                    // we are setting lastUpdated, so here is also needed since no camera was updated -yet-. (RACE CONDITION)
                    // STILL NOT WORKING DUDE. (THOUGH IS A LOW LEVEL ISSUE, showing loading when selecting a searched area item)
                    this.positions.lastUpdated = Position.positionFromLatLng(area.center.lat, area.center.lng);
                    if (this.positionBeforeNavigate && this.positionBeforeNavigate.lat) {
                        this.positionBeforeNavigate = {};
                        this.storage.set('positionBeforeNavigate', this.positionBeforeNavigate);
                    } else {
                        this.setCurrentPosition(this.positions.lastUpdated);
                    }
                    //this.loadCurrentPosition();
                    // this.isEmptyArea(area.id); buggy approach
                    next(this.positions.current, true, area);
                } else {
                    next(this.positions.current, false);
                }
            }, (err: Error) => {
                this.errorHandler(err)
                this.searchOptions.invalidArea = true;
                this.loader.instance.hide();
                this.searchOptions.emptyArea = false;
                next(this.positions.current, false);
            })
        }
    }

    isEmptyArea(areaId: string): void {
        this.area.countEstablishments(areaId).subscribe(
            (res: any) => {
                if (res.count == 0) {
                    this.searchOptions.emptyArea = true;
                } else {
                    this.searchOptions.emptyArea = false;
                }
            },
            error => this.errorHandler(error)
        )
    }
    /**
     * @method expand
     * @description will expand the map to see all markers
     */
    expand(isOwner): void {
        this.isHistoryOwner = isOwner;
        if (this.search.enabled) {
            this.showBack = true;
        }
        this.blockCameraChange = true;
        this.search.enabled = false;
        if (this.markers.length == 0) {
            return;
        }
        if (isIOS) {
            let bounds = new GMSCoordinateBounds(this.positions.current, this.positions.current);
            this.markers.forEach((marker: Marker) => {
                console.log('B');
                bounds = bounds.includingCoordinate(marker.position);
            });
            this.mapView.gMap.animateWithCameraUpdate(GMSCameraUpdate.fitBoundsWithPadding(bounds, 50.0));
        } else {
            let b = new com.google.android.gms.maps.model.LatLngBounds.Builder();
            this.markers.forEach((marker: Marker) => {
                let p = new com.google.android.gms.maps.model.LatLng(marker.position.latitude, marker.position.longitude);
                b.include(p);
            });
            let bounds = b.build();
            let cu = com.google.android.gms.maps.CameraUpdateFactory.newLatLngBounds(bounds, 50.0);
            this.mapView.gMap.animateCamera(cu);
        }
    }

    onSearchBack() {
        this.isInSearchMode = false;
        this.actionBarTitle = "Trending";
        this.fromSearch = true;
        this.onCameraChanged({ camera: this.positions.lastPosition });
        this.zone.run(() => {
            this.removeCurrentEstablishmentMarkers();
        })
        this.search.enabled = false;
        this.searchOptions.invalidArea = false;
        this.searchOptions.emptyArea = false;
        this.searchOptions.emptySearch = false;
        //this.showBack = false;
        if (this.criteria && this.criteria.text) {
            this.criteria.text = '';
        }
        if (this.criteria && this.criteria.lastArea) {
            this.criteria.lastArea = '';
        }
        if (this.criteria && this.criteria.area) {
            this.criteria.area = '';
        }
        if (this.criteria && this.criteria.category) {
            this.criteria.category = '';
        }
        this.setCurrentPosition(this.positions.lastPosition);
        this.loadCurrentPosition();
        //this.animateMenu('bottom')
        if (isIOS) {
            timer.setTimeout(() => {
                let x = this.getScreenCoordinates();
                this.watchLocationEstablishments();
            }, 150)
        }
        setTimeout(() => {
            this.restoreEstablishmentMarkers();
        }, 1800)
    }

    saveLastPosition() {
        if (
            this.positions &&
            this.positions.current &&
            this.positions.current.latitude &&
            this.positions.current.longitude
        ) {
            this.positions.lastPosition = this.positions.current;
        }
    }
    /**
     * @method loadUserMarker
     */
    loadUserMarker(): void {
        if (this.showOneEstablishment) {
            return;
        }
        // If there is no map loaded, there is nothing to do.
        if (!this.mapView || !this.positions.user) {
            return console.log('UNABLE TO LOAD USER MARKER, NO MAP VIEW OR USER POSITION');
        }
        // Remove previous marker
        if (this.userMarker) {
            //           this.mapView.removeMarker(this.userMarker);
        }
        //
        this.userMarker = new Marker();
        //        this.userMarker.color = "blue";
        //        let icon: Image = new Image();
        //        let iconName = 'bmarker_profile';
        //        if (this.account) {
        //            this.userMarker.userData = { account: this.account };
        //            iconName = this.account.gender === 'male' ? iconName : 'bmarker_profile_w';
        //
        //        }
        //          icon.imageSource = fromResource(iconName);
        //          icon.borderRadius = 5;
        //          icon.width = 60;
        //          icon.height = 60;
        //          icon.className = 'img-circle';
        //          icon.stretch = 'fill';
        this.userMarker.position = Position.positionFromLatLng(
            this.positions.user.latitude,
            this.positions.user.longitude
        );
        //        this.userMarker.icon = icon;
        //        this.mapView.addMarker(this.userMarker);

    };
    /**
     * @method loadHistoryMode
     * @return {void}
     * It will load markers from user history
     */
    loadHistoryMode(): void {
        console.log('LOADING HISTORY MODE');
        this.history.enabled = true;
        this.cameraMovementSettings(true);
        // this.watchLocationEstablishments();
    }
    /**
     * @method sortMarkersByProximityFrom
     * @return {void}
     */
    sortMarkersByProximityFrom(from: Position): void {
        this.markers.sort((a: Marker, b: Marker) => {

            let establishmentA: Establishment = a.userData.establishment;
            let establishmentB: Establishment = b.userData.establishment;

            let positionA: Position = new Position();
            positionA.latitude = establishmentA.geoLocation.coordinates[1];
            positionA.longitude = establishmentA.geoLocation.coordinates[0];

            let positionB: Position = new Position();
            positionB.latitude = establishmentB.geoLocation.coordinates[1];
            positionB.longitude = establishmentB.geoLocation.coordinates[0];

            let distanceA: number = this.calculateDistance(from, positionA);
            let distanceB: number = this.calculateDistance(from, positionB);

            if (distanceA < distanceB) {
                return -1;
            } else if (distanceA > distanceB) {
                return 1;
            } else {
                return 0;
            }
        });
    }
    /**
     * @method onCameraChanged
     * @param {camera: Position} args
     * @return {void}
     * This method avoids memory leaks, it avoid requesting the server on each camera change.
     * When users navigate fast, making server calls and registering events casuses several issues.
     * It will also avoid requesting the server if the navigation is less than 1 km from current point
     */
    onCameraChanged(args: any) {

        console.log('MAP VIEW ZOOM', this.mapView.zoom);
        console.log('CHANGED', args.camera.latitude, args.camera.longitude);

        this.storage.set('currentZoom', this.mapView.zoom);

        if (this.blockCameraChange) {
            this.blockCameraChange = false;
            return;
        }

        if (this.cameraCounter.timeout) {
            timer.clearTimeout(this.cameraCounter.timeout);
            this.cameraCounter.timeout = null;
        }

        this.setCurrentPosition(
            Position.positionFromLatLng(args.camera.latitude, args.camera.longitude)
        );

        this.centered = this.verifyIfCenteredTo(this.positions.user);

        if (!this.centered) {
            this.unloadCurrentEstablishment();
        }

        if (this.showBack) {
            this.locationFromSearch = {
                latitude: args.camera.latitude,
                longitude: args.camera.longitude
            }
        } else {
            this.locationFromSearch = {};
        }

        this.positions.lastUpdated = args.camera;
        this.watchLocationEstablishments();
    }

    onTapSearch(): void {
        this.search.enabled = true;
        this.isInSearchMode = true;
        this.actionBarTitle = "Search";
        this.saveCurrentEstablishments();
        this.watchLocationEstablishments()
        this.saveLastPosition();
        this.closeMenu();
    }

    /**
     * @method findNearByEstablishment
     * @param {camera: Position} args
     * @return {void}
     * This will load the closest establishment from center of the camera,
     * and also will calculate several distance states. It will automatically
     * load a nearby establishment if the usear is around or if the user
     * is navigating through the map.
     */
    findNearByEstablishment(camera: Position): void {
        // If there are markers to process then we procceed
        if (this.markers.length > 0 && this.settings.CALCULATE_DISTANCES) {
            let establishmentPosition: Position;
            // This condition is super important because we don't want process both at once.
            // These will be processed differently depending if the map is centered or not.
            if (this.centered) {
                // Order markers by proximity from user position since we need to know
                // which is the closest establishment from user regardless of camera position
                this.sortMarkersByProximityFrom(this.positions.user);
                this.establishment.closestFromUser = this.markers[0].userData.establishment;
                establishmentPosition = new Position();
                establishmentPosition.latitude = this.establishment.closestFromUser.geoLocation.coordinates[1];
                establishmentPosition.longitude = this.establishment.closestFromUser.geoLocation.coordinates[0];
                this.distances.closestEstablishmentFromUser = this.calculateDistance(
                    this.positions.user,
                    establishmentPosition
                );
                // In this case the center and user is the same, so distances are the same
                this.establishment.closestFromCenter = this.establishment.closestFromUser;
                this.distances.closestEstablishmentFromCenter = this.distances.closestEstablishmentFromUser;
            } else {
                // Order markers by proximity from camera position since we need to know
                // What is the closest establishment from
                this.sortMarkersByProximityFrom(camera);
                this.establishment.closestFromCenter = this.markers[0].userData.establishment;
                establishmentPosition = new Position();
                establishmentPosition.latitude = this.establishment.closestFromCenter.geoLocation.coordinates[1];
                establishmentPosition.longitude = this.establishment.closestFromCenter.geoLocation.coordinates[0];
                // In this case
                this.distances.closestEstablishmentFromCenter = this.calculateDistance(
                    camera,
                    establishmentPosition
                );
            }
            // If the camera is centered into the real user location and the closest
            // establishment is around 150 meters from the user, then we
            // load the establishment, though users should be able to select their-selves
            if (this.distances.closestEstablishmentFromCenter > this.settings.LOAD_NEARBY_MARKER) {
                return this.unloadCurrentEstablishment();
            }

            let visit;
            if (this.history.enabled) {
                visit = this.markers[0].userData.visit;
            }

            if (this.settings.LOAD_NEARBY_MARKER > 0) {
                console.log("Establishment marker: ", this.markers[0].userData.index);
                // IF custom markers we need to highlight a marker for closest establishment
                if (this.settings.CUSTOM_MARKERS) {
                    if (this.centered) {
                        this.highLightEstablishment(this.establishment.closestFromUser, visit, this.markers[0].userData.index);
                    } else {
                        this.highLightEstablishment(this.establishment.closestFromCenter, visit, this.markers[0].userData.index);
                    }
                }
            }
        }
    }

    onCardFocus(focused: CarouselItem) {
        console.log("this.highLightEstablishment");
        console.dir(focused.item);
        this.highLightEstablishment(focused.item.userData.establishment, focused.item.userData.visit, focused.index);
    }

    highLightEstablishment(establishment: Establishment, visit?: Visit, index: number = 0) {
        console.log('HIGHLIGHTING ESTABLISHMENT', establishment.name);
        if (this.settings.CUSTOM_MARKERS) {
            if (this.mapView) {
                if (this.highlightMarker) {
                    if (this.highlightMarker.userData.establishment.id === establishment.id) {
                        this.routerExtensions.navigate(["/establishment", establishment.id]);
                        return console.log('Marker has been already highlighted');
                    } else {
                        this.mapView.removeMarker(this.highlightMarker);
                    }
                }
                console.log('CREATING NOW HIGHLIGHTED ESTABLISHMENT', establishment.name);
                //this.establishment.selected = establishment;
                this.loadCurrentEstablishment(establishment);
                this.zone.run(() => {
                    this.highlightMarker = new Marker();
                    //this.highlightMarker.icon = this.categoriesMap.getImageSource(establishment.categories, true);
                    this.highlightMarker.icon = this.categoriesMap.getHcImageSource([establishment.hcCategory], true);
                    this.highlightMarker.position = Position.positionFromLatLng(establishment.geoLocation.coordinates[1], establishment.geoLocation.coordinates[0]);
                    this.highlightMarker.userData = { establishment, index, visit };
                    this.highlightMarker.zIndex = 10;
                    this.mapView.addMarker(this.highlightMarker);
                    console.log("************************************************** MARKER HIGHTLIGHTED IS " + this.highlightMarker.position);
                    // Calculate selected distance from user
                    this.distances.selectedEstablishmentFromUser = this.calculateDistance(
                        this.highlightMarker.position,
                        this.positions.user
                    );
                    // Added here to allow future navigation results
                    this.establishmentProfile.enabled = false;
                });

            } else {
                console.log("****************************************************************** MAP VIEW PROBLEM!!!!!");
                setTimeout(() => this.highLightEstablishment(establishment, visit, index), 100);
            }
        }
    }
    /**
     * @method onVisitSelect
     * @param {any} args
     * @return {void}
     */
    onVisitSelect(visit: Visit) {
        this.markers.forEach((marker: Marker) => {
            if (visit.location.establishment.id === marker.userData.establishment.id) {
                this.setCurrentPosition(Position.positionFromLatLng(
                    marker.userData.establishment.geoLocation.coordinates[1],
                    marker.userData.establishment.geoLocation.coordinates[0]
                ));
                this.loadCurrentPosition();
                // Set Map Zoom
                this.mapView.zoom = this.settings.SET_MAX_ZOOM;
            }
        });
    }
    /**
     * @method onMarkerSelect
     * @param {any} args
     * @return {void}
     */
    onMarkerSelect(args: any, center: boolean = false) {
        //console.log('MARKER SELECTED', args.marker.userData.establishment.name);
        this.tappedMarker.enabled = true;
        this.zone.run(() => {
            // If we select a marker, either by taping a marker or an item within the any establishment list.
            if (
                args.marker &&
                args.marker.userData &&
                args.marker.userData.establishment &&
                args.marker.userData.establishment.id
            ) {
                // Add reference for location to get right Location Type
                // this.establishment.selected = args.marker.userData.establishment;
                this.loadCurrentEstablishment(args.marker.userData.establishment);
                let establishmentPos: Position = Position.positionFromLatLng(
                    this.establishment.selected.geoLocation.coordinates[1],
                    this.establishment.selected.geoLocation.coordinates[0]
                );
                // Calcualte distance between user and selected establishment
                this.distances.selectedEstablishmentFromUser = this.calculateDistance(
                    establishmentPos,
                    this.positions.user
                );

                this.highLightEstablishment(this.establishment.selected, args.marker.userData.visit, args.marker.userData.index);
                if (center) {
                    this.setCurrentPosition(establishmentPos);
                    this.loadCurrentPosition();
                }
                // If center flag is set, we need to position the user to the marker selection
                // This is mosthly when other components like search select an establishment
                // and we want to move the camera to that place.
                // This won't be executed if taping the actual markers on the map, because
                // Google maps will automatically pan, and then onCameraChanged will be called.
                // Then we don't need to call the following, otherwise is a must
                // NOTE we are using center and not this.center, these are 2 totally different things.
                //if (center) {
                //this.onCameraChanged({ camera: establishmentPos });
                //this.setCurrentPosition(establishmentPos);
                //this.loadCurrentPosition();
                // Set Map Zoom
                // this.mapView.zoom = this.settings.SET_MAX_ZOOM; should not be set in here
                //}
            } else if (
                args.marker &&
                args.marker.userData &&
                args.marker.userData.account &&
                args.marker.userData.account.id
            ) {
                this.routerExtensions.navigate(['/user-profile/' + args.marker.userData.account.id, { account: JSON.stringify(this.account) }]);
            }
        });
        return true;
    }
    /**
     * @method setUserPosition
     * @param {Position}
     * @return {void}
     **/
    setUserPosition(position: geolocation.Location): void {
        this.storage.set('userPosition', {
            latitude: position.latitude,
            longitude: position.longitude,
            horizontalAccuracy: position.horizontalAccuracy,
            verticalAccuracy: position.verticalAccuracy,
            speed: position.speed,
            direction: position.direction
        });
        this.positions.user = position;
        // this.setCurrentPosition(position); Generates bug on back, when tries to go back to navigated current, not user current
    }
    /**
     * @method setCurrentPosition
     * @param {Position}
     * @return {void}
     **/
    setCurrentPosition(position: Position): void {
        if (position && position.latitude !== 0 && position.longitude !== 0) {
            console.log('SETTING CURRENT POSITION', position.latitude, position.longitude);
            if (!this.accountHistoryId) {
                this.storage.set('currentPosition', { latitude: position.latitude, longitude: position.longitude });
                if (this.mapView && this.mapView.zoom) {
                    this.storage.set('currentZoom', this.mapView.zoom);
                }
            }
            this.positions.current = position;
        }
    }
    /**
     * @method loadCurrentPosition
     * @param {Position}
     * @return {void}
     **/
    loadCurrentPosition(): void {
        console.log('LOADING CURRENT POSITIOM');
        if (
            !this.positions.current ||
            !this.positions.current.latitude ||
            !this.positions.current.longitude
        ) {
            return;
        }

        if (this.mapView && this.positions.current) {
            console.log('MOVING MAP TO CURRENT POSITION', this.positions.current.latitude, this.positions.current.longitude);
            this.zone.run(() => {
                this.mapView.latitude = this.positions.current.latitude;
                this.mapView.longitude = this.positions.current.longitude;
            })
            // Set Map Zoom
            // this.mapView.zoom = this.settings.SET_MIN_ZOOM; shouldn't be set while centering generates bad experience
        }
    }
    /**
     * @method loadUserPosition
     * @param {Position}
     * @return {void}
     **/
    loadUserPosition(): void {
        console.log('LOADING USER POSITION:', this.positions.user.latitude, this.positions.user.longitude);
        if (this.positions.user) {
            this.centered = true;
            if (this.mapView) {
                this.mapView.zoom = this.settings.SET_MAX_ZOOM;
                this.mapView.latitude = this.positions.user.latitude;
                this.mapView.longitude = this.positions.user.longitude;
            }
        }
    }
    /**
     * @method unloadCurrentEstablishment
     * @param {Position}
     * @return {void}
     **/
    unloadCurrentEstablishment(): void {
        if (this.showOneEstablishment) {
            return;
        }
        this.establishment.selected = null;
        this.animateMenu('bottom');
        // Create Highlight Marker
        if (this.mapView && this.highlightMarker) {
            this.mapView.removeMarker(this.highlightMarker);
            this.highlightMarker = null;
        }
    }

    loadCurrentEstablishment(establishment: Establishment): void {
        let prevState = !!this.establishment.selected;
        this.establishment.selected = establishment;
        if (!prevState) {
            this.animateMenu('top');
        }
    }

    animateMenu(position: string) {
        let menuContainer = <View>this.page.getViewById('menu-container');
        if (menuContainer) {
            //let positionY = position == 'bottom' ? -95 : -190;
            let positionY = position == 'bottom' ? -65 : -160;
            menuContainer.animate({
                translate: { x: 0, y: positionY },
                duration: 300,
                curve: AnimationCurve.easeOut
            })
        } else {
            setTimeout(() => {
                this.animateMenu(position);
            }, 100);
        }
    }

    /**
     * @method findCachedMarker
     * @param {Establishment}
     * @return {Marker}
     **/
    findCachedMarker(establishment: Establishment): Marker {
        let filtered = this.markers.filter((marker: Marker) => {
            return marker.userData.establishment.id === establishment.id;
        });
        if (filtered.length > 0) {
            return filtered.pop();
        } else {
            return null;
        }
    }
    /**
     * @calculateDistance
     * @param {Position} a Location A
     * @param {Position} b Location B
     * @return {number} Delta distance between location A and B
     **/
    calculateDistance(a: Position, b: Position): number {
        let aLocation = new geolocation.Location();
        let bLocation = new geolocation.Location();
        aLocation.latitude = a.latitude || 0;
        aLocation.longitude = a.longitude || 0;
        bLocation.latitude = b.latitude || 0;
        bLocation.longitude = b.longitude || 0;
        return geolocation.distance(bLocation, aLocation);
    }

    /**
     * @calculateDistance
     * @param {Visit[]} visits
     **/
    loadVisits(visits: Visit[]): void {
        let visitsEstablishments: VisitEstablishment[] = visits.filter((visit: Visit) => visit.location.establishment ? true : false)
            .map((visit: Visit, index) => {
                return { visit: visit, establishment: visit.location.establishment, index: index }
            });
        if (this.settings.WIPE_MARKERS) {
            this.removeCurrentEstablishmentMarkers();
            // console.log("====================================================================CALLED FROM HERE5");
            this.loadEstablishmentMarkers(visitsEstablishments);
        } else {
            // console.log("====================================================================CALLED FROM HERE6");
            this.loadEstablishmentMarkers(
                this.removeCurrentEstablishmentMarkers(visitsEstablishments)
            );
        }
    }
    /**
     * @method getCurrentUserAccount
     **/
    getCurrentUserAccount(): void {
        this.subscriptions.push(
            this.accountApi.getCurrent().subscribe(
                (account: Account) => {
                    this.account = account;
                    this.loadUserMarker();
                },
                error => this.errorHandler(error)
            )
        );
    }

    /**
     * @method onCardTap
     * @return {void}
     **/
    onCardTap(selected: CarouselItem) {
        console.log('TAP ON NAVIGATE2');
        if (this.showBack || this.history.enabled) {
            this.positionBeforeNavigate = {
                lat: this.positions.current.latitude,
                lng: this.positions.current.longitude,
                zoom: this.storage.get('currentZoom')
            }
            this.storage.set('positionBeforeNavigate', this.positionBeforeNavigate);
        }
        console.dir(selected.item.userData.establishment);
        console.log(selected.item.userData.establishment.id);
        // this.routerExtensions.navigate([
        //     '/establishment/',
        //     selected.item.userData.establishment.id
        // ], {
        //         transition: {
        //             name: 'slideLeft',
        //             duration: 500,
        //             curve: 'linear'
        //         }
        //     });
        this.routerExtensions.navigate(["/establishment", selected.item.userData.establishment.id]);
    }

    /**
     * @method openNotifications
     * @return {void}
     **/
    openNotifications(): void {
        this.notifications.enabled = true;
    }

    /**
     * @method closeNotifications
     * @return {void}
     **/
    closeNotifications(): void {
        this.notifications.enabled = false;
    }

    /**
     * @method openFriendsList
     * @return {void}
     **/
    openFriendsList() {
        this.showFriends = true;
    }

    /**
     * @method onFriendsClose
     * @return {void}
     **/
    onFriendsClose() {
        this.showFriends = false;
    }

    /**
     * @method shareEstablishment
     * @return {void}
     **/
    shareEstablishment(): void {
        this.share.enabled = true;
    }

    /**
     * @method closeShare
     * @return {void}
     **/
    closeShare() {
        this.share.enabled = false
    }

    /**
     * @method isEstablishmentSelected
     * @return {void}
     **/
    isEstablishmentSelected() {
        if (this.showOneEstablishment) {
            return;
        }
        if (this.showFriends || this.share.enabled) {
            return;
        }
        if (this.showBack && this.establishment.selected) {
            this.unloadCurrentEstablishment();
            return;
        }
        if (this.history.enabled && this.establishment.selected) {
            this.unloadCurrentEstablishment();
            return;
        }
        if (this.establishment.selected) {
            this.back();
        }
    }

    /**
     * @method countHistory
     * @return {void}
     **/
    countHistory(): void {
        let filter = {
            where: {
                accountId: this.accountApi.getCurrentId()
            }
        };
        this.visitApi.find(filter).subscribe(
            (data: Array<Visit>) => {
                if (data && data.length) {
                    this.menuInformation.enableHistory = true;
                    this.menuInformation.unconfirmedHistory =
                        data.reduce((count: any, visit: any) => {
                            return count += visit.validity < 100 ? 1 : 0;
                        }, 0)
                    this.menuInformation.totalCount = this.menuInformation.unconfirmedHistory +
                        this.menuInformation.newNotifications
                } else {
                    this.menuInformation.enableHistory = false;
                    this.menuInformation.unconfirmedHistory = 0;
                }
                console.log('HISTORY COUNT: ', JSON.stringify(this.menuInformation));
            },
            error => this.errorHandler(error)
        )
    }

    /**
     * @method updateNotificationsNumber
     * @return {void}
     **/
    updateNotificationsNumber(data: MenuInfoInterface) {
        console.log('1-- data', JSON.stringify(data))
        // this.newNotifications = data.notViewed;
        this.menuInformation.newNotifications = data.newNotifications;
        this.menuInformation.enableNotifications = data.enableNotifications;
        this.menuInformation.totalCount = this.menuInformation.unconfirmedHistory + data.newNotifications;
    }

    /**
     * @method openRectify
     * @return {void}
     **/
    openRectify(visit: Visit) {
        console.log(JSON.stringify(visit));
        this.visit = visit;
        this.showRectify = true;
    }

    /**
     * @method closeRectify
     * @return {void}
     **/
    closeRectify() {
        if (this.detectionCard.enabled) {
            this.detectionCard.enabled = false;
        }
        this.visit = null;
        this.showRectify = false;
        this.countHistory();
    }

    /**
     * @method confirmVisit
     * @return {void}
     **/
    confirmVisit(visit: Visit): void {
        if (this.detectionCard.enabled) {
            this.detectionCard.enabled = false;
        }
        let _visit: Visit = new Visit();
        _visit.id = visit.id;
        _visit.validity = 100;
        this.visitApi.upsert(_visit).subscribe(
            res => {
                if (this.history.enabled) {
                    this.historyService.refreshHistory();
                }
                this.countHistory();
            },
            error => this.errorHandler(error)
        );
    }

    /**
     * @method rejectVisit
     * @return {void}
     **/
    rejectVisit(visit: Visit): void {
        this.visitApi.deleteById(visit.id).subscribe(
            (data: any) => {
                console.log('deleted', data);
                if (this.history.enabled) {
                    this.historyService.refreshHistory();
                }
                this.countHistory();
            },
            error => this.errorHandler(error)
            // (err: Error) => console.log('Error in rejectVisit', err.message)
        )
        this.closeRectify();
    }

    /**
     * @method rectifyVisit
     * @return {void}
     **/
    rectifyVisit(rectify: { visit: Visit, establishment: Establishment }): void {
        let _visit = new Visit();
        _visit.locationId = rectify.establishment.locationId;
        _visit.validity = 100;
        _visit.id = rectify.visit.id;
        this.fixLocation(rectify.establishment.locationId, rectify.visit.location.id, rectify.visit.id);
        this.visitApi.updateAttributes(_visit.id, _visit).subscribe(
            res => {
                if (this.history.enabled) {
                    this.historyService.refreshHistory();
                }
            },
            error => this.errorHandler(error)
        );
        this.closeRectify();
    }

    /**
     * @method fixLocation
     * @return {void}
     **/
    fixLocation(fixedId: string, incorrectId: string, visitId: string) {
        this.subscriptions.push(
            this.locationFixApi.create({
                fixedId: fixedId,
                incorrectId: incorrectId,
                visitId: visitId
            }).subscribe(res => { },
                error => this.errorHandler(error))
        )
    }

    /**
     * @method showVisitDetectionCard
     * @return {void}
     **/
    showVisitDetectionCard(visit: Visit) {
        this.zone.run(() => {
            this.visit = visit;
            this.detectionCard.enabled = true;
        });
    }

    /**
     * @method openVisitOnHistory
     **/
    openVisitOnHistory(notification: Notification) {
        this.visitHistoryId = notification.visitId;
        this.closeNotifications();
        this.loadHistoryMode();
    }

    /**
     * @method getCurrentLocationName
     **/
    getCurrentLocationName() {
        // let result = this.positions.current.latitude+","+this.positions.current.longitude;
        // this.subscriptions.push(
        //     this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${result}&key=AIzaSyAaGsoyNTNWIgJJwCUn9sagg3bvyvQbRfc`)
        //         .map((res: any) => {
        //             let body = res.json();
        //             return body;
        //         })
        //         .subscribe(
        //             (res: any) => {
        //                 console.log('THIS IS THE RESPONSE!!!! ', JSON.stringify(res));
        //                 let results = res.results;
        //                 let area = '';
        //                 console.log(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${result}&key=AIzaSyAaGsoyNTNWIgJJwCUn9sagg3bvyvQbRfc`);
        //                 if(results && Array.isArray(results) && results.length > 0) {
        //                     results[0].address_components.forEach((address: any) => {
        //                         console.log('AREA NAME', address.long_name);
        //                         if(address.type.indexOf("administrative_area_level_1") > -1) {
        //                             area = address.long_name;
        //                         }
        //                     })
        //                 }
        //                 if(area != '') {
        //                     this.criteria.lastArea = area;
        //                 }
        //                 alert(area);
        //                 this.search.enabled = true;
        //             },
        //             (err: Error) => {
        //                 this.search.enabled = true;
        //             }
        //         )
        // );
        // this.mapService.getDirection({
        //     lat: this.positions.current.latitude,
        //     lng: this.positions.current.longitude
        // }).subscribe((res: any) => {
        //     let results = res.results;
        //     let area = '';
        //     if(results && Array.isArray(results) && results.length > 0) {
        //         results[0].address_components.forEach((address: any) => {
        //             if(address.type.indexOf("administrative_area_level_1") > -1) {
        //                 area = address.long_name;
        //             }
        //         })
        //     }
        //     if(area != '') {
        //         this.criteria.lastArea = area;
        //     }
        //     alert(area);
        // });
        // this.search.enabled = true;
    }

    watchHour(): void {
        if (this.hourSubscription) {
            this.hourSubscription.unsubscribe();
        }
        this.hourSubscription = this.visitDetectionService.onHourChange().subscribe(
            (shift: string) => {
                if (!this.mapView) {
                    // console.log('SHIFT RECIEVED BUT NO MAP READY')
                    return;
                }
                // console.log('SHIFT RECIEVED: ', shift,', CURRENT: ', this.isNight ? 'NIGHT' : 'DAY')
                if (shift === 'day' && this.isNight) {
                    this.isNight = false;
                    this.mapView.setStyle(dayStyle)
                } else if (shift === 'night' && !this.isNight) {
                    this.isNight = true;
                    this.mapView.setStyle(nightStyle)
                } else {
                    // console.log('NO CHANGES ON SHIFT');
                }
            }
        )
    }

    /**
     * @method ngOnInit
     **/
    ngOnInit() {
        this.loader.instance.show(this.loader.options);
        //this.page.actionBarHidden = true;
        this.actionBarTitle += "";
        this.getCurrentUserAccount();
        this.countHistory();
        this.loader.instance.hide();

        // Listen to iOS-only BackgroundFetch events.
        /* BackgroundFetch.configure({stopOnTerminate: false}, () => {
             console.log('[event] BackgroundFetch');
             BackgroundFetch.finish(UIBackgroundFetchResult.NewData);
           });*/

        // 2. Configure it.
        /*  BackgroundGeolocation.configure({
              debug: true,
              desiredAccuracy: 0,
              stationaryRadius: 25,
              distanceFilter: 10,
              activityRecognitionInterval: 10000,
              useSignificantChangesOnly: true,
              stopOnTerminate: false,
              autoSync: true
          }, (state) => {
              // 3. Plugin is now ready to use.
              //alert("state: " + state.stopOnTerminate);
              //state.stopOnTerminate = false;
              //console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa => " + (state.stopOnTerminate));
              if (!state.enabled) {
                  // BackgroundGeolocation.start();
              }
          });*/
    }

    //....NavigatinMenu...Setup............
    @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
    private drawer: RadSideDrawer;

    ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
        //this._sideDrawerTransition = new PushTransition();
        this._changeDetectionRef.detectChanges();
    }

    public openDrawer() {
        this.drawer.toggleDrawerState();
    }

    /**
     * @method ngOnDestroy
     **/
    ngOnDestroy() {
        if (this.watchId) {
            geolocation.clearWatch(this.watchId);
        }
        //this.visitRef.dispose();
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
        this.markers = new Array<Marker>();
        this.mapView = null;
    }

    errorHandler(error: any): void {
        console.log('Map Error', error.statusCode, error.statusCode == 401 ? 'si' : 'no');
        if (error && error.statusCode == 401) {
            this.logoutService.logout();
        }
    }
}
/*
{"limit": 10, "fields": ["name"], "where": {"geoLocation":{"geoWithin":{"$centerSphere":[[-73.9441580325365,40.678178285889615],0.0012616067823980623]}}}}

{"where":{"geoLocation":{"geoWithin":{"$centerSphere":[[-73.9441580325365,40.678178285889615],0.0012616067823980623]}}},"limit":25,"fields":["id","name","description","geoLocation","address","categories","locationId"],"include":{"relation":"photos"}}
*/