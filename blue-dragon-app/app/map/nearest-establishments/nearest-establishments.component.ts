import {
    Component, OnInit, Input,
    Output, EventEmitter, SimpleChange,
    OnChanges, ChangeDetectionStrategy
} from '@angular/core';
import { ObservableArray } from 'data/observable-array';
import { Establishment, Location, FireLoopRef } from '../../shared/sdk/models';

@Component({
    moduleId: module.id,
    selector: 'nearest-establishments',
    styleUrls: ['nearest-establishments.component.css'],
    templateUrl: 'nearest-establishments.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NearestEstablishmentsComponent implements OnInit, OnChanges {
    @Input() locations: Array<Location>;
    @Output() onSelect = new EventEmitter();

    locationsArray: Array<Location>;

    constructor() { }

    ngOnInit() { }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        if(changes['locations'].currentValue) {
            this.locationsArray =
                changes['locations'].currentValue;
        }
    }

    select(location: Location) {
        console.log('location', JSON.stringify(location));
        this.onSelect.emit(location);
    }

    close() {
        this.onSelect.emit(false);
    }
}