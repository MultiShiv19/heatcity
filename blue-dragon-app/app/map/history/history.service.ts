import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

interface NotificationInterface {
    text: string;
    status: boolean;
}

@Injectable()
export class HistoryService {
    private historySubject: Subject<void> = new Subject<void>();
    private markersSubject: Subject<any> = new Subject<any>();

    refreshHistory(): void {
        this.historySubject.next();
    }

    onRefreshHistory(): Observable<any> {
        return this.historySubject.asObservable();
    }

    getMarkers(): void {
        this.markersSubject.next();
    }

    onGetMarkers(): Observable<any> {
        return this.markersSubject.asObservable();
    }
}