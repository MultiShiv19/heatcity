import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, NgZone } from '@angular/core';
import * as googleAnalytics from "nativescript-google-analytics";
import { MenuInfoInterface } from '../../shared/interfaces/menu-info.interface';
import { Page } from 'ui/page';
import { Animation, AnimationDefinition } from "ui/animation";
import { AnimationCurve } from "ui/enums";
import timer = require('timer');

const DIRECTION = {
	topToBottom: 1,
	bottomToTop: 2
}

@Component({
  moduleId: module.id,
  selector: 'MenuComponent',
  templateUrl: 'menu.component.html',
  styleUrls: ['menu.component.css']
})

export class MenuComponent implements OnInit, AfterViewInit {

	@Input('menuInformation') menuInfo: MenuInfoInterface = {enableNotifications: true};
	@Input('isNight') isNight: boolean;
	@Output('onNotificationsTap') onNotificationsTap: EventEmitter<null> = new EventEmitter<any>();
	@Output('onFriendsTap') onFriendsTap: EventEmitter<null> = new EventEmitter<any>();
	@Output('onListTap') onListTap: EventEmitter<null> = new EventEmitter<any>();
	@Output('onHistoryTap') onHistoryTap: EventEmitter<null> = new EventEmitter<any>();
	@Output('onCloseTap') onCloseTap: EventEmitter<null> = new EventEmitter<any>();
	gradient: Array<number> = [0,0,0,0];

	constructor(
		private page: Page,
		private zone: NgZone
	) {
		googleAnalytics.logView('menu');
	}

	ngOnInit() { }

	ngAfterViewInit(): void {
		this.animateButtons(DIRECTION.bottomToTop);
	}

	animateButtons(direction: number): void {
        let size = this.page.getActualSize();
		let willHide: boolean = direction == DIRECTION.topToBottom;
		let yCoordinate = willHide ? size.height : 0;
        let buttons: any = this.page.getViewById('buttons');
        if (buttons) {
			if (!willHide) {
				buttons.translateY = size.height;
			}
			this.addColorAnimation(direction);
            buttons.animate({
                translate: { x: 0, y: yCoordinate},
                duration: 500,
				curve: willHide ? AnimationCurve.easeIn : AnimationCurve.easeOut
            }).then(() => {
				if (willHide) {
					this.onCloseTap.next();
				}
			});
        }
		
	}

	addColorAnimation(direction: number): void {
		let willHide = direction == DIRECTION.topToBottom;
		let interval = timer.setInterval(() => {
			if (
				(this.gradient[0] === 150 && !willHide) ||
				(this.gradient[0] === 0 && willHide)
			) {
				timer.clearInterval(interval);
			}else {
				this.zone.run(() => {
					if (willHide) {
						this.gradient[0] -= 15;
					}else {
						this.gradient[0] += 15;
					}
					this.gradient = this.gradient.slice();
				});
			}
		}, 50);
	}

	onTapNotifications(): void {
		if (
			this.menuInfo &&
			this.menuInfo.enableNotifications
		) {
			this.onNotificationsTap.next();
		}
	}

	onTapHistory(): void {
		if (
			this.menuInfo &&
			this.menuInfo.enableHistory
		) {
			this.onHistoryTap.next();
		}
	}

	closeMenu(): void {
		this.animateButtons(DIRECTION.topToBottom)
	}
}