import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Position } from 'nativescript-google-maps-sdk';
import { Visit, Establishment } from '../../shared/sdk/models';
import geolocation = require('nativescript-geolocation');

@Injectable()
export class VisitDetectionService {
  private visitSubject: Subject<any> = new Subject<any>();
  private locationSubject: Subject<{ position: geolocation.Location, updateUser: boolean }> = new Subject<{ position: geolocation.Location, updateUser: boolean }>();
  private startVisitDetectorSubject: Subject<void> = new Subject<void>();
  private hourSubject: Subject<string> = new Subject<string>();
  private currentLocationSubject: Subject<{ position: geolocation.Location }> = new Subject<{ position: geolocation.Location }>();

  newVisit(visit: Visit, establishment: Establishment): void {
    this.visitSubject.next({ visit, establishment });
  }

  onNewVisit(): Observable<any> {
    return this.visitSubject.asObservable();
  }

  newUserPosition(position: geolocation.Location, updateUser: boolean): void {
    this.locationSubject.next({ position, updateUser });
  }

  onNewUserPosition(): Observable<{ position: geolocation.Location, updateUser: boolean }> {
    return this.locationSubject.asObservable();
  }

  newUserCurrentPosition(position: geolocation.Location): void {
    this.currentLocationSubject.next({ position });
  }

  onNewUserCurrentPosition(): Observable<{ position: geolocation.Location }> {
    return this.currentLocationSubject.asObservable();
  }

  startVisitDetector(): void {
    this.startVisitDetectorSubject.next();
  }

  onStartVisitDetector(): Observable<void> {
    return this.startVisitDetectorSubject.asObservable();
  }

  hourChange(shift: string): void {
    this.hourSubject.next(shift);
  }

  onHourChange(): Observable<string> {
    return this.hourSubject.asObservable();
  }

}