import * as app from "application";
import * as utils from "utils/utils";
import * as googleAnalytics from "nativescript-google-analytics";
var firebaseCommon = require("nativescript-plugin-firebase/firebase-common");
import * as branch from "nativescript-branch-io-sdk";
import { Accuracy } from "ui/enums";
import {BackgroundFetch} from "nativescript-background-fetch";

declare var FBSDKApplicationDelegate;
declare var FBSDKAppEvents;
declare var interop;
declare var UIApplicationStateInactive;
declare var UIApplicationStateBackground;
export function delegate(){
    if(app.ios) {
        return class MyDelegate extends UIResponder implements UIApplicationDelegate {
            public static ObjCProtocols = [UIApplicationDelegate];
            public static ObjCExposedMethods = {
                "runOnBackground": { returns: interop.types.void }
            };

            public visitCounter;
            public visitDetector;
            public bgTask;

            public locationManager: CLLocationManager;

            public applicationDidFinishLaunchingWithOptions(application, launchOptions) {
                console.log("APP LAUNCHING");
/*, branch.deepLinkHandler(branch.params, branch.error) => {
import * as branch from "nativescript-branch-io-sdk";
      						if (branch.error == null) {
          					console.log("params:", branch.params.description);
      						}
  							});
var branch = require("nativescript-branch-io-sdk");
*/
                if(launchOptions) {
		 initBranch(launchOptions);
                    /**
                     * If the app was terminated and the iOS is launching it in result of push
                     * notification tapped by the user, this will hold the notification data.
                     */
                    var remoteNotification = launchOptions.objectForKey(UIApplicationLaunchOptionsRemoteNotificationKey);
                    if (remoteNotification) {
                        handlePushNotification(application, remoteNotification);
                    }
                }
                initAnalytics();
                FBSDKApplicationDelegate.sharedInstance().applicationDidFinishLaunchingWithOptions(application, launchOptions);
                return true;
            }

            public applicationOpenURLSourceApplicationAnnotation(application, url, sourceApplication, annotation) {
                return FBSDKApplicationDelegate.sharedInstance().applicationOpenURLSourceApplicationAnnotation(application, url, sourceApplication, annotation);
            }

            public applicationDidBecomeActive(application) {
                console.log("APP BECOME ACTIVE");
                FBSDKAppEvents.activateApp();
                //this.visitCounter = 0
                //this.endBackgroundTask(application);
            }

            public applicationWillTerminate(application) {}

            public applicationDidReceiveRemoteNotification(application, userInfo) {
                if ( application.applicationState == UIApplicationStateInactive )
                {
                    /**
                     * If the app is in background and the iOS is launching it in result of push
                     * notification tapped by the user, this method will be executed.
                     */
                    console.log('OPENED FROM PUSH NOTIFICATION');
                    handlePushNotification(application, userInfo);
                }
            }

            /**
             * In this method we create a timer background task just to keep the CPU alive
             * this way the visit detector is still alive when the app is on the background.
             */
            public applicationDidEnterBackground(application) {
                /*console.log('SETTING UP BACKGROUND TIMER');
                this.bgTask = application.beginBackgroundTaskWithNameExpirationHandler("MyTask", () => {
                    this.endBackgroundTask(application);
                });
                this.visitCounter = 0;
                this.visitDetector = NSTimer.scheduledTimerWithTimeIntervalTargetSelectorUserInfoRepeats(1, this, "runOnBackground", null, true);*/
            }

            /**
             * Method to finish background task and reset timer values
             */
            private endBackgroundTask(application: UIApplication): void {
                if (this.visitDetector) {
                    this.visitDetector.invalidate();
                    this.visitDetector = null;
                }
                this.visitCounter = 0;
                application.endBackgroundTask(this.bgTask);
                this.bgTask = UIBackgroundTaskInvalid;
                console.log("END OF BACKGROUND TIMER");
            }

            public runOnBackground(): void {
                console.log(`BACKGROUND TIMER: ${this.visitCounter}`);
                this.visitCounter++;
            }

            //background-location plugin
            public applicationPerformFetchWithCompletionHandler(application: UIApplication, completionHandler:any) {
                BackgroundFetch.performFetchWithCompletionHandler(application, completionHandler);
            }
        };
    }
};

function initBranch(launchOptions){
   // branch.initializeIOS(launchOptions);
}

function initAnalytics(){
    googleAnalytics.initalize(<any>{
        trackingId: "UA-93149103-1",
        dispatchInterval: 120,
        logging: {
            native: true,
            console: false
        },
        enableDemographics: true
    });
}


function handlePushNotification(app, userInfo) {
    var userInfoJSON = firebaseCommon.firebase.toJsObject(userInfo);
    var aps = userInfo.objectForKey("aps");
    if (aps !== null) {
        var alrt = aps.objectForKey("alert");
        if (alrt !== null && alrt.objectForKey) {
            userInfoJSON.title = alrt.objectForKey("title");
            userInfoJSON.body = alrt.objectForKey("body");
        }
    }

    firebaseCommon.firebase._pendingNotifications.push(userInfoJSON);
    userInfoJSON.foreground = false;
    if (firebaseCommon.firebase._receivedNotificationCallback !== null) {
        firebaseCommon.firebase._processPendingNotifications();
    }
}
