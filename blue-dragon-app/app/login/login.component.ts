import { Component } from "@angular/core";
import { topmost } from "ui/frame";
import { isIOS } from "platform";
import { Color } from "color";
import { Page } from "ui/page";
import * as googleAnalytics from "nativescript-google-analytics";
import { openUrl } from 'utils/utils';

@Component({
    selector: "app-login",
    templateUrl: "login/login.component.html",
    styleUrls: ["login/login-common.css"]
})
export class LoginComponent {
    constructor(
        private page: Page
    ) {
        page.backgroundColor = new Color('#000');
        googleAnalytics.logView('login');
        if (isIOS) {
            //page.backgroundSpanUnderStatusBar = true;
            //topmost().ios.controller.navigationBar.barStyle = 1;
        }
        else {
            page.actionBarHidden = true;
        }
    }

    public onTermsTap(){
        openUrl('http://api.heat.city/tos.html');
    }
    
    public onPrivacyTap() {
        openUrl('http://api.heat.city/privacy-policy.html');
    }
}
