import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NgModule } from "@angular/core";

import { accountSetupRouting } from "./account-setup.routing";
import { AccountSetupComponent } from "./account-setup.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptRouterModule,
    accountSetupRouting,
    SharedModule
  ],
  exports: [ NativeScriptRouterModule ],
  declarations: [
    AccountSetupComponent,
  ]
})
export class AccountSetupModule { }
