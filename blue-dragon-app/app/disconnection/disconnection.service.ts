import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DisconnectionService {
    private subject: Subject<boolean> = new Subject<boolean>();
    public status: boolean = true;

    constructor() { }

    setConnectionStatus(status: boolean): void {
        this.status = status;
        this.subject.next(status);
    }

    getConnectionStatus(): Observable<boolean> {
        return this.subject.asObservable();
    }
}