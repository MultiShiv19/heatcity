import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { topmost } from "ui/frame";
import { Page } from "ui/page";
import { View } from 'ui/core/view';
import { DisconnectionService } from './disconnection.service';
import { AnimationCurve } from 'ui/enums';

@Component({
    moduleId: module.id,
    selector: 'ns-disconnection',
    templateUrl: 'disconnection.component.html',
    styleUrls: ['disconnection.component.css']
})
export class DisconnectionComponent implements OnInit, OnDestroy {
    public isConnected: boolean = true;
    private subscriptions: Subscription[] = new Array<Subscription>();

    constructor(
        private disconnectionService: DisconnectionService,
        private _ngzone: NgZone,
        private page: Page,
    ) {
        this.watchConnection();
    }

    ngOnInit() {
        /*if (topmost().ios) {
            // get the view controller navigation item 
            const controller = topmost().ios.controller;
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);
        }*/
    }


    public watchConnection() {
        this._ngzone.run(() => {
            this.subscriptions.push(
                this.disconnectionService.getConnectionStatus().subscribe((status) => {
                    console.log("stateeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee -> " + status);
                  
                        this.openNoConnection(status);


                }
                ));
        })
    }
    openNoConnection(status: boolean): void {
        this.isConnected = status;
        let stackLayout = <View>this.page.getViewById("disconnection");
        if (stackLayout) {
            stackLayout.translateY = -30;
            stackLayout.animate({
                translate: { x: 0, y: 0 },
                opacity: 1,
                duration: 1000,
                curve: AnimationCurve.easeOut
            });
            let timeout: number = 5000;
            setTimeout(() => {
                this.closeNoConnection();
            }, timeout);
        }
    }

    closeNoConnection(): void {
        let stackLayout = <View>this.page.getViewById("disconnection");
        if (stackLayout) {
            stackLayout.animate({
                opacity: 0,
                duration: 200
            }).then(() => {
                this._ngzone.run(() => {
                    // this.isConnected = true;
                });
            });
        }
    }


    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

}