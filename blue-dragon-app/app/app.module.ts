import { NgModule } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { SDKNativeModule } from './shared/sdk/index';
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { registerElement } from "nativescript-angular";
import { ShareModule } from "./share/share.module";
import { authProviders, appRoutes } from "./app.routing";
import { AppComponent } from "./app.component";
import { LoginModule } from "./login/login.module";
import { MapModule } from './map/map.module';
import { SharedModule } from './shared/shared.module';
import { NavigateService } from './shared/navigate.service';
import { UserProfileModule } from './user-profile/user-profile.module';
import { CategoriesMap } from './shared/category-marker.service';
import { CategoryResourceService } from './shared/category-resource.service';
// import { AccountSetupModule } from './account-setup/account-setup.module';
import { AppNotificationsModule } from './app-notifications/app-notifications.module';
import { AppNotificationsService } from './app-notifications/app-notifications.service';
import { NotificationsService } from './notifications/notifications.service';
import { EstablishmentProfileModule } from './establishment-profile/establishment-profile.module';
import { EstablishmentModule } from './establishment/establishment.module'; // Added by Sandip
import { DisconnectionComponent } from './disconnection/disconnection.component';
import { DisconnectionService } from './disconnection/disconnection.service';
import { VisitDetectionService } from './map/visit-detection/visit.detection.service';
import { LogoutService } from './shared/logout.service';
import { CheckInsModule } from "./check-ins/check-ins.module";
import { MapRedirectComponent } from './map/map-redirect/map-redirect.component';
registerElement("Gradient", () => require("nativescript-gradient").Gradient);

@NgModule({
    declarations: [
        AppComponent,
        MapRedirectComponent,
    ],
    bootstrap: [AppComponent],
    imports: [
        NativeScriptModule,
        NativeScriptHttpModule,
        NativeScriptFormsModule,
        SDKNativeModule.forRoot(),
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(appRoutes),
        SharedModule,
        ShareModule,
        EstablishmentProfileModule,
        EstablishmentModule, // Added by Sandip
        LoginModule,
        MapModule,
        UserProfileModule,
        AppNotificationsModule,
        CheckInsModule,
    ],
    providers: [
        authProviders,
        CategoriesMap,
        CategoryResourceService,
        AppNotificationsService,
        NotificationsService,
        NavigateService,
        DisconnectionService,
        VisitDetectionService,
        LogoutService
    ]
})
export class AppModule { }
