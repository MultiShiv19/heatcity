import { AuthGuard } from "./auth-guard.service";
import { DisconnectionComponent } from './disconnection/disconnection.component';
import { MapRedirectComponent } from './map/map-redirect/map-redirect.component';
export const authProviders = [
  AuthGuard
];

export const appRoutes = [
  { path: "", component:MapRedirectComponent, pathMatch: "full" ,canActivate: [AuthGuard]},
  //{ path: "/mapre", redirectTo: "/posts", pathMatch: "full" },
  { path: "disconnect", component: DisconnectionComponent }
];