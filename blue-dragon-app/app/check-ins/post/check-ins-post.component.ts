import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { topmost } from "ui/frame";
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import geolocation = require('nativescript-geolocation');
import dialog = require("ui/dialogs");
import { Page } from "ui/page";
import { Accuracy } from "ui/enums";
import { MapView, Marker, Polyline, Position } from 'nativescript-google-maps-sdk';
import { LoadingIndicator } from 'nativescript-loading-indicator';
import { VisitDetectionService } from '../../map/visit-detection/visit.detection.service';
import { EstablishmentApi, VisitApi } from '../../shared/sdk/services';
import { AccountApi } from '../../shared/sdk/services/custom/Account';
import { Visit, Comment } from '../../shared/sdk/models';
import { Establishment, LoopBackFilter } from '../../shared/sdk/models';
import { KeyboardObserver } from '../../shared/keyboard-observer';
import {CheckInsService} from '../check-ins.service'

declare var UIImage: any;
declare var UIBarMetrics: any;

@Component({
    moduleId: module.id,
    selector: 'check-ins-post',
    templateUrl: 'check-ins-post.component.html',
    styleUrls: ['check-ins-post.component.css'],
    providers: [KeyboardObserver]
})

export class CheckInsPostComponent implements OnInit, OnDestroy {
    private _TAG: string = "CheckInsPostComponent";

    /** Establishment object **/
    private establishment: Establishment = new Establishment();

    /** List of active subscriptions which we can unsubcrive from **/
    private _subscriptions: Array<Subscription> = [];

    /** @type {Subscription} **/
    private locationSubscription: Subscription;

    /** @type {Position} **/
    private positions: { user?: geolocation.Location, current?: Position, lastUpdated?: Position, lastPosition?: Position };

    /** @type {Subject<Position>} **/
    private locationSubject: Subject<Position> = new Subject<Position>();

    private _around: geolocation.Location;

    private _watchId: any;

    public postTypeText: string = "Post";

    public checkInText: string = "";

    public privacy: string = "Mutual";

    private visibility: number = 1;

    private _visitId: any;

    // Privacy Options
    private _checkInOptionsRegular: string[] = ["Public", "Followers who you also follow", "Private"]
    private _checkInOptionsLive: string[] = ["Public", "Mutual Followers", "Private"]

    private _isLive: boolean = false;
    private iqKeyboard: IQKeyboardManager;

    /**
     * Loading indicator
     */
    private _loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                android: {
                    indeterminate: true,
                    cancelable: false
                },
                ios: {
                    square: false,
                    margin: 10,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'
                }
            }
        };

    public keyboardHeight: number = 0;

    constructor(
        private page: Page,
        private _pageRoute: PageRoute,
        private _establishmentApi: EstablishmentApi,
        private _visitDetectionService: VisitDetectionService,
        private _accountApi: AccountApi,
        private _visitApi: VisitApi,
        private _activatedRoute: ActivatedRoute,
        private _routerExtensions: RouterExtensions,
        private _keyboardObserver: KeyboardObserver,
        private _ngZone: NgZone,
        private checkInsService:CheckInsService
    ) {
        this.iqKeyboard = IQKeyboardManager.sharedManager();
        this.iqKeyboard.enable = false;
        this._activatedRoute.queryParams.subscribe(params => {
            this._visitId = params["visit_id"];
            let id = params["establishment_id"];
            this.getEstablishment(id);
            console.error("visit_id -> " + this._visitId + " establishmentId -> " + id);
        });
        //this.getUrlParam();
    }

    public ngOnInit(): void {
        if (topmost().ios) {
            // get the view controller navigation item
            const controller = topmost().ios.controller;
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);
            this._subscriptions.push(this._keyboardObserver.heightChange$().subscribe((height) => {
                this._ngZone.run(() => {
                    if (height < 80 || height > this.keyboardHeight)
                        this.keyboardHeight = height;
                })
            }));
        }
    }

    public onTextViewLoaded(args) {
        if (topmost().ios) {
            args.object.ios.becomeFirstResponder();
            args.object.ios.inputAccessoryView = UIView.alloc().init();
        }
    }

    public onCancelTap() {
        this._routerExtensions.back();
    }
    /**
     * @method getUrlParam
     * @return {void}
     */
    private getUrlParam(): void {
        this._pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                let id = params['id'];
                this.getEstablishment(id);
            })
    }

    /**
     * @method getEstablishment
     * @param {string} establishmentId
     * @return {void}
     */
    private getEstablishment(establishmentId: string): void {
        let filter: LoopBackFilter = {
            include: ['location']
        }
        // Show loading indicator
        //this._loader.instance.show();
        let that = this;
        this._subscriptions.push(
            this._establishmentApi.findById(establishmentId, filter)
                .subscribe((establishment: Establishment) => {
                    that.establishment = establishment;
                    that.watch();
                    that._visitDetectionService.startVisitDetector();

                },
                that.error
                )
        );
    }

    watch(): void {
        // This will mainly happen on navigation, because the location subscription already exists
        // But the map was wiped, so at this stage watching is really triggering a fist time
        // So the map can be populated, this only happens the second time watch is executed.
        if (this.locationSubscription) {
            this.locationSubscription.unsubscribe();
            console.log('AVOIDING LOCATION SUBSCRIPTION (POTENTIAL DUPLICATED EVENT)');
        }
        //this.watchingEvents = true;
        this.locationSubscription = this.locationSubject.subscribe((position: geolocation.Location) => {
            console.log(this._TAG, "latitude: " + position.latitude + " longitude: " + position.longitude);
        });
        this.watchLocation();
    }

    watchLocation(): void {

        // Default options
        let options: any = {
            desiredAccuracy: Accuracy.high,
            updateDistance: 5,
            minimumUpdateTime: 5000,
            iosAllowsBackgroundLocationUpdates: true,
            iosPausesLocationUpdatesAutomatically: false
        };

        // If there is a user position, lets use that to change update distance and min update time
        if (this._around && this._around.speed && this._around.horizontalAccuracy) {
            options.updateDistance = this._around.horizontalAccuracy < 20 ? 5 :
                this._around.horizontalAccuracy < 50 ? 20 :
                    this._around.horizontalAccuracy > 50 ? 40 : 50;
            options.minimumUpdateTime = this._around.speed === 0 ? 10000 :
                this._around.speed < 5 ? 6000 :
                    this._around.speed < 10 ? 3000 :
                        this._around.speed > 10 ? 500 : 5000;
        }
        if (this._watchId) {
            geolocation.clearWatch(this._watchId);
        }
        console.log('\x1b[33mSETTING UP WATCH LOCATION\x1b[0m');
        this._watchId = geolocation.watchLocation(
            (position: geolocation.Location) => {
                console.log('\x1b[33mVISIT DETECTOR - GOT LOCATION FROM DEVICE\x1b[0m -> ' + position.latitude);
                if (!this.establishment || !this.establishment.geoLocation) return;
                let establishmentPosition = new Position();
                establishmentPosition.latitude = this.establishment.geoLocation.coordinates[1];
                establishmentPosition.longitude = this.establishment.geoLocation.coordinates[0];

                let distance = this.calculateDistance(position, establishmentPosition);
                console.log(this._TAG, "Distance is -> " + distance);

                if (distance <= 100) this.postTypeText = "LIVE Post";
                this._isLive = distance <= 100;

                this._around = position;
                if (
                    (this._around &&
                        (
                            (this._around.horizontalAccuracy - position.horizontalAccuracy) > 10 ||
                            (this._around.horizontalAccuracy - position.horizontalAccuracy) < -10
                        )
                        ||
                        (
                            (this._around.speed - position.speed) > 3 ||
                            (this._around.speed - position.speed) < -3
                        )
                    )
                ) {
                    //this.visitDetectionService.newUserPosition(position, true);
                    this.watchLocation(); // Run again to use new settings
                } else {
                    // console.log('\x1b[33mUser is not Moving\x1b[0m');
                    //this.visitDetectionService.newUserPosition(position, false);
                }
            },
            (err: Error) => {
                console.log('VISIT DETECTOR - GEOLOCATION ERROR:', err)
            },
            options
        )
    }

    public showPrivacyDialog() {
        dialog.action("Change the audience of this Check-In",
            "Cancel",
            this._isLive ? this._checkInOptionsLive : this._checkInOptionsRegular)
            .then((result => {
                if (result == 'Cancel') return;
                this.privacy = result;
                console.log("Dialog result: " + result);
                switch (result) {
                    case "Public":
                        this.visibility = 2;
                        break;
                    case "Private":
                        this.visibility = 0;
                        break;
                    default:
                        this.visibility = 1;
                }
            }));
    }

    public doPost(): void {
        let that = this;
        // Check if visit id is present
        if (this._visitId) {
            this._loader.instance.show();
            // Visit Id is there, that means user wants to confirm the suggested check-in
            // So first we will need to fetch all the data of this visit
            this._visitApi.findById(this._visitId)
                .subscribe((visit: Visit) => {
                    console.dir(visit);
                    // mark validity as 100 for making this visit as approved
                    visit.validity = 100;
                    visit.visible = this.visibility
                    visit.locationId = this.establishment.locationId;
                    visit.geoLocation = this.establishment.geoLocation;
                    this._accountApi
                        .updateByIdVisits(
                        this._accountApi.getCurrentId(),
                        this._visitId,
                        visit).subscribe((visit: Visit) => {
                            console.dir(visit)
                            // now if we have some input from user,
                            // we need to add that as a comment!
                            if (this.checkInText.length > 0) this.addComment(this.checkInText, visit.id);
                        }, that.error)
                }, that.error)
				this.handleSuccess();
        } else {
			console.log("starting loader");
            this._loader.instance.show();
            // We need to create a visit
            let visit = new Visit();
            visit.accountId = this._accountApi.getCurrentId();
            visit.locationId = this.establishment.locationId;
            visit.geoLocation = this.establishment.geoLocation;
            if (this._isLive) visit.entry = new Date();
            visit.createdAt = new Date();
            visit.validity = 100;
            visit.visible = this.visibility;

            //this._accountApi.createVisits(this._accountApi.getCurrentId(), visit)
            this._visitApi.create(visit)
                .subscribe((visit) => {
                    if (visit && visit.id) {
                        // Successfully created Visit, now if we have some input from user,
                        // we need to add that as a comment!
                        if (this.checkInText.length > 0) this.addComment(this.checkInText, visit.id);
                        else this.handleSuccess();
                    }
                }, that.error);
        }

    }

    private addComment(commentTxt: string, visitId: any): void {
        let comment = new Comment();
        comment.comment = commentTxt;
        comment.accountId = this._accountApi.getCurrentId();
        comment.visitId = visitId;
        comment.createdAt = new Date();
        comment.updatedAt = new Date();
        let that = this;
        this._visitApi
            .createComments(visitId, comment)
            .subscribe((data) => {
                this.handleSuccess();
            }, (error) => {
                that._loader.instance.hide();
            });
    }

    public ngOnDestroy(): void {
        if (this._watchId) {
            geolocation.clearWatch(this._watchId);
        }
        this._subscriptions.forEach((subscription) => {
            subscription.unsubscribe();
        })
    }

    /**
     * @calculateDistance
     * @param {Position} a Location A
     * @param {Position} b Location B
     * @return {number} Delta distance between location A and B
     **/
    calculateDistance(a: Position, b: Position): number {
        let aLocation = new geolocation.Location();
        let bLocation = new geolocation.Location();
        aLocation.latitude = a.latitude || 0;
        aLocation.longitude = a.longitude || 0;
        bLocation.latitude = b.latitude || 0;
        bLocation.longitude = b.longitude || 0;
        console.log(this._TAG, "aLocation lat -> " + aLocation.latitude + " lng -> " + aLocation.longitude);
        console.log(this._TAG, "bLocation lat -> " + bLocation.latitude + " lng -> " + bLocation.longitude);
        return geolocation.distance(bLocation, aLocation);
    }

    handleSuccess() {
        this._loader.instance.hide();
        this._routerExtensions.back();
        this.checkInsService.checkInPostcall();
    }

    error(error: Error) {
        // Hide the loading indicator incase of error
        this._loader.instance.hide();
    }
}
