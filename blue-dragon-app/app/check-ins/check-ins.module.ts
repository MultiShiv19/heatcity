import { NgModule } from '@angular/core';
import { NativeScriptUIListViewModule } from "nativescript-pro-ui/listview/angular";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";


import { ShareModule } from "../share/share.module";
import { checkInRouting, checkInNavigatableComponents } from './check-ins.routing';
import { SharedModule } from '../shared/shared.module';
import { StorageNative } from '../shared/sdk/storage/storage.native';
import { FormatDistancePipe } from '../shared/format.pipe';
import { DialogComponent } from './dialog/dialog.component';
import { CheckInsComponent } from './check-ins.component';
import { CheckInsService } from './check-ins.service';
import { AppNotificationsModule } from "../app-notifications/app-notifications.module";
import { NativeScriptUISideDrawerModule } from "nativescript-pro-ui/sidedrawer/angular";

@NgModule({
    imports: [
        NativeScriptUIListViewModule,
        NativeScriptModule,
        NativeScriptFormsModule,
        AppNotificationsModule,
        ShareModule,
        SharedModule,
        checkInRouting,
    ],
    exports: [

    ],
    declarations: [
        checkInNavigatableComponents,
        DialogComponent,
    ],
    entryComponents: [
        DialogComponent
    ],
    providers: [StorageNative, CheckInsService]
})
export class CheckInsModule { }
