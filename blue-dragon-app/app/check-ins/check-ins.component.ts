import { Component, OnInit, AfterViewInit, OnDestroy, NgZone } from '@angular/core';
import { Page } from 'ui/page';
import { Button } from "ui/button";
import { ActivatedRoute, Router } from "@angular/router";
import { registerElement } from 'nativescript-angular';
import { BottomBar, BottomBarItem, TITLE_STATE, SelectedIndexChangedEventData } from 'nativescript-bottombar';
import { RouterExtensions } from "nativescript-angular/router";
import { LoopBackFilter, Account, Establishment, Location } from '../shared/sdk/models';
import { AccountApi } from '../shared/sdk/services/custom/Account';
import { VisitApi } from '../shared/sdk/services/custom/Visit';
import { FriendApi } from '../shared/sdk/services/custom/Friend';
import { topmost } from "ui/frame";
import { AnimationDefinition } from "ui/animation";
import * as appSettings from "application-settings";
import * as frameModule from "ui/frame";
import { isIOS } from "platform";
import { Color } from 'tns-core-modules/color/color';
import { ListView } from "ui/list-view";
import { LoadingIndicator } from "nativescript-loading-indicator";
import { Subscription } from 'rxjs/Subscription';
import { LogoutService } from '../shared/logout.service';
import { CheckInsService } from "./check-ins.service"

declare var UIImage: any;
declare var UIColor: any;
declare var UIBarMetrics: any;
declare var UITableViewCellSelectionStyle: any;

@Component({
    moduleId: module.id,
    selector: 'check-ins',
    templateUrl: 'check-ins.component.html',
    styleUrls: ['check-ins.component.css']
})

export class CheckInsComponent implements OnInit, AfterViewInit, OnDestroy {

    private subscriptions: Subscription[] = new Array<Subscription>();

    /** @type {Account} **/
    public account: Account;
    public dataItems: Array<any>;
    private counter: number;
    public follower;
    public profile;
    public myVisits = [];
    public followingVisits = [];
    public currentLocation;
    public userid = "";
    public goBackSucess: Array<boolean> = [false, false];
    private _followersPostList: ListView;
    private _myPostList: ListView;
    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                progress: 0,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'// see iOS specific options below
                }
            }
        };

    constructor(
        private routerExtensions: RouterExtensions,
        private page: Page,
        private _accountApi: AccountApi,
        private _visitApi: VisitApi,
        private _friendApi: FriendApi,
        private router: Router,
        private route: ActivatedRoute,
        private logoutService: LogoutService,
        private _ngZone: NgZone,
        private checkInsService: CheckInsService,
    ) {

    }
    public onLoaded() {
        console.log("onloaded+++++++++++++++++++++++++++++++++++++call");
        this.loader.instance.show(this.loader.options);
        this.getMyVisits();
        this.getFriendVisit();



    }

    public ngOnInit(): void {
        // this.loader.instance.show(this.loader.options);
        try {
            /**  This 3 line are Hide Back Button From ActionBar */
            if (topmost().ios) {
                // get the view controller navigation item
                const controller = frameModule.topmost().ios.controller;
                const navigationItem = controller.visibleViewController.navigationItem;
                // hide back button
                navigationItem.setHidesBackButtonAnimated(true, false);

            }
            this.follower = true;
            this.profile = false;
            this.userid = this._accountApi.getCurrentId();
            this.page.on("loaded", () => this.onLoaded());
            this.checkInsService.checkInPostrecevie().subscribe(() => {
                // console.log("checkins service res~~~~~~~~~~~~~~~~~~~~~")
                this.onLoaded();
            })
            // this.getMyVisits();
            const self = this;
            this.route.params
                .subscribe((params) => {
                    if (params['data']) {
                        const data = JSON.parse(params['data']);
                        if (data.type === 'self') {
                            this.OnTapProfile(self);
                        } else if (data.type === 'following') {
                            this.onTapFollowerTap(self);
                        }
                    }
                    // this.onTapFollowerTap(self);

                });

        } catch (error) {
            console.log("getFollowingList" + error);

        }


    }

    ngAfterViewInit(): void {
        this._followersPostList = <ListView>this.page.getViewById('lvFollowersPost');
        // this._followersPostList = this.page.getViewById('lvFollowersPost');

    }

    public listViewItemLoading(args): void {
        if (isIOS) {

            args.ios.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }

    public onClickUser(args) {
        this.routerExtensions.navigate(['/user-profile/' + this.userid], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickPosts(args) {
        this.routerExtensions.navigate(['/posts'], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickDiscover(args) {
        this.routerExtensions.navigate(['/map'], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickNotification(args) {
        this.routerExtensions.navigate(['/notification'], {
            clearHistory: true,
            animated: false
        });
    }

    public OnTapProfile($event) {
        const followingButton = <Button>this.page.getViewById("following");
        const profileButton = <Button>this.page.getViewById("profile");
        profileButton.backgroundColor = "#ff0086";
        followingButton.backgroundColor = "#000000";
        this.profile = true;
        this.follower = false;
        this._myPostList = <ListView>this.page.getViewById('lvMyPost');
    }

    public onTapFollowerTap($event) {
        const followingButton = <Button>this.page.getViewById("following");
        const profileButton = <Button>this.page.getViewById("profile");
        followingButton.backgroundColor = "#ff0086";
        profileButton.backgroundColor = "#000000";
        this.follower = true;
        this.profile = false;
        this._followersPostList = <ListView>this.page.getViewById('lvFollowersPost');
        // this._followersPostList = this.page.getViewById('lvFollowersPost');

    }

    public getMyVisits() {
        try {
            // console.log("myvisitvisit~~~~~~~~~~~~~~~~~~~~~");
            let filter: LoopBackFilter = {
                include: ["account", { "location": "establishment" },
                    { "relation": "comments", "scope": { "fields": ["accountId", "comment"] } },
                    { "relation": "likes", "scope": { "fields": ["accountId", "id"] } }],
                order: "createdAt DESC"
            };
           // console.log("myvisit filter=======", JSON.stringify(filter));
            this.subscriptions.push(
                this._accountApi.getVisits(this.userid, filter).subscribe((data) => {
                    this._ngZone.run(() => {
                        this.goBackSucess[0] = true;
                       // console.log("myvisitvisit res~~~~~~~~~~~~~~~~~~~~~");
                        if (this.goBackSucess.every((data) => data === true)) {
                            this.loader.instance.hide();
                        }
                        this.myVisits = data;
                    })
                }, (error) => {
                    // this.loader.instance.hide();
                    this.goBackSucess[0] = true;
                    this.errorHandler(error);
                    console.error("Error While Getting Visit -> " + error.message);
                }));
        } catch (error) {
            // this.loader.instance.hide();
            this.goBackSucess[0] = true;
            console.log("MyVisitList Error" + error);
        }
    }

    public getFriendVisit() {
        let filter2: LoopBackFilter = {
            fields: ["friendId"],
        };
        // console.log("friendvisit~~~~~~~~~~~~~~~~~~~~~");

        this.subscriptions.push(
            this._visitApi.friendsPosts(this.userid).subscribe((data) => {
                this._ngZone.run(() => {
                    this.goBackSucess[1] = true;
                    if (this.goBackSucess.every((data) => data === true)) {
                        this.loader.instance.hide();
                        // console.log("friendvisit res~~~~~~~~~~~~~~~~~~~~~");
                    }
                    this.followingVisits = data;
                });
            }, (error) => {
                console.error("Error While Getting FriendList -> " + error.message);
                this.goBackSucess[1] = true;
                // this.loader.instance.hide();
            }));
    }

    public actionVisit(data) {
        try {
            if (data.action === "delete") {
                this.subscriptions.push(
                    this._visitApi.deleteById(data.data).subscribe((result) => {
                        this.getMyVisits();
                    }, (error) => {
                        this.errorHandler(error);
                        console.error("Error While Deleting Friends -> " + error.message);
                        this.loader.instance.hide();
                    })
                );
            } else if (data.action === "edit") {

            } else if (data.action === 'like_unlike') {
                //if (this._followersPostList) this._followersPostList.refresh();
                //if (this._myPostList) this._myPostList.refresh();
            }
        } catch (error) {
            console.log("Refresh Error" + error);
        }
    }
    public ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    public errorHandler(error: any): void {
        console.log('User profile Error', JSON.stringify(error));
        if (error && error.statusCode === 401) {
            this.logoutService.logout();
        }
    }
}
