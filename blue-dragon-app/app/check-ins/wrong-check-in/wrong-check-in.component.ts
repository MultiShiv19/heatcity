import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

import { Marker } from 'nativescript-google-maps-sdk';
import { EstablishmentApi, VisitApi } from '../../shared/sdk/services';
import { AccountApi } from '../../shared/sdk/services/custom';
import { topmost } from "ui/frame";
import { isIOS } from "platform";
import { CriteriaInterface } from '../../map/search/search.component'; import {
    LoopBackFilter, Area, Establishment,
    EstablishmentPhoto, FireLoopRef, Location,
    FavoriteList, Visit, Notification
} from '../../shared/sdk/models';

declare var UITableViewCellSelectionStyle: any;

@Component({
    moduleId: module.id,
    selector: 'wrong-check-in',
    templateUrl: 'wrong-check-in.component.html',
    styleUrls: ['wrong-check-in.component.css']
})

export class WrongCheckInComponent implements OnInit {

    public establishmentId: string = ""
    public establishmentName: string = "";
    public establishmentAddress: string = "";
    private visitId: any;
    public markers: Marker[] = new Array<Marker>();
    public establishmentsNearBy: Establishment[] = new Array<Establishment>();

    private criteria: CriteriaInterface;
    /** @type {enabled: boolean} */
    private search: { enabled: boolean } = { enabled: false };
    private searchOptions: { invalidArea: boolean, emptyArea: boolean, emptySearch: boolean } = {
        invalidArea: false,
        emptyArea: false,
        emptySearch: false
    }

    constructor(private _activatedRoute: ActivatedRoute,
        private _visitApi: VisitApi,
        private _accountApi: AccountApi,
        private _routerExtensions: RouterExtensions,
        private _establishmentApi: EstablishmentApi) {
        this._activatedRoute.queryParams.subscribe(params => {
            this.visitId = params["visit_id"];
            this.establishmentId = params["establishment_id"];
            this.establishmentName = params["establishment_name"];
            this.establishmentAddress = params["establishment_address"];

            this.getVisitDetails();
        });

        /*let marker: Marker = new Marker();
        let marker1: Marker = new Marker();

        let establishment: any = {
            name: "Viva La Tarte",
            address: "1160 Howard St, San Fransisco, United States"
        }
        marker.userData = {
            establishment
        }

        marker1.userData = {
            establishment:
            {
                name: "Blue Bottle Coffee",
                address: "1355 Marker St, San Fransisco, United States"
            }

        }

        this.markers.push(marker);
        this.markers.push(marker1);*/
    }

    ngOnInit() {
        if (topmost().ios) {
            // get the view controller navigation item
            const controller = topmost().ios.controller;
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);
        }
    }
    public listViewItemLoading(args): void {
        if (isIOS) {

            args.ios.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }

    public goToBack(): void {
        this._routerExtensions.back()
    }

    public onDeleteCheckInTap(): void {
        if (!this.visitId) return;
        this._visitApi.deleteById(this.visitId)
            .subscribe((result) => {
                console.dir(result);
                this._routerExtensions.back();
            });
    }

    getVisitDetails() {
        if (!this.visitId) return;
        this._visitApi.findById(this.visitId, {
            include: {
                location: {
                    relation: 'establishment',
                    scope: {
                        fields: [
                            'id',
                            'name',
                            'description',
                            'geoLocation',
                            'address',
                            'categories',
                            'locationId'
                        ],
                        include: 'photos'
                    }
                }
            }
        }).subscribe(
            (visit: Visit) => {
                //alert("visit found -> " + visit.id);
                this.getNearEstablishments(visit);
            },
            error => this.handleError(error)
            )
    }

    getNearEstablishments(visit: Visit) {
        //alert(JSON.stringify(visit));
        //let abc = true;
        //if(abc) return;
        try {
            let filter: LoopBackFilter = {
                where: {
                    geoLocation: {
                        nearSphere: {
                            $geometry: {
                                type: "Point",
                                coordinates: visit.location.geoLocation.coordinates
                            },
                            $maxDistance: 100,
                            $minDistance: 0
                        }
                    },
                    id: {
                        nlike: visit.location.establishment.id
                    },
                },
                include: { relation: "photos" }
            }
            this._establishmentApi.find(filter).subscribe(
                (establishments: Establishment[]) => {
                    this.establishmentsNearBy = establishments;
                },
                error => this.handleError(error)
            )
        } catch (error) {
            alert(error.message);
        }

    }

    public onItemTap(item): void {
        this._routerExtensions.navigate([
            '/post'
        ], {
                queryParams: {
                    "establishment_id": item.id,
                    "visit_id": this.visitId
                },
                transition: {
                    name: 'slideLeft',
                    duration: 500,
                    curve: 'linear'
                }
            });
    }

    handleError(error: any) {
        alert(error.message);
        console.log(error.message);
    }
    /**
     * @method watchLocationEstablishments
     * @return {void}
     */
    /*watchLocationEstablishments(criteria: CriteriaInterface = null): void {
        if (criteria && (criteria.text !== '' || criteria.area !== '' || criteria.category !== '')) {
            this.criteria = criteria;
            if (this.search.enabled) {
                //this.loader.instance.show(this.loader.options);
            }
            if (!criteria.area || criteria.area == '') {
                this.searchOptions.invalidArea = false;
            }
        }

        let filter: LoopBackFilter = {
            where: {},
            limit: 25
        };

        //this.loadingEstablishments = true;
        //if (this.loader.loading) {
            //this.loader.instance.hide();
        //}
        //this.loader.loading = true;

        // There is another call to the server within selecTCurrentLocation
        //this.loader.instance.show(this.loader.options); // Should be only loader, since here it loads from server.
        // Select current position, if user wrote a different area, will move to that area and set as current.
        this.selectCurrentLocation((current: Position, searched: boolean, area?: Area) => {
            console.log('STARTING REQUEST123', JSON.stringify(current));
            console.time('ESTABLISHMENT TIMER');

            // Add current position to query
            // if(this.search.enabled && this.criteria.area !== '')
            if (searched && area && area.id) {
                filter.where.geoLocation = {
                    geoWithin: {
                        $polygon: [
                            [area.southWest.lng, area.southWest.lat],
                            [area.southWest.lng, area.northEast.lat],
                            [area.northEast.lng, area.northEast.lat],
                            [area.northEast.lng, area.southWest.lat]
                        ]
                    }
                };
            } else {
                if (this.search.enabled) {
                    let xs = this.userMarker.position;
                    let center = [
                        this.userMarker.position.longitude,
                        this.userMarker.position.latitude
                    ];
                    // distance to radians: divide the distance by the radius of
                    // the sphere (e.g. the Earth) in the same units as the distance
                    // measurement.
                    let radius = criteria && criteria.area == '' ? 5 : 20;
                    let searchArea = radius / 3963.2;
                    filter.where.geoLocation = {
                        geoWithin: {
                            $centerSphere: [center, searchArea]
                        }
                    };
                } else {
                    let x = this.getScreenCoordinates()
                    filter.where.geoLocation = { geoWithin: { $polygon: x } };

                }
            }
            // Order
            filter.order = 'rating DESC'

            filter.fields = [
                'id',
                'name',
                'description',
                'geoLocation',
                'address',
                'categories',
                'locationId',
                'hcCategory'
            ];

            filter.include = { relation: 'photos' };

            if ((this.criteria && this.criteria.text !== '' && this.criteria.text != null) && (this.criteria && this.criteria.category !== '' && this.criteria.category != null)) {
                filter.where.or = [
                    { name: { "like": '.*' + this.criteria.text + '.*', "options": "i" } },
                    { hcCategory: this.criteria.category }
                ]
            } else if (this.criteria && this.criteria.text !== '' && this.criteria.text != null) {
                filter.where.or = [
                    { name: { "like": '.*' + this.criteria.text + '.*', "options": "i" } },
                    { hcCategory: this.criteria.category }
                ]
                // filter.where.name = { "like": '.*' + this.criteria.text + '.*', "options": "i" } ;
            } else if (this.criteria && this.criteria.category !== '' && this.criteria.category != null) {
                //filter.where.categories = { '"' + this.criteria.category + '"' };
                filter.where.hcCategory = this.criteria.category;
                // filter.where.name = { "like": '.*' + this.criteria.text + '.*', "options": "i" } ;
            }

            //filter = {"where":{"geoLocation":{"geoWithin":{"$polygon":[[-74.0088629,40.7373582],[-74.0088629,40.7570384],[-73.9877916,40.7570384],[-73.9877916,40.7373582]]}},"hcCategory":"Dance"},"limit":15,"order":"rating DESC","fields":["id","name","description","geoLocation","address","categories","locationId","hcCategory"],"include":{"relation":"photos"}};
            console.log('FINAL FILTER: ', JSON.stringify(filter));
            // Listen for establishments around the selected position

            this.subscription.establishment = this.establishmentApi.find(filter).subscribe(
                (establishments: Establishment[]) => {
                    this.zone.run(() => {
                        console.timeEnd('ESTABLISHMENT TIMER');
                        this.loader.instance.hide();
                    })
                    //TOGO GOOGLE ANALYTICS
                    //googleAnalytics.stopTimer('establishments-timer')
                    // IF not enabled: generates a bug when navigating different areas, only loads the area center.
                    // IF enabled: won't show the latest nearby area, fix: implement last criteria in search component
                    this.searchOptions.emptySearch = establishments.length === 0;
                    if (criteria && criteria.area) {
                        if (searched) {
                            this.criteria.lastArea = "" + criteria.area;
                        }
                        this.searchOptions.emptyArea = (!area.updates || area.updates === 0);
                        console.log('EMPTY AREA: ', this.searchOptions.emptyArea);
                        if (this.criteria) {
                            //                            this.criteria.area = null;
                        }

                    }
                    let visitsEstablishments: VisitEstablishment[] = establishments.map((establishment: Establishment, index: number) => ({ establishment, index }));
                    if (this.settings.WIPE_MARKERS) {
                        this.removeCurrentEstablishmentMarkers();
                        console.log("====================================================================CALLED FROM HERE1");
                        this.loadEstablishmentMarkers(visitsEstablishments);
                    } else {
                        console.log("====================================================================CALLED FROM HERE2");
                        this.loadEstablishmentMarkers(
                            this.removeCurrentEstablishmentMarkers(visitsEstablishments)
                        );

                    }
                    // console.log('1-- 2', establishments.length, this.isMapNew ? 'yes' : 'nel mijo');

                    if (this.isMapNew && establishments.length == 0) {
                        // this.setCurrentPosition(this.positions.user);
                        // this.loadCurrentPosition();
                        timer.setTimeout(() => {
                            this.watchLocationEstablishments();
                            this.isMapNew = false;
                        }, 200)
                    } else {
                        this.isMapNew = false;
                    }
                    // This fixes the search expand button, we need to move the camera
                    // But avoid doing a new query.
                    console.log('2--Searched', searched ? 'si' : 'no');
                    if (searched) {
                        this.blockCameraChange = true;
                        this.positions.lastUpdated = current;
                        this.setCurrentPosition(current);
                        this.loadCurrentPosition();
                    }
                },
                error => this.errorHandler(error)
            );
        });
    }*/

    /**
     * @method selectCurrentLocation
     * Change from area only if user added a search criteria
     */
    /*selectCurrentLocation(next: Function): void {
        if (this.fromBack || this.fromSearch) {
            if (this.criteria && this.criteria.area == null) {
                this.criteria.area = '';
            }
        }
        console.log('SELECTING CURRENT LOCATION', JSON.stringify(this.criteria));
        if (!this.criteria || (this.criteria && this.criteria.area === '')) {
            console.log('PASSING CURRENT POSITION FROM MEMORY');
            this.fromSearch = false;
            next(this.positions.current, false);
        } else if (this.criteria && this.criteria.area != null && this.criteria.area !== '') {
            console.log('PASSING AREA POSITION FROM API');
            this.area.find({
                where: {
                    name: this.criteria.area
                }
            }).subscribe((areas: Area[]) => {
                this.searchOptions.invalidArea = false;
                if (Array.isArray(areas) && areas.length > 0) {
                    this.states.FARAWAY_SEARCH_LOAD = true;
                    let area: Area = areas.pop();
                    this.centered = false; // Needed to tell the map we are in a new area.
                    // Needed because this time the update was not due camera change, so only there
                    // we are setting lastUpdated, so here is also needed since no camera was updated -yet-. (RACE CONDITION)
                    // STILL NOT WORKING DUDE. (THOUGH IS A LOW LEVEL ISSUE, showing loading when selecting a searched area item)
                    this.positions.lastUpdated = Position.positionFromLatLng(area.center.lat, area.center.lng);
                    if (this.positionBeforeNavigate && this.positionBeforeNavigate.lat) {
                        this.positionBeforeNavigate = {};
                        this.storage.set('positionBeforeNavigate', this.positionBeforeNavigate);
                    } else {
                        this.setCurrentPosition(this.positions.lastUpdated);
                    }
                    //this.loadCurrentPosition();
                    // this.isEmptyArea(area.id); buggy approach
                    next(this.positions.current, true, area);
                } else {
                    next(this.positions.current, false);
                }
            }, (err: Error) => {

                this.searchOptions.invalidArea = true;
//                this.loader.instance.hide();
                this.searchOptions.emptyArea = false;
                next(this.positions.current, false);
            })
        }
    }*/
}
