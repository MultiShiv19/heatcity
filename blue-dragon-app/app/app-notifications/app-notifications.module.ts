import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NgModule } from "@angular/core";
import { AppNotificationsComponent } from "./app-notifications.component";
import { AppNotificationsService } from "./app-notifications.service";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [
    NativeScriptModule,
    SharedModule
  ],
  exports: [AppNotificationsComponent],
  declarations: [AppNotificationsComponent],
  providers: []
})
export class AppNotificationsModule { }
