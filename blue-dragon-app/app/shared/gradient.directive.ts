import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  OnDestroy,
  AfterViewInit,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import * as application from 'application';
import { Color } from 'color';
import { Page } from 'ui/page';
import { View } from 'ui/core/view';

declare var android: any;
declare var NSMutableArray: any;
declare var CAGradientLayer: any;
declare var interop: any;

@Directive({
  selector: '[gradient]'
})
export class GradientDirective implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input('gradient') start: string | Array<number>;

  @Input('endColor') end: string | Array<number>;

  @Input('borderRadius') border: number;

  @Input('disableGradient') disable: boolean;

  @Input('allowAnimation') allowAnimation: boolean;

  private loadedEventFn: () => void;

  constructor(private el: ElementRef, private page: Page) {
  }

  ngOnInit() {
    this.loadedEventFn = () => {
      this.setGradient();
    };

    this.page.on(Page.loadedEvent, this.loadedEventFn);

    try {
      const oldOnSizeChangedFn = this.el.nativeElement._onSizeChanged;
      if (this.el.nativeElement.ios) {
        this.el.nativeElement._onSizeChanged = () => {
          oldOnSizeChangedFn.call(this.el.nativeElement);
          this.setGradient();
        };
      }
    } catch (exp) {
      console.log(exp);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    for(let prop in changes) {
/*
      if (
        prop == 'start'
        && typeof changes[prop].previousValue[0] !== 'undefined'
        && this.allowAnimation
      ) {
        this.setGradient();
      }
*/
    }
  }

  ngAfterViewInit() {
    this.setGradient();
  }

  ngOnDestroy() {
    this.page.off(Page.loadedEvent, this.loadedEventFn);
  }

  getSublayersCount(sublayers: any): number {
    try {
      let count = sublayers.count
      return count;
    } catch (error) {
      return 0;
    }
  }

  private setGradient() {

    var startColor: Color;
    var endColor: Color;

    if (typeof this.start === "object") {
      startColor = new Color(this.start[0], this.start[1], this.start[2], this.start[3] );
    }else {
      startColor = new Color(this.start);
    }
    if (typeof this.end === "object") {
      endColor = new Color(this.end[0], this.end[1], this.end[2], this.end[3] );
    }else {
      endColor = new Color(this.end);
    }
    if (!this.disable) {
      const view = this.el.nativeElement;
      if (view.android) {
        let backgroundDrawable = view.android.getBackground();
        const orientation = android.graphics.drawable.GradientDrawable.Orientation.TOP_BOTTOM;
        const RECTANGLE: number = 0;

        if (
          !(backgroundDrawable instanceof android.graphics.drawable.GradientDrawable)
          || this.allowAnimation
        ) {
          backgroundDrawable = new android.graphics.drawable.GradientDrawable();
          backgroundDrawable.setColors([startColor.android, endColor.android]);
          backgroundDrawable.setGradientType(RECTANGLE);
          backgroundDrawable.setOrientation(orientation);
          if(this.border)
            backgroundDrawable.setCornerRadius(this.border);
          this.el.nativeElement.android.setBackgroundDrawable(backgroundDrawable);
        }
      } else if (view.ios && view._nativeView && view._nativeView.bounds) {
        const nativeView = view._nativeView;
        const colorsArray = NSMutableArray.alloc().initWithCapacity(2);
        colorsArray.addObject(interop.types.id(startColor.ios.CGColor));
        colorsArray.addObject(interop.types.id(endColor.ios.CGColor));

        const gradientLayer = CAGradientLayer.layer();
        gradientLayer.colors = colorsArray;
        gradientLayer.frame = nativeView.bounds;
        gradientLayer.name = "gradient";
        if (nativeView.layer && nativeView.layer.sublayers) {
          for(let i = 0; i < this.getSublayersCount(nativeView.layer.sublayers); i++) {
            if(nativeView.layer.sublayers.objectAtIndex(i) && nativeView.layer.sublayers.objectAtIndex(i).name == "gradient") {
              nativeView.layer.sublayers.objectAtIndex(i).removeFromSuperlayer();
            }
          }
        }
        nativeView.layer.insertSublayerAtIndex(gradientLayer, 0);
      }
    }
  }
}
