import { Injectable } from '@angular/core';

@Injectable()
export class CategoryResourceService {

    private map: { [key: string]: string} = {
      'Taco Place': '_mexican_food',
      'Mexican Restaurant': '_mexican_food',
      'Coffee Shop': '_coffee_shop',
      'Chinese Restaurant': '_asian_restaurant',
      'Dim Sum Restaurant': '_asian_restaurant',
      'Ramen Restaurant': '_asian_restaurant',
      'Bubble Tea Shop': '_tea_room',
      'Default': '_world_restaurant',
      'Hawaiian Restaurant': '_world_restaurant',
      'Bakery': '_donnut',
      'Market': '_vegan',
      'Farmers Market': '_vegan',
      'Sandwich Place': '_sandwitches',
      'Grocery Store': '_food',
      'Food Court': '_food',
      'American Restaurant': '_food',
      'New American Restaurant': '_food',
      'Lounge': '_drinks_bar',
      'Gourmet Shop': '_food',
      'Greek Restaurant':'_food',
      'Afghan Restaurant':'_food',
      'African Restaurant':'_food',
      'Asian Restaurant':'_asian_restaurant',
      'Australian Restaurant':'_food',
      'Austrian Restaurant':'_food',
      'BBQ Joint':'_food',
      'Bagel Shop':'_donnut',
      'Belgian Restaurant':'_food',
      'Bistro':'_food',
      'Breakfast Spot':'_breakfast',
      'Buffet':'_food',
      'Burger Joint':'_fast_food',
      'Cafeteria':'_coffee_shop',
      'Cafe':'_coffee_shop',
      'Cajun Creole Restaurant':'_food',
      'Caribbean Restaurant':'_caribean',
      'Caucasian Restaurant':'_food',
      'Comfort Food Restaurant':'_food',
      'Creperie':'_caribean',
      'Czech Restaurant':'_food',
      'Deli Bodega':'_food',
      'Dessert Shop':'_caribean',
      'Diner':'_food',
      'Donut Shop':'_donnut',
      'Dumpling Restaurant':'_asian_restaurant',
      'Dutch Restaurant':'_food',
      'Eastern European Restaurant':'_food',
      'English Restaurant':'_food',
      'Falafel Restaurant':'_food',
      'Fast Food Restaurant':'_fast_food',
      'Fish & Chips Shop':'_fast_food',
      'Fondue Restaurant':'_french',
      'Food Stand':'_food_truck',
      'Food Truck':'_food_truck',
      'French Restaurant':'_french',
      'Fried Chicken Joint':'_fast_food',
      'Friterie':'_fast_food',
      'Gastropub':'_pub',
      'German Restaurant':'_food',
      'Gluten free Restaurant':'_vegan',
      'Halal Restaurant':'_food',
      'Hot Dog Joint':'_fast_food',
      'Hungarian Restaurant':'_food',
      'Indian Restaurant':'_food',
      'Irish Pub':'_pub',
      'Italian Restaurant':'_italian_food',
      'Jewish Restaurant':'_food',
      'Juice Bar':'_pub',
      'Kebab Restaurant':'_food',
      'Latin American Restaurant':'_food',
      'Mac & Cheese Joint':'_fast_food',
      'Mediterranean Restaurant':'_food',
      'Middle Eastern Restaurant':'_food',
      'Modern European Restaurant':'_food',
      'Molecular Gastronomy Restaurant':'_food',
      'Pakistani Restaurant':'_food',
      'Pet Cafe':'_coffee_shop',
      'Pizza Place':'_pizza',
      'Polish Restaurant':'_food',
      'Portuguese Restaurant':'_food',
      'Poutine Place':'_fast_food',
      'Restaurant':'_food',
      'Russian Restaurant':'_food',
      'Salad Place':'_salads',
      'Scandinavian Restaurant':'_food',
      'Scottish Restaurant':'_food',
      'Seafood Restaurant':'_caribean',
      'Slovak Restaurant':'_food',
      'Snack Place':'_fast_food',
      'Soup Place':'_food',
      'Southern Soul Food Restaurant':'_food',
      'Spanish Restaurant':'_food',
      'Sri Lankan Restaurant':'_food',
      'Steakhouse':'_food',
      'Swiss Restaurant':'_food',
      'Tea Room':'_tea_room',
      'Theme Restaurant':'_food',
      'Truck Stop':'_food',
      'Turkish Restaurant':'_food',
      'Ukrainian Restaurant':'_food',
      'Vegetarian Vegan Restaurant':'_vegan',
      'Bar':'_drinks_bar',
      'Beach Bar':'_drinks_bar',
      'Beer Bar':'_pub',
      'Beer Garden':'_pub',
      'Champagne Bar':'_drinks_bar',
      'Cocktail Bar':'_drinks_bar',
      'Dive Bar':'_pub',
      'Gay Bar':'_night_club',
      'Hookah Bar':'_drinks_bar',
      'Hotel Bar':'_drinks_bar',
      'Karaoke Bar':'_karaokee',
      'Pub':'_pub',
      'Sake Bar':'_drinks_bar',
      'Sports Bar':'_sports_bar',
      'Tiki Bar':'_drinks_bar',
      'Whisky Bar':'_drinks_bar',
      'Wine Bar':'_wine',
      'Brewery':'_pub',
      'Night Market':'_food',
      'Nightclub':'_night_club',
      'Other Nightlife':'_night_club',
      'Speakeasy':'_drinks_bar',
      'Strip Club':'_night_club'
    };

  //  Edited by Gopal
    private hcmap: { [key: string]: string} = {
        'Dinner': '_dinner',
        'Dance': '_dance',
        'Drinks': '_drinks',
    };

    constructor() { }

    getResourceName(prefix: string, category: string): string {
        let name = this.map['Default'];
        if (this.map[category]) {
            name = this.map[category];
        }
        return prefix + name;
    }
//  Edited by Gopal
    getResourceHcName(prefix: string, category: string): string {
        let name = this.hcmap['Default'];
        if (this.hcmap[category]) {
            name = this.hcmap[category];
        }
        return prefix + name;
    }
}
