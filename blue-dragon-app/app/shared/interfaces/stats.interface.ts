import { ObservableArray } from "data/observable-array";
export interface StatsInterface {
    gender?: {
        female: number
    },
    crowd?: {
        current: number
    },
    time?: {
        min: number,
        max: number,
        average: number
    },
    heatHours?: Array<any>,
    ages?: ObservableArray<any>,
    displayAges?: boolean
}