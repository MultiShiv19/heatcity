import { Component, OnInit, Input, ViewContainerRef, EventEmitter, Output, OnChanges, NgZone } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import * as dialogs from "ui/dialogs";
import { Router, NavigationExtras } from '@angular/router';
import { LoadingIndicator } from "nativescript-loading-indicator";
import { Color } from 'color';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { DialogComponent } from '../../check-ins/dialog/dialog.component';
import { AccountApi } from '../../shared/sdk/services/custom/Account';
import { VisitApi } from '../../shared/sdk/services/custom/Visit';
import { Like, Visit } from "../../shared/sdk/models";
import { LogoutService } from '../logout.service';

@Component({
    selector: "post-card",
    moduleId: module.id,
    templateUrl: "post-card.component.html",
    styleUrls: ['post-card.component.css']
})
export class PostCardComponent implements OnInit, OnChanges {
    @Input() public data;
    @Output() public action: EventEmitter<any> = new EventEmitter<any>();
    @Input() public isMore: boolean = true;;
    public self = false;
    public commentCount = 0;
    public userId: string;
    public visitId: string;
    public like = new Like();
    // public likeCount = 0;
    //public ownLikes = false;
    //public currentLikeID: string;
    // public isLive = false;
    public currentUser: Account;
    public addressLine: string;
    public likeIcons = [
        String.fromCharCode(0xf08a),
        String.fromCharCode(0xf004),
        String.fromCharCode(0xf08a)
    ]
    public likeIconsColor = [
        new Color("#737373"),
        new Color("#ff0086"),
        new Color("#ff0086")
    ]
    public likeCountDisplay: number = 0;
    public likeIconTxt = '';
    public likeIconColor = this.likeIconsColor[0];

    private _loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                progress: 0,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'// see iOS specific options below
                }
            }
        };
    private _likeIndexInLikeList: number = -1;

    constructor(private routerextensions: RouterExtensions,
        private modalService: ModalDialogService,
        private viewContainerRef: ViewContainerRef,
        private _accountApi: AccountApi,
        private _visitApi: VisitApi,
        private _router: Router,
        private _ngZone: NgZone,
        private logoutService: LogoutService, ) {
    }

    public ngOnInit() {

        this._accountApi.getCurrent().subscribe((account) => {
            this.currentUser = account;
        });

    }

    public ngOnChanges() {
        // console.log("======================= ON CHANGES CALLED!!!!!");
        // console.log("PostCard OnChange :"+ this.data.location.establishment.name);
        this._likeIndexInLikeList = -1;
        if (this.data.likeCount == undefined) this.data["likeCount"] = 0;
        if (this.data.ownLikes == undefined) this.data["ownLikes"] = false;
        this.data.currentLikeID = undefined;
        this.data["isLive"] = false;


        this.userId = this._accountApi.getCurrentId();
        this.visitId = this.data.id;
        this.addressLine = this.data.location.establishment.address;
        try {
            const addressBreak = this.addressLine.split(',');
            this.addressLine = (addressBreak[addressBreak.length - 2].trim() + ',' + addressBreak[addressBreak.length - 1]) || addressBreak[addressBreak.length - 1];
        } catch (error) {
            this.addressLine = this.data.location.establishment.address;
        }
        if (this.userId === this.data.accountId) {
            this.self = true;
        } else {
            this.self = false;
        }
        if (new Date(this.data.exit).getTime() === 0) {
            this.data.isLive = true;
        }
        this.checkLike();
        this.visitLikeCount();
        this.setLikeStatus(this.data.ownLikes);
        this.vistiCommentCount();
    }
    public checkLike() {
        let counter = 0;
        this.data.likes.forEach((like) => {
            if (like.accountId === this.userId) {
                this.data.ownLikes = true;
                this.data.currentLikeID = like.id;
                this._likeIndexInLikeList = counter
                //return true;
            }
            counter++;
            //return false;
        });
    }

    private removeLike() {
        if (this._likeIndexInLikeList == -1) return;
        console.log("size before removing like -> " + this.data.likes.length);
        this.data.likes.splice(this._likeIndexInLikeList, 1);
        console.log("size after removing like -> " + this.data.likes.length);
        this._likeIndexInLikeList = -1;
        this.checkLike();
        this.setLikeStatus(this.data.ownLikes);
        this.visitLikeCount();
    }

    private addLike(like: Like) {
        this.data.likes.push(like);
        this.checkLike();
        this.setLikeStatus(this.data.ownLikes);
        this.visitLikeCount();
    }

    public vistiCommentCount() {
        // console.log("Like");
        this.commentCount = this.data.comments.length;
    }
    public visitLikeCount() {
        // console.log("comment");
        this.data.likeCount = this.data.likes.length;
        this.setLikeToDisplay();
    }

    public onItemTap(args): void {
        // console.log('Item with index: ' + args.index + ' tapped');
    }

    public more(data) {
        // let options: ModalDialogOptions = {
        //     context: { promptMsg: ["Edit", "Delete"] },
        //     fullscreen: false,
        //     viewContainerRef: this.viewContainerRef
        // };

        // this.modalService.showModal(DialogComponent, options)
        //     .then((dialogResult: string) => {
        //         if (dialogResult === 'delete') {
        //             // this._visitApi.deleteById(data).subscribe();
        //             this.action.emit({ data, action: 'delete' });
        //         } else if (dialogResult === 'edit') {
        //             this.action.emit({ data, action: 'edit' });
        //         }
        //     });

        var dialogs = require("ui/dialogs");
        dialogs.action({
            message: "More",
            cancelButtonText: "Cancel",
            actions: ["Edit", "Delete"],
        }).then((dialogResult: string) => {
            if (dialogResult === 'Delete') {
                this.action.emit({ data, action: 'delete' });
            } else if (dialogResult === 'Edit') {
                this.onWrongCheckInTap();
                this.action.emit({ data, action: 'edit' });
            }
        });
    }

    public deleteVisit(id) {
        this.action.emit(id);
    }


    public onCommentTap(args) {
        if (this.data.validity === 100) {
            //this.routerextensions.navigate(['/comment/' + this.visitId]);
            this.routerextensions.navigate(['/comment/' + this.data.id]);
        }
    }

    public onConfirmCheckInTap() {
        if (!this.data || !this.data.location || !this.data.location.establishment) return;
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "visit_id": this.data.id,
                "establishment_id": this.data.location.establishment.id,
            },
        };
        this._router.navigate(["/post"], navigationExtras);
    }

    public onWrongCheckInTap() {
        //alert("visit id -> " + this.visitId + " from data -> " + this.data.id + " establishement -> " + this.data.location.establishment.name);
        if (!this.data || !this.data.location || !this.data.location.establishment) return;
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "visit_id": this.data.id,
                "establishment_id": this.data.location.establishment.id,
                "establishment_name": this.data.location.establishment.name,
                "establishment_address": this.data.location.establishment.address
            },
        };
        this._router.navigate(["/wrong-checkin"], navigationExtras);
    }

    public onLikeTap(args) {
        // console.log("PostCard OnLike :"+ this.data.location.establishment.name);
        if (this.data.ownLikes == undefined || !this.data.ownLikes) {
            try {
                this._loader.instance.show();
                //if (this.data.currentLikeID === undefined) {
                this.like.like = true;
                this.like.accountId = this.userId;
                this.like.visitId = this.data.id;
                this.like.createdAt = new Date();
                this.like.updatedAt = new Date();
                this._visitApi
                    .createLikes(this.data.id, this.like)
                    .subscribe((data) => {
                        this._loader.instance.hide();
                        this.data.currentLikeID = data.id;
                        console.log("Like callBack" + data.id);
                        let like = new Like();
                        like.id = data.id;
                        like.accountId = this.userId;
                        this.addLike(like);
                        this.setLikeStatus(true);
                    }, (error) => {
                        this._loader.instance.hide();
                        this.errorHandler(error);
                        console.error("Error While Click On Like -> " + error.message);
                    });

                if (this.data.likeCount == undefined || this.data.likeCount == '')
                    this.data.likeCount = 0
                this.data.likeCount++;
                this.setLikeToDisplay();
                this.setLikeStatus(true);
                //}
            } catch (error) {
                this._loader.instance.hide();
                console.log("Like Error :" + error);
            }
        } else {
            try {
                this._loader.instance.show();
                if (this.data.currentLikeID !== undefined) {

                    // console.log("likeID:" + this.data.currentLikeID);
                    this._visitApi
                        .destroyByIdLikes(this.data.id, this.data.currentLikeID)
                        .subscribe(() => {
                            this.removeLike();
                            this._loader.instance.hide();
                        }, (error) => {
                            if (error.message == undefined) this.removeLike();
                            this._loader.instance.hide();
                            console.error("Unlike Error :" + error.message)
                        });
                    this.data.likeCount--;
                    this.setLikeToDisplay();
                    this.setLikeStatus(false);
                    this.data.currentLikeID = undefined;

                }
            } catch (error) {
                this._loader.instance.hide();
                console.log("unlike Error" + error);
            }
        }
    }
    //  else {
    //     console.log("likeID:" + this.currentLikeID);
    //     this._visitApi
    //         .destroyByIdLikes(this.visitId, this.currentLikeID)
    //         .subscribe();
    //     this.ownLikes = false;
    //     this.likeCount--;
    //     this.currentLikeID = undefined;
    // };


    public OnUnlikeTap(args) {
        console.log("Like Tap" + this.data.currentLikeID);
        //==========
        this.setLikeStatus(false)
        this.data["ownLikes"] = false;
        this.data.likeCount--;
        this.data.currentLikeID = undefined;
        //==========
        // this.checkLike();
        // console.log("UnLike Tap" + this.data.currentLikeID);
        // console.log("PostCard OnUnlike :"+ this.data.location.establishment.name);

        /*try {
            if (this.data.currentLikeID !== undefined) {

                // console.log("likeID:" + this.data.currentLikeID);
                this._visitApi
                    .destroyByIdLikes(this.data.id, this.data.currentLikeID)
                    .subscribe(() => { }, (error) => {
                        console.error("Unlike Error :" + error.message)
                    });
                this.data.ownLikes = false;
                this.data.likeCount--;
                this.data.currentLikeID = undefined;
                this.action.emit({ action: 'like_unlike' });

            }
        } catch (error) {
            // console.log("unlike Error" + error);
            this.action.emit({ action: 'like_unlike' });
        }*/
    }

    public getAccountDetail() {
        this.routerextensions.navigate(["/user-profile/" + this.data.accountId]);
    }

    public moveToEstablishment(id) {
        this.routerextensions.navigate(["/establishment", id]);
    }

    private setLikeStatus(ownLike: boolean): void {
        this.data["ownLikes"] = ownLike;
        if (this.data["ownLikes"]) {
            this.likeIconColor = this.likeIconsColor[1]
            this.likeIconTxt = this.likeIcons[1]
        } else if (this.data.likeCount > 0) {
            this.likeIconColor = this.likeIconsColor[2]
            this.likeIconTxt = this.likeIcons[2]
        } else {
            this.likeIconColor = this.likeIconsColor[0]
            this.likeIconTxt = this.likeIcons[0]
        }
    }

    private setLikeToDisplay() {
        if (this.data.likeCount == undefined || this.data.likeCount == '')
            this.data.likeCount = 0;

        if (this.data.likeCount > 0) this.likeCountDisplay = 0 + this.data.likeCount
        else this.likeCountDisplay = 0
    }

    public errorHandler(error: any): void {
        console.log('User profile Error', JSON.stringify(error));
        if (error && error.statusCode === 401) {
            this.logoutService.logout();
        }
    }
}
