import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'loadItems'})
export class LoadItemsPipe implements PipeTransform {
    transform(items: Array<any>, limit: number): Array<any> {
        if(limit >= items.length) {
            return items;
        } else {
            return items.slice(0, limit);
        }
    }
}