import { Pipe, PipeTransform } from '@angular/core';
import { BASE_URL, API_VERSION } from '../base.api';
import { DEFAULTS } from '../config';
import { Establishment, Account } from '../sdk/models';

@Pipe({name: 'accountUrl'})
export class AccountUrlPipe implements PipeTransform {
    transform(account: Account, type: string): string {
        let url: string;
        switch (type) {
            case 'background-img':
                url =  `url('${this.getUrlFromAccount(account)}')`;
                break;
            default:
                url =  this.getUrlFromAccount(account);
                break;
        }
        return url;
    }
    
    getUrlFromAccount(account: Account): string {
//        console.dir(account);
        let path = account && account.photo
            && account.photo.path ?
            account.photo.path :
            ``;
        return path;
    }
}