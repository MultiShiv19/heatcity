import { Pipe, PipeTransform } from '@angular/core';
import { StatsInterface } from '../interfaces';
interface TimeInterface {
    minutes: string;
    hours: string;
}
@Pipe({name: 'timeSpent'})
export class TimeSpentPipe implements PipeTransform {
    /*
    *   This pipe allow parse the information of time spend to display
    *   durations with different formats.
    */
    transform(stats: StatsInterface, type: string): string {
        let labelText: string;
        switch (type) {
            case 'min':
                labelText = this.getMinDuration(stats);
                break;
            case 'max':
                labelText = this.getMaxDuration(stats);
                break;
            case 'average':
                labelText = this.getAverageDuration(stats);
                break;
            case 'duration':
                labelText = this.getDuration(stats)
                break;
        }
        return labelText.trim();
    }

    getAverageDuration(stats: StatsInterface): string {
        let value = '';
        if (stats && stats.time && stats.time.average) {
            let time = this.formatMilisecods(stats.time.average);
            value = this.formatHrsMin(time);
        }
        return value + ' average visit';
    }

    getMaxDuration(stats: StatsInterface): string {
        let value = '';
        if (stats && stats.time && stats.time.max) {
            let time = this.formatMilisecods(stats.time.max);
            value = this.formatHrsMin(time);
        }
        return value;
    }

    getMinDuration(stats: StatsInterface): string {
        let value = '';
        if (stats && stats.time && stats.time.min) {
            let time = this.formatMilisecods(stats.time.min);
            value = this.formatHrsMin(time);
        }
        return value;
    }

    getDuration(stats: StatsInterface): string {
        let value = '';
        if (stats && stats.time && stats.time.average) {
            let time = this.formatMilisecods(stats.time.average);
            value = this.formatEnglishNotation(time);
        }
        return value;
    }

    formatMilisecods(miliseconds: number): TimeInterface {
        let minutes = ~~(miliseconds/(1000*60))%60,
            hours = ~~(miliseconds/(1000*60*60))%24;
        return {
            minutes: minutes < 10 ? '0' + minutes.toString(): minutes.toString(),
            hours: hours < 10 ? '0' + hours.toString(): hours.toString()
        };
    }

    formatEnglishNotation(time: TimeInterface): string {
        let value: string;
        value = time.hours !== '00' ? time.hours + '′ ' : '';
        value += time.minutes !== '00' ? time.minutes + '″' : '';
        return value;
    }

    formatHHMM(time: TimeInterface): string {
        let value: string;
        value = time.hours !== '00' ? time.hours + ':' : '';
        value += time.minutes !== '00' ? time.minutes + '' : '';
        return value;
    }

    formatHrsMin(time: TimeInterface): string {
        let value: string;
        value = time.hours !== '00' ? parseInt(time.hours) + ' hrs ' : '';
        value += time.minutes !== '00' ? parseInt(time.minutes) + ' min' : '';
        return value;
    }
}
