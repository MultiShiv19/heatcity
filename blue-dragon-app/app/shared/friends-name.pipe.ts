import { Pipe, PipeTransform } from '@angular/core';
import { Account } from './sdk/models';
import { FormattedString } from "text/formatted-string";
import { Span } from "text/span";

@Pipe({name: 'friendsName'})
export class FriendsName implements PipeTransform{
    transform(friends: Account[], show: number): FormattedString {
        let formattedString = new FormattedString();
        let initialText = new Span();
        let friendsNames = new Span();
        let and = new Span();
        let othersSize = new Span();
        let others = new Span();

        initialText.text = 'Friends with ';
        and.text = ' & '
        others.text = ' others';
        if(friends.length > 0) {
            formattedString.spans.push(initialText);
            if(friends[0].firstName)
                friendsNames.text = friends[0].firstName;
        }
        for(let i = 1; i < friends.length && i < show; i++){
            if(friends[i].firstName) {
                friendsNames.text += ', ' +friends[i].firstName;
            } else {
                if(friends[i].displayName)
                    friendsNames.text += ', ' +friends[i].displayName.split(' ')[0];
            }
        }
        friendsNames.fontWeight = 'bold';
        formattedString.spans.push(friendsNames);
        if(friends.length > show) {
            othersSize.text = (friends.length - show) + '';
            formattedString.spans.push(and);
            formattedString.spans.push(othersSize);
            formattedString.spans.push(others);
        }
        return formattedString;
    }
}
