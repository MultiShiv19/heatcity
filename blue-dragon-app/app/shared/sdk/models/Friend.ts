/* tslint:disable */
import {
    Account
} from '../index';

declare var Object: any;
export interface FriendInterface {
    "approved"?: boolean;
    "deletedAt"?: Date;
    "accountId": any;
    "friendId": any;
    "mutual"?: boolean;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    account?: Account;
    friend?: Account;
}

export class Friend implements FriendInterface {
    "approved": boolean = true;
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "friendId": any = <any>null;
    "mutual": boolean = true;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    account: Account = null;
    friend: Account = null;
    constructor(data?: FriendInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Friend`.
     */
    public static getModelName() {
        return "Friend";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Friend for dynamic purposes.
    **/
    public static factory(data: FriendInterface): Friend {
        return new Friend(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Friend',
            plural: 'friends',
            path: 'friends',
            idName: 'id',
            properties: {
                "approved": {
                    name: 'approved',
                    type: 'boolean',
                    default: true
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "friendId": {
                    name: 'friendId',
                    type: 'any'
                },
                "mutual": {
                    name: 'mutual',
                    type: 'boolean',
                    default: true
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
                friend: {
                    name: 'friend',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'friendId',
                    keyTo: 'id'
                },
            }
        }
    }
}
