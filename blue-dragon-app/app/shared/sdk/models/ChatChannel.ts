/* tslint:disable */
import {
    Account,
    Location,
    Message
} from '../index';

declare var Object: any;
export interface ChatChannelInterface {
    "name"?: string;
    "description"?: string;
    "expiresAt"?: Date;
    "deletedAt"?: Date;
    "accountId": any;
    "locationId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    account?: Account;
    location?: Location;
    messages?: Message[];
}

export class ChatChannel implements ChatChannelInterface {
    "name": string = '';
    "description": string = '';
    "expiresAt": Date = new Date();
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "locationId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    account: Account = null;
    location: Location = null;
    messages: Message[] = null;
    constructor(data?: ChatChannelInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `ChatChannel`.
     */
    public static getModelName() {
        return "ChatChannel";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of ChatChannel for dynamic purposes.
    **/
    public static factory(data: ChatChannelInterface): ChatChannel {
        return new ChatChannel(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'ChatChannel',
            plural: 'chatChannels',
            path: 'chatChannels',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "description": {
                    name: 'description',
                    type: 'string'
                },
                "expiresAt": {
                    name: 'expiresAt',
                    type: 'Date'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "locationId": {
                    name: 'locationId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
                location: {
                    name: 'location',
                    type: 'Location',
                    model: 'Location',
                    relationType: 'belongsTo',
                    keyFrom: 'locationId',
                    keyTo: 'id'
                },
                messages: {
                    name: 'messages',
                    type: 'Message[]',
                    model: 'Message',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'chatChannelId'
                },
            }
        }
    }
}
