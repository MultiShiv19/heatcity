/* tslint:disable */
import {
    Account,
    Establishment
} from '../index';

declare var Object: any;
export interface MusicListInterface {
    "name": string;
    "deletedAt"?: Date;
    "accountId": any;
    "establishmentId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    account?: Account;
    establishment?: Establishment;
}

export class MusicList implements MusicListInterface {
    "name": string = '';
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "establishmentId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    account: Account = null;
    establishment: Establishment = null;
    constructor(data?: MusicListInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `MusicList`.
     */
    public static getModelName() {
        return "MusicList";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of MusicList for dynamic purposes.
    **/
    public static factory(data: MusicListInterface): MusicList {
        return new MusicList(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'MusicList',
            plural: 'musicLists',
            path: 'musicLists',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "establishmentId": {
                    name: 'establishmentId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
                establishment: {
                    name: 'establishment',
                    type: 'Establishment',
                    model: 'Establishment',
                    relationType: 'belongsTo',
                    keyFrom: 'establishmentId',
                    keyTo: 'id'
                },
            }
        }
    }
}
