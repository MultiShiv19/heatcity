/* tslint:disable */
import {
    Visit,
    Account
} from '../index';

declare var Object: any;
export interface CommentInterface {
    "accountId"?: any;
    "visitId": any;
    "comment"?: string;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    visit?: Visit;
    account?: Account;
}

export class Comment implements CommentInterface {
    "accountId": any = <any>null;
    "visitId": any = <any>null;
    "comment": string = '';
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    visit: Visit = null;
    account: Account = null;
    constructor(data?: CommentInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Comment`.
     */
    public static getModelName() {
        return "Comment";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Comment for dynamic purposes.
    **/
    public static factory(data: CommentInterface): Comment {
        return new Comment(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Comment',
            plural: 'comments',
            path: 'comments',
            idName: 'id',
            properties: {
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "visitId": {
                    name: 'visitId',
                    type: 'any'
                },
                "comment": {
                    name: 'comment',
                    type: 'string'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                visit: {
                    name: 'visit',
                    type: 'Visit',
                    model: 'Visit',
                    relationType: 'belongsTo',
                    keyFrom: 'visitId',
                    keyTo: 'id'
                },
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
