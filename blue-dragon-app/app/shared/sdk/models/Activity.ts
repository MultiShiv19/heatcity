/* tslint:disable */
import {
    ActivityCategory,
    Location
} from '../index';

declare var Object: any;
export interface ActivityInterface {
    "name"?: string;
    "approved"?: boolean;
    "deletedAt"?: Date;
    "id"?: any;
    "activityCategoryId"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    activityCategory?: ActivityCategory;
    locations?: Location[];
}

export class Activity implements ActivityInterface {
    "name": string = '';
    "approved": boolean = false;
    "deletedAt": Date = new Date();
    "id": any = <any>null;
    "activityCategoryId": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    activityCategory: ActivityCategory = null;
    locations: Location[] = null;
    constructor(data?: ActivityInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Activity`.
     */
    public static getModelName() {
        return "Activity";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Activity for dynamic purposes.
    **/
    public static factory(data: ActivityInterface): Activity {
        return new Activity(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Activity',
            plural: 'activities',
            path: 'activities',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "approved": {
                    name: 'approved',
                    type: 'boolean',
                    default: false
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "activityCategoryId": {
                    name: 'activityCategoryId',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                activityCategory: {
                    name: 'activityCategory',
                    type: 'ActivityCategory',
                    model: 'ActivityCategory',
                    relationType: 'belongsTo',
                    keyFrom: 'activityCategoryId',
                    keyTo: 'id'
                },
                locations: {
                    name: 'locations',
                    type: 'Location[]',
                    model: 'Location',
                    relationType: 'hasMany',
                    modelThrough: 'LocationActivity',
                    keyThrough: 'locationId',
                    keyFrom: 'id',
                    keyTo: 'activityId'
                },
            }
        }
    }
}
