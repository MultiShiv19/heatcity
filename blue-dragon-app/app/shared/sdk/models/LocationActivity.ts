/* tslint:disable */
import {
    Activity,
    Visit,
    Location
} from '../index';

declare var Object: any;
export interface LocationActivityInterface {
    "deletedAt"?: Date;
    "locationId": any;
    "activityId": any;
    "visitId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    activity?: Activity;
    visit?: Visit;
    location?: Location;
}

export class LocationActivity implements LocationActivityInterface {
    "deletedAt": Date = new Date();
    "locationId": any = <any>null;
    "activityId": any = <any>null;
    "visitId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    activity: Activity = null;
    visit: Visit = null;
    location: Location = null;
    constructor(data?: LocationActivityInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `LocationActivity`.
     */
    public static getModelName() {
        return "LocationActivity";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of LocationActivity for dynamic purposes.
    **/
    public static factory(data: LocationActivityInterface): LocationActivity {
        return new LocationActivity(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'LocationActivity',
            plural: 'locationActivities',
            path: 'locationActivities',
            idName: 'id',
            properties: {
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "locationId": {
                    name: 'locationId',
                    type: 'any'
                },
                "activityId": {
                    name: 'activityId',
                    type: 'any'
                },
                "visitId": {
                    name: 'visitId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                activity: {
                    name: 'activity',
                    type: 'Activity',
                    model: 'Activity',
                    relationType: 'belongsTo',
                    keyFrom: 'activityId',
                    keyTo: 'id'
                },
                visit: {
                    name: 'visit',
                    type: 'Visit',
                    model: 'Visit',
                    relationType: 'belongsTo',
                    keyFrom: 'visitId',
                    keyTo: 'id'
                },
                location: {
                    name: 'location',
                    type: 'Location',
                    model: 'Location',
                    relationType: 'belongsTo',
                    keyFrom: 'locationId',
                    keyTo: 'id'
                },
            }
        }
    }
}
