/* tslint:disable */
import {
    LocationFix,
    Visit,
    Activity,
    Establishment,
    ChatChannel
} from '../index';

declare var Object: any;
export interface LocationInterface {
    "geoLocation"?: any;
    "deletedAt"?: Date;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    fixes?: LocationFix[];
    visits?: Visit[];
    activities?: Activity[];
    establishment?: Establishment;
    chatChannels?: ChatChannel[];
}

export class Location implements LocationInterface {
    "geoLocation": any = <any>null;
    "deletedAt": Date = new Date();
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    fixes: LocationFix[] = null;
    visits: Visit[] = null;
    activities: Activity[] = null;
    establishment: Establishment = null;
    chatChannels: ChatChannel[] = null;
    constructor(data?: LocationInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Location`.
     */
    public static getModelName() {
        return "Location";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Location for dynamic purposes.
    **/
    public static factory(data: LocationInterface): Location {
        return new Location(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Location',
            plural: 'locations',
            path: 'locations',
            idName: 'id',
            properties: {
                "geoLocation": {
                    name: 'geoLocation',
                    type: 'any'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                fixes: {
                    name: 'fixes',
                    type: 'LocationFix[]',
                    model: 'LocationFix',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'fixedId'
                },
                visits: {
                    name: 'visits',
                    type: 'Visit[]',
                    model: 'Visit',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'locationId'
                },
                activities: {
                    name: 'activities',
                    type: 'Activity[]',
                    model: 'Activity',
                    relationType: 'hasMany',
                    modelThrough: 'LocationActivity',
                    keyThrough: 'activityId',
                    keyFrom: 'id',
                    keyTo: 'locationId'
                },
                establishment: {
                    name: 'establishment',
                    type: 'Establishment',
                    model: 'Establishment',
                    relationType: 'hasOne',
                    keyFrom: 'id',
                    keyTo: 'locationId'
                },
                chatChannels: {
                    name: 'chatChannels',
                    type: 'ChatChannel[]',
                    model: 'ChatChannel',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'locationId'
                },
            }
        }
    }
}
