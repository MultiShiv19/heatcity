/* tslint:disable */
import {
    Account,
    Establishment,
    FavoriteList
} from '../index';

declare var Object: any;
export interface FavoriteInterface {
    "deletedAt"?: Date;
    "accountId": any;
    "favoriteListId": any;
    "establishmentId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    account?: Account;
    establishment?: Establishment;
    favoriteList?: FavoriteList;
}

export class Favorite implements FavoriteInterface {
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "favoriteListId": any = <any>null;
    "establishmentId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    account: Account = null;
    establishment: Establishment = null;
    favoriteList: FavoriteList = null;
    constructor(data?: FavoriteInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Favorite`.
     */
    public static getModelName() {
        return "Favorite";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Favorite for dynamic purposes.
    **/
    public static factory(data: FavoriteInterface): Favorite {
        return new Favorite(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Favorite',
            plural: 'favorites',
            path: 'favorites',
            idName: 'id',
            properties: {
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "favoriteListId": {
                    name: 'favoriteListId',
                    type: 'any'
                },
                "establishmentId": {
                    name: 'establishmentId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
                establishment: {
                    name: 'establishment',
                    type: 'Establishment',
                    model: 'Establishment',
                    relationType: 'belongsTo',
                    keyFrom: 'establishmentId',
                    keyTo: 'id'
                },
                favoriteList: {
                    name: 'favoriteList',
                    type: 'FavoriteList',
                    model: 'FavoriteList',
                    relationType: 'belongsTo',
                    keyFrom: 'favoriteListId',
                    keyTo: 'id'
                },
            }
        }
    }
}
