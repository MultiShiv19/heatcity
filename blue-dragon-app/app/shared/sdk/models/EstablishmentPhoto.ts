/* tslint:disable */
import {
    Establishment,
    Account
} from '../index';

declare var Object: any;
export interface EstablishmentPhotoInterface {
    "file": any;
    "deletedAt"?: Date;
    "expiresAt"?: Date;
    "id"?: any;
    "establishmentId"?: any;
    "accountId"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    establishment?: Establishment;
    account?: Account;
}

export class EstablishmentPhoto implements EstablishmentPhotoInterface {
    "file": any = <any>null;
    "deletedAt": Date = new Date();
    "expiresAt": Date = new Date();
    "id": any = <any>null;
    "establishmentId": any = <any>null;
    "accountId": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    establishment: Establishment = null;
    account: Account = null;
    constructor(data?: EstablishmentPhotoInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `EstablishmentPhoto`.
     */
    public static getModelName() {
        return "EstablishmentPhoto";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of EstablishmentPhoto for dynamic purposes.
    **/
    public static factory(data: EstablishmentPhotoInterface): EstablishmentPhoto {
        return new EstablishmentPhoto(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'EstablishmentPhoto',
            plural: 'establishmentPhotos',
            path: 'establishmentPhotos',
            idName: 'id',
            properties: {
                "file": {
                    name: 'file',
                    type: 'any'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "expiresAt": {
                    name: 'expiresAt',
                    type: 'Date'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "establishmentId": {
                    name: 'establishmentId',
                    type: 'any'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                establishment: {
                    name: 'establishment',
                    type: 'Establishment',
                    model: 'Establishment',
                    relationType: 'belongsTo',
                    keyFrom: 'establishmentId',
                    keyTo: 'id'
                },
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
