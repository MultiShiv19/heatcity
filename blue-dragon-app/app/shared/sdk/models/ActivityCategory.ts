/* tslint:disable */
import {
    Activity
} from '../index';

declare var Object: any;
export interface ActivityCategoryInterface {
    "name"?: string;
    "deletedAt"?: Date;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    activities?: Activity[];
}

export class ActivityCategory implements ActivityCategoryInterface {
    "name": string = '';
    "deletedAt": Date = new Date();
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    activities: Activity[] = null;
    constructor(data?: ActivityCategoryInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `ActivityCategory`.
     */
    public static getModelName() {
        return "ActivityCategory";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of ActivityCategory for dynamic purposes.
    **/
    public static factory(data: ActivityCategoryInterface): ActivityCategory {
        return new ActivityCategory(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'ActivityCategory',
            plural: 'activityCategories',
            path: 'activityCategories',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
                activities: {
                    name: 'activities',
                    type: 'Activity[]',
                    model: 'Activity',
                    relationType: 'hasMany',
                    keyFrom: 'id',
                    keyTo: 'activityCategoryId'
                },
            }
        }
    }
}
