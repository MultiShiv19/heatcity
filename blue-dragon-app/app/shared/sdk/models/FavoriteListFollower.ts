/* tslint:disable */
import {
    FavoriteList,
    Account
} from '../index';

declare var Object: any;
export interface FavoriteListFollowerInterface {
    "deletedAt"?: Date;
    "accountId": any;
    "favoriteListId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    favoriteList?: FavoriteList;
    account?: Account;
}

export class FavoriteListFollower implements FavoriteListFollowerInterface {
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "favoriteListId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    favoriteList: FavoriteList = null;
    account: Account = null;
    constructor(data?: FavoriteListFollowerInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `FavoriteListFollower`.
     */
    public static getModelName() {
        return "FavoriteListFollower";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of FavoriteListFollower for dynamic purposes.
    **/
    public static factory(data: FavoriteListFollowerInterface): FavoriteListFollower {
        return new FavoriteListFollower(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'FavoriteListFollower',
            plural: 'favoriteListFollowers',
            path: 'favoriteListFollowers',
            idName: 'id',
            properties: {
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "favoriteListId": {
                    name: 'favoriteListId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                favoriteList: {
                    name: 'favoriteList',
                    type: 'FavoriteList',
                    model: 'FavoriteList',
                    relationType: 'belongsTo',
                    keyFrom: 'favoriteListId',
                    keyTo: 'id'
                },
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
