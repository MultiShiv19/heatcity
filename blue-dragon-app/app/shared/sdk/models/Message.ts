/* tslint:disable */
import {
    ChatChannel,
    Account
} from '../index';

declare var Object: any;
export interface MessageInterface {
    "text"?: string;
    "attachments"?: Array<any>;
    "deletedAt"?: Date;
    "accountId": any;
    "chatChannelId": any;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "bizAccountId"?: any;
    chatChannel?: ChatChannel;
    account?: Account;
}

export class Message implements MessageInterface {
    "text": string = '';
    "attachments": Array<any> = <any>[];
    "deletedAt": Date = new Date();
    "accountId": any = <any>null;
    "chatChannelId": any = <any>null;
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "bizAccountId": any = <any>null;
    chatChannel: ChatChannel = null;
    account: Account = null;
    constructor(data?: MessageInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `Message`.
     */
    public static getModelName() {
        return "Message";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of Message for dynamic purposes.
    **/
    public static factory(data: MessageInterface): Message {
        return new Message(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'Message',
            plural: 'messages',
            path: 'messages',
            idName: 'id',
            properties: {
                "text": {
                    name: 'text',
                    type: 'string'
                },
                "attachments": {
                    name: 'attachments',
                    type: 'Array&lt;any&gt;',
                    default: <any>[]
                },
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "accountId": {
                    name: 'accountId',
                    type: 'any'
                },
                "chatChannelId": {
                    name: 'chatChannelId',
                    type: 'any'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "bizAccountId": {
                    name: 'bizAccountId',
                    type: 'any'
                },
            },
            relations: {
                chatChannel: {
                    name: 'chatChannel',
                    type: 'ChatChannel',
                    model: 'ChatChannel',
                    relationType: 'belongsTo',
                    keyFrom: 'chatChannelId',
                    keyTo: 'id'
                },
                account: {
                    name: 'account',
                    type: 'Account',
                    model: 'Account',
                    relationType: 'belongsTo',
                    keyFrom: 'accountId',
                    keyTo: 'id'
                },
            }
        }
    }
}
