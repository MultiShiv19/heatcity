/* tslint:disable */

declare var Object: any;
export interface ParentOrgInterface {
    "name": string;
    "description"?: string;
    "email"?: string;
    "url"?: string;
    "address"?: string;
    "id"?: any;
    "createdAt": Date;
    "updatedAt": Date;
}

export class ParentOrg implements ParentOrgInterface {
    "name": string = '';
    "description": string = '';
    "email": string = '';
    "url": string = '';
    "address": string = '';
    "id": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    constructor(data?: ParentOrgInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `ParentOrg`.
     */
    public static getModelName() {
        return "ParentOrg";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of ParentOrg for dynamic purposes.
    **/
    public static factory(data: ParentOrgInterface): ParentOrg {
        return new ParentOrg(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'ParentOrg',
            plural: 'parentorgs',
            path: 'parentorgs',
            idName: 'id',
            properties: {
                "name": {
                    name: 'name',
                    type: 'string'
                },
                "description": {
                    name: 'description',
                    type: 'string'
                },
                "email": {
                    name: 'email',
                    type: 'string'
                },
                "url": {
                    name: 'url',
                    type: 'string'
                },
                "address": {
                    name: 'address',
                    type: 'string'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
            },
            relations: {
            }
        }
    }
}
