/* tslint:disable */
import {
    Visit,
    Location
} from '../index';

declare var Object: any;
export interface LocationFixInterface {
    "deletedAt"?: Date;
    "id"?: any;
    "visitId"?: any;
    "createdAt": Date;
    "updatedAt": Date;
    "fixedId"?: any;
    "incorrectId"?: any;
    visit?: Visit;
    fixed?: Location;
    incorrect?: Location;
}

export class LocationFix implements LocationFixInterface {
    "deletedAt": Date = new Date();
    "id": any = <any>null;
    "visitId": any = <any>null;
    "createdAt": Date = new Date();
    "updatedAt": Date = new Date();
    "fixedId": any = <any>null;
    "incorrectId": any = <any>null;
    visit: Visit = null;
    fixed: Location = null;
    incorrect: Location = null;
    constructor(data?: LocationFixInterface) {
        Object.assign(this, data);
    }
    /**
     * The name of the model represented by this $resource,
     * i.e. `LocationFix`.
     */
    public static getModelName() {
        return "LocationFix";
    }
    /**
    * @method factory
    * @author Jonathan Casarrubias
    * @license MIT
    * This method creates an instance of LocationFix for dynamic purposes.
    **/
    public static factory(data: LocationFixInterface): LocationFix {
        return new LocationFix(data);
    }
    /**
    * @method getModelDefinition
    * @author Julien Ledun
    * @license MIT
    * This method returns an object that represents some of the model
    * definitions.
    **/
    public static getModelDefinition() {
        return {
            name: 'LocationFix',
            plural: 'locationFixes',
            path: 'locationFixes',
            idName: 'id',
            properties: {
                "deletedAt": {
                    name: 'deletedAt',
                    type: 'Date'
                },
                "id": {
                    name: 'id',
                    type: 'any'
                },
                "visitId": {
                    name: 'visitId',
                    type: 'any'
                },
                "createdAt": {
                    name: 'createdAt',
                    type: 'Date'
                },
                "updatedAt": {
                    name: 'updatedAt',
                    type: 'Date'
                },
                "fixedId": {
                    name: 'fixedId',
                    type: 'any'
                },
                "incorrectId": {
                    name: 'incorrectId',
                    type: 'any'
                },
            },
            relations: {
                visit: {
                    name: 'visit',
                    type: 'Visit',
                    model: 'Visit',
                    relationType: 'belongsTo',
                    keyFrom: 'visitId',
                    keyTo: 'id'
                },
                fixed: {
                    name: 'fixed',
                    type: 'Location',
                    model: 'Location',
                    relationType: 'belongsTo',
                    keyFrom: 'fixedId',
                    keyTo: 'id'
                },
                incorrect: {
                    name: 'incorrect',
                    type: 'Location',
                    model: 'Location',
                    relationType: 'belongsTo',
                    keyFrom: 'incorrectId',
                    keyTo: 'id'
                },
            }
        }
    }
}
