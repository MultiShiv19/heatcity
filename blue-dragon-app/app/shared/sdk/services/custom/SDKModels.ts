/* tslint:disable */
import { Injectable } from '@angular/core';
import { User } from '../../models/User';
import { Account } from '../../models/Account';
import { Friend } from '../../models/Friend';
import { ExternalFriend } from '../../models/ExternalFriend';
import { Establishment } from '../../models/Establishment';
import { Promotion } from '../../models/Promotion';
import { EstablishmentPhoto } from '../../models/EstablishmentPhoto';
import { Event } from '../../models/Event';
import { Visit } from '../../models/Visit';
import { ActivityCategory } from '../../models/ActivityCategory';
import { Activity } from '../../models/Activity';
import { LocationActivity } from '../../models/LocationActivity';
import { LocationFix } from '../../models/LocationFix';
import { Location } from '../../models/Location';
import { MusicList } from '../../models/MusicList';
import { Share } from '../../models/Share';
import { FavoriteList } from '../../models/FavoriteList';
import { Favorite } from '../../models/Favorite';
import { FavoriteListFollower } from '../../models/FavoriteListFollower';
import { ChatChannel } from '../../models/ChatChannel';
import { Message } from '../../models/Message';
import { Area } from '../../models/Area';
import { Storage } from '../../models/Storage';
import { SocialCredential } from '../../models/SocialCredential';
import { Twilio } from '../../models/Twilio';
import { Device } from '../../models/Device';
import { Notification } from '../../models/Notification';
import { Like } from '../../models/Like';
import { Comment } from '../../models/Comment';
import { BizAccount } from '../../models/BizAccount';
import { Invitee } from '../../models/Invitee';
import { ParentOrg } from '../../models/ParentOrg';
import { Search } from '../../models/Search';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    User: User,
    Account: Account,
    Friend: Friend,
    ExternalFriend: ExternalFriend,
    Establishment: Establishment,
    Promotion: Promotion,
    EstablishmentPhoto: EstablishmentPhoto,
    Event: Event,
    Visit: Visit,
    ActivityCategory: ActivityCategory,
    Activity: Activity,
    LocationActivity: LocationActivity,
    LocationFix: LocationFix,
    Location: Location,
    MusicList: MusicList,
    Share: Share,
    FavoriteList: FavoriteList,
    Favorite: Favorite,
    FavoriteListFollower: FavoriteListFollower,
    ChatChannel: ChatChannel,
    Message: Message,
    Area: Area,
    Storage: Storage,
    SocialCredential: SocialCredential,
    Twilio: Twilio,
    Device: Device,
    Notification: Notification,
    Like: Like,
    Comment: Comment,
    BizAccount: BizAccount,
    Invitee: Invitee,
    ParentOrg: ParentOrg,
    Search: Search,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
