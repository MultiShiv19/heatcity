import { Injectable, Inject } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { BASE_URL, API_VERSION } from '../shared/base.api';
import { StorageNative } from '../shared/sdk/storage/storage.native';
import { LoopBackConfig } from '../shared/sdk';
import { Account } from '../shared/sdk/models';
import { AccountApi } from '../shared/sdk/services';

@Injectable()
export class LogoutService {

    constructor(
        @Inject(AccountApi) private accountApi,
        @Inject(StorageNative) private storage,
        @Inject(RouterExtensions) private router,
    ) {
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
    }

    
    public logout(): void {
        let subscription = this.accountApi.logout().subscribe(
            (res) => {
                this.clearStorage();
                this.router.navigate(['/login']);
                subscription.unsubscribe();
            }, err => {
                this.clearStorage();
                this.router.navigate(['/login'])
                subscription.unsubscribe();
            })
    }

    private clearStorage(): void {
        this.storage.remove('facebookToken');
        this.storage.remove('invitationCode');
        this.storage.remove('notifications');
    }
}