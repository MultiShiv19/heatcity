// External imports
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Page } from 'ui/page';
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs';
import { LoadingIndicator } from 'nativescript-loading-indicator';
import { Subject } from 'rxjs/Subject';
import geolocation = require('nativescript-geolocation');
const uniqBy = require('lodash/uniqBy');
import dialogs = require("ui/dialogs");
import { SwipeGestureEventData, SwipeDirection } from "ui/gestures";
import { isIOS } from "platform";

// Internal imports
import { VisitDetectionService } from '../map/visit-detection/visit.detection.service';
import { Establishment, Visit, LoopBackFilter } from '../shared/sdk/models';
import { EstablishmentApi, VisitApi, AccountApi } from '../shared/sdk/services';
import { EstablishmentHelper } from './helper/establishment.helper';
import { Accuracy } from "ui/enums";
import { MapView, Marker, Polyline, Position } from 'nativescript-google-maps-sdk';

declare var UITableViewCellSelectionStyle: any;
@Component({
    moduleId: module.id,
    selector: 'establishment',
    templateUrl: 'establishment.component.html',
    styleUrls: ['establishment-common.css', 'establishment.component.css']
})

export class EstablishmentComponent implements OnInit, OnDestroy {
    private _TAG: string = "Establishment";

    /** List of active subscriptions which we can unsubcrive from **/
    private _subscriptions: Array<Subscription> = [];

    /** Establishment object **/
    private _establishment: Establishment = new Establishment();

    /**
     * A convienient helper for this class which might have more
     * responsibilities in future for separation of concern
     */
    private _establishmentHelper: EstablishmentHelper = new EstablishmentHelper();

    /** List of Visits **/
    public visits: Array<Visit> = [];
    /** List of There Now **/
    public visitsThereNow: Array<Visit> = [];
    /** List of Item to be displayed. This either contains Visit List or There Now List at a time. */
    public listItems: Array<Visit> = [];
    public visitCount: number;

    /**
     * Boolean which states if user is currently in list mode of visit or there now.
     */
    public isInListMode: boolean = false;
    public listMode: string = "";
    private _invalideDate: string = "1970-01-01T00:00:00.000Z";

    /**
     * Loading indicator
     */
    private _loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                android: {
                    indeterminate: true,
                    cancelable: false
                },
                ios: {
                    square: false,
                    margin: 10,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'
                }
            }
        };

    public isDataLoaded: boolean = false;

    /** @type {Subscription} **/
    private locationSubscription: Subscription;

    /** @type {Position} **/
    private positions: { user?: geolocation.Location, current?: Position, lastUpdated?: Position, lastPosition?: Position };

    /** @type {Subject<Position>} **/
    private locationSubject: Subject<Position> = new Subject<Position>();

    private _around: geolocation.Location;

    private _watchId: any;

    private _userRadius: number = -1;

    public isUserLive = false;

    constructor(private _page: Page,
        private _establishmentApi: EstablishmentApi,
        private _pageRoute: PageRoute,
        private _visitApi: VisitApi,
        private _routerExtensions: RouterExtensions,
        private _router: Router,
        private _visitDetectionService: VisitDetectionService,
        private _accountApi: AccountApi) {
        this._page.actionBarHidden = true;
        this.getUrlParam();
        this._page.on("loaded", this.onLoaded, this);
    }

    onLoaded() {
        console.log("onLoaded called");
        if (this._establishment && this._establishment.locationId) {
            this.getLastVisits(this._establishment.locationId);
        }
    }

    public ngOnInit(): void {
        if (isIOS) this._page.enableSwipeBackNavigation = false;
        this._page.backgroundColor = "black";
        this._page.backgroundSpanUnderStatusBar = true;
    }
    public listViewItemLoading(args): void {
        if (isIOS) {

            args.ios.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }
    public ngOnDestroy(): void {
        // Clear the pending API calls subscriptions
        this._subscriptions.forEach((subscription) => {
            subscription.unsubscribe();
        })

        if (this._watchId) {
            geolocation.clearWatch(this._watchId);
        }
    }

    /**
     * @method getUrlParam
     * @return {void}
     */
    private getUrlParam(): void {
        this._pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                let id = params['id'];
                this.getEstablishment(id);
            })
    }

    /**
     * @method getEstablishment
     * @param {string} establishmentId
     * @return {void}
     */
    private getEstablishment(establishmentId: string): void {
        let filter: LoopBackFilter = {
            include: ['photos', 'location']
        }
        // Show loading indicator
        //this._loader.instance.show();
        this._subscriptions.push(
            this._establishmentApi.findById(establishmentId, filter)
                .subscribe((establishment: Establishment) => {
                    this._establishment = establishment;
                    this.watch();
                    this._visitDetectionService.startVisitDetector();
                    this.getLastVisits(establishment.locationId);
                },
                this.error
                )
        );
    }

    /**
     * @method getLastVisits
     * @param {string} locationId Location Id of Establishment. i.e{@link Establishment#locationId}
     */
    private getLastVisits(locationId: any): void {
        this._subscriptions.push(this._visitApi.find(this._establishmentHelper.getFilter(locationId)).subscribe(
            (visits: Array<Visit>) => {
                this.visits = [];
                this.visitsThereNow = [];
                this.visitCount = uniqBy(visits, 'accountId').length;
                visits = uniqBy(visits, 'accountId');
                this.isUserLive = false;
                visits.forEach((visit) => {
                    if (visit.entry && visit.entry.toString() != this._invalideDate && (!visit.exit || visit.exit.toString() == this._invalideDate)) {
                        if (visit.accountId == this._accountApi.getCurrentId())
                            this.isUserLive = true;
                        this.visitsThereNow.push(visit)
                    } else {
                        this.visits.push(visit)
                    }
                })
                //this.visits[1].visible = 1;
                // Filter out the visible i.e "There now"
                /*Observable.from(this.visits)
                    .filter(item => item.visible == 1)
                    .toArray()
                    .subscribe(items => {
                        this.visitsThereNow = items;
                    })*/
                //this._loader.instance.hide();
                this.isDataLoaded = true
            }, this.error
        ));
    }

    public onTapForUserList(type: string): void {
        if (type != 'Cancel') {
            if (type == 'Visited') {
                this.listItems = this.visits;
            } else {
                this.listItems = this.visitsThereNow;
            }
            if (this.listItems == null || this.listItems.length < 1) return;
            this.isInListMode = true;
            this.listMode = type;
        } else {
            this.isInListMode = false;
            this.listMode = "";
            this.listItems = this.visits;
        }

    }

    public onVisitItemTap(visit: Visit) {
        console.log(this._TAG, visit.account.displayName);
    }

    public onTapCheckIn(): void {
        if (this.isUserLive) return;
        console.log("userRadius -> " + this._userRadius);
        if (this._userRadius != -1 && this._userRadius <= 100) {
            this._routerExtensions.navigate([
                '/post'
            ], {
                    queryParams: {
                        "establishment_id": this._establishment.id,
                    },
                    transition: {
                        name: 'slideLeft',
                        duration: 500,
                        curve: 'linear'
                    }
                });
        } else {
            dialogs.alert({
                message: "Looks like you're too far away...Check-in once you get there!",
                okButtonText: "Close"
            })
        }
    }

    /**
     * @method error
     * @param {SwipeGestureEventData} args
     * @return {void}
     */
    onSwipe(args: SwipeGestureEventData): void {
        if (args.direction == SwipeDirection.right) {
            this._routerExtensions.back();
        }
    }

    error(error: Error) {
        // Hide the loading indicator incase of error
        //this._loader.instance.hide();
        console.error('Error: ', error.message);
    }

    public getAccountDetail(id) {
        console.log(id);
        this._routerExtensions.navigate(["/user-profile/" + id]);
    }

    watch(): void {
        //this._loader.instance.show();
        // This will mainly happen on navigation, because the location subscription already exists
        // But the map was wiped, so at this stage watching is really triggering a fist time
        // So the map can be populated, this only happens the second time watch is executed.
        if (this.locationSubscription) {
            this.locationSubscription.unsubscribe();
            console.log('AVOIDING LOCATION SUBSCRIPTION (POTENTIAL DUPLICATED EVENT)');
        }
        //this.watchingEvents = true;
        this.locationSubscription = this.locationSubject.subscribe((position: geolocation.Location) => {
            console.log(this._TAG, "latitude: " + position.latitude + " longitude: " + position.longitude);
        });
        this.watchLocation();
    }

    watchLocation(): void {
        // Default options
        let options: any = {
            desiredAccuracy: Accuracy.high,
            updateDistance: 5,
            minimumUpdateTime: 5000,
            iosAllowsBackgroundLocationUpdates: true,
            iosPausesLocationUpdatesAutomatically: false
        };

        // If there is a user position, lets use that to change update distance and min update time
        if (this._around && this._around.speed && this._around.horizontalAccuracy) {
            options.updateDistance = this._around.horizontalAccuracy < 20 ? 5 :
                this._around.horizontalAccuracy < 50 ? 20 :
                    this._around.horizontalAccuracy > 50 ? 40 : 50;
            options.minimumUpdateTime = this._around.speed === 0 ? 10000 :
                this._around.speed < 5 ? 6000 :
                    this._around.speed < 10 ? 3000 :
                        this._around.speed > 10 ? 500 : 5000;
        }
        if (this._watchId) {
            geolocation.clearWatch(this._watchId);
        }
        let that = this;
        console.log('\x1b[33mSETTING UP WATCH LOCATION\x1b[0m');
        this._watchId = geolocation.watchLocation(
            (position: geolocation.Location) => {
                that._loader.instance.hide();
                console.log('\x1b[33mVISIT DETECTOR - GOT LOCATION FROM DEVICE\x1b[0m -> ' + position.latitude);
                if (!this._establishment || !this._establishment.geoLocation) return;
                let establishmentPosition = new Position();
                establishmentPosition.latitude = this._establishment.geoLocation.coordinates[1];
                establishmentPosition.longitude = this._establishment.geoLocation.coordinates[0];

                let distance = this.calculateDistance(position, establishmentPosition);
                console.log(this._TAG, "Distance is -> " + distance);
                this._userRadius = distance;

                this._around = position;
                if (
                    (this._around &&
                        (
                            (this._around.horizontalAccuracy - position.horizontalAccuracy) > 10 ||
                            (this._around.horizontalAccuracy - position.horizontalAccuracy) < -10
                        )
                        ||
                        (
                            (this._around.speed - position.speed) > 3 ||
                            (this._around.speed - position.speed) < -3
                        )
                    )
                ) {
                    //this.visitDetectionService.newUserPosition(position, true);
                    this.watchLocation(); // Run again to use new settings
                } else {
                    // console.log('\x1b[33mUser is not Moving\x1b[0m');
                    //this.visitDetectionService.newUserPosition(position, false);
                }
            },
            (err: Error) => {
                console.log('VISIT DETECTOR - GEOLOCATION ERROR:', err)
            },
            options
        )
    }

    /**
     * @calculateDistance
     * @param {Position} a Location A
     * @param {Position} b Location B
     * @return {number} Delta distance between location A and B
     **/
    calculateDistance(a: Position, b: Position): number {
        let aLocation = new geolocation.Location();
        let bLocation = new geolocation.Location();
        aLocation.latitude = a.latitude || 0;
        aLocation.longitude = a.longitude || 0;
        bLocation.latitude = b.latitude || 0;
        bLocation.longitude = b.longitude || 0;
        console.log(this._TAG, "aLocation lat -> " + aLocation.latitude + " lng -> " + aLocation.longitude);
        console.log(this._TAG, "bLocation lat -> " + bLocation.latitude + " lng -> " + bLocation.longitude);
        return geolocation.distance(bLocation, aLocation);
    }
}
