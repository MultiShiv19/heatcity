import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth-guard.service';
import { EstablishmentProfileComponent } from './establishment-profile.component';

const establishmentProfileRoutes: Routes = [
    // Changed by Sandip to avoid conflicts with establishment module
    // I have just added "_" at the end of path.
    { path: 'establishment_/:id', component: EstablishmentProfileComponent, canActivate: [AuthGuard] }
];

export const establishmentProfileRouting: ModuleWithProviders = RouterModule.forChild(establishmentProfileRoutes);
