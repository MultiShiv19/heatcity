import { Component, OnInit, OnDestroy, ViewContainerRef, ViewChild, NgZone } from '@angular/core';
import geolocation = require('nativescript-geolocation');
import { Router } from '@angular/router';
import { Page } from 'ui/page';
import { Subscription } from 'rxjs/Subscription';
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import * as moment from 'moment';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/directives/dialogs";
import { ObservableArray } from "data/observable-array";
import { StorageNative } from '../shared/sdk/storage/storage.native';
import { Position } from 'nativescript-google-maps-sdk';
import { isIOS } from "platform";
import * as application from "application";
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import { SwipeGestureEventData } from "ui/gestures";
import { ScrollView } from 'ui/scroll-view';
import { StackLayout } from 'ui/layouts/stack-layout';
import { Label } from 'ui/label';
import { Color } from 'color';
import { AnimationCurve } from "ui/enums";

import { View } from 'ui/core/view';
import { BASE_URL, API_VERSION } from '../shared/base.api';
import { LoopBackConfig } from '../shared/sdk';
import { Establishment, Visit, LoopBackFilter } from '../shared/sdk/models';
import { EstablishmentApi, VisitApi, AccountApi } from '../shared/sdk/services';
import { GalleryComponent } from './gallery/gallery.component';
import { StatsInterface } from '../shared/interfaces';
import { RadCartesianChart } from "nativescript-pro-ui/chart"
const kebabCase = require('lodash/kebabCase');
const deburr = require('lodash/deburr');
const uniqBy = require('lodash/uniqBy');
import * as googleAnalytics from "nativescript-google-analytics";

// added by Solulab on 17th August 2017
import { RadListViewComponent } from "nativescript-pro-ui/listview/angular";
import { RadListView } from 'nativescript-pro-ui/listview';
// added by Solulab on 17th August 2017
// 
//TODO DELETE THIS AFTER BACKEND INTEGRATION...USE ONLY FOR TEST
class Country {
    constructor(public name: string) { }
}

let europianCountries = ["Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus", "Czech Republic",
    "Denmark", "Estonia", "Finland", "France", "Germany", "Greece", "Hungary", "Ireland", "Italy",
    "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland", "Portugal", "Romania", "Slovakia",
    "Slovenia", "Spain", "Sweden", "United Kingdom"];
//////////////////////////////////////////////////////


@Component({
    moduleId: module.id,
    selector: 'establishment-profile',
    providers: [ModalDialogService],
    templateUrl: 'establishment-profile.component.html',
    styleUrls: ['establishment-profile-common.css', 'establishment-profile.component.css']
})
export class EstablishmentProfileComponent implements OnInit, OnDestroy {
    /** @type {Number} **/
    public visitCount: number = 0;

    /** @type {ObservableArray} **/
    private ageRanges: ObservableArray<any>;

    /** @type {ObservableArray} **/
    private heatHours: Array<any>;

    /** @type {Array<string>} **/
    private days: Array<string> = [
        'SUN',
        'MON',
        'TUE',
        'WED',
        'THU',
        'FRI',
        'SAT'
    ]

    /** @type {Number} **/
    private stats: StatsInterface;

    /** @type {Boolean} **/
    private displayScrollView: boolean = false;

    /** @type {Number} **/
    private dayOfWeek: number = 0;

    /** @type {Establishment} **/
    private establishment: Establishment = new Establishment();

    /** @type {Array<Subscription>} **/
    private subscriptions: Array<Subscription> = [];

    /** @type {Array<Visit>} **/
    public visits: Array<Visit> = [];
    
    public visitsThereNow: Array<Visit> = [];

    public share: { enabled: boolean } = { enabled: false };

    ///////TODO REMOVE. JUST FOR TEST///////
    public countries: Array<Country>;


    /**
     * @method constructor
     * @param {Page} page NativeScript Page
     * @param {ModalDialogService} modalService NativeScript Modal Service
     * @param {ViewContainerRef} vcRef Angular Core
     * @param {EstablishmentApi} establishmentApi FireLoop Service
     * @param {VisitApi} visitApi FireLoop Service
     * @param {RouterExtensions} router Nativescript RouterExtensions
     * @param {StorageNative} storage Nativescript Local Storage
     **/
    constructor(
        private page: Page,
        private modalService: ModalDialogService,
        private vcRef: ViewContainerRef,
        private establishmentApi: EstablishmentApi,
        private pageRoute: PageRoute,
        private visitApi: VisitApi,
        private routerExtensions: RouterExtensions,
        private storage: StorageNative,
        private router: Router,
        private zone: NgZone
    ) {
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        this.page.actionBarHidden = true;
        this.dayOfWeek = moment().weekday();
        if (!isIOS) {
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                if (this.router.isActive("/establishment", false)) {
                    if (this.share.enabled) {
                        data.cancel = true;
                        this.zone.run(() => this.share.enabled = false);
                    } else {
                        console.log('going back!');
                    }
                }
            });

            ///////TODO REMOVE. JUST FOR TEST///////
            this.countries = [];
            for (let i = 0; i < europianCountries.length; i++) {
                this.countries.push(new Country(europianCountries[i]));
            }
            /////////////////////////////////////////
        }
    }
    /**
     * @method ngOnInit
     * @return {void}
     */
    ngOnInit() {
        this.getUrlParam();
    }
    // added by Keyur Mistry on 17th August 2017
    @ViewChild(RadListViewComponent) public listviewComponent: RadListViewComponent;
    // added by Keyur Mistry on 17th August 2017

    /**
     * @method ngOnDestroy
     * @return {void}
     */
    ngOnDestroy() {
        this.subscriptions.forEach((subscription) => {
            subscription.unsubscribe();
        })
    }
    /**
     * @method getEstablishment
     * @param {string} establishmentId
     * @return {void}
     */
    getEstablishment(establishmentId: string): void {
        let filter: LoopBackFilter = {
            include: ['photos', 'location']
        }
        this.subscriptions.push(
            this.establishmentApi.findById(establishmentId, filter)
                .subscribe((establishment: Establishment) => {
                    this.getLatestVisits(establishment.locationId);
                    this.establishment = establishment;
                    //console.log("this.establishment");
                    //console.dir(this.establishment);
                    this.getStats(this.establishment.id);
                    //this.getVisitsCount(this.establishment.locationId);
                    
                    let name = `establishment-profile-${this.formatString(establishment.name)}-${this.formatString(establishment.address)}`
                    googleAnalytics.logView(name);
                },
                this.error
                )
        );
    }
    /**
     * @method getStats
     * @param {string} establishmentId
     * @return {void}
     */
    getStats(establishmentId: string): void {
        this.subscriptions.push(
            this.establishmentApi.statistics(establishmentId)
                .subscribe(
                (stats: StatsInterface) => {
                    this.stats = stats;
                    console.log("this.stats");
                    //console.dir(this.stats);
                    if (this.stats && this.stats.heatHours) {
                        this.stats.heatHours = this.stats.heatHours
                            .map((heat: Array<any>) => new ObservableArray(heat))
                        this.disableGraphInteraction();
                    }
                    if (this.stats && this.stats.ages) {
                        this.stats.ages = new ObservableArray(this.stats.ages);
                    }
                },
                this.error
                )
        )
    }

    /**
     * @method disableGraphInteraction
     * @return {void}
     */
    disableGraphInteraction(): void {
        if (!isIOS) {
            return;
        }
        setTimeout(() => {
            this.days.forEach((day) => {
                let element = this.page.getViewById<RadCartesianChart>(day.toLowerCase());
                if (element) {
                    console.log('exists');
                    element.ios.userInteractionEnabled = false;
                } else {
                    console.log('nel exists');
                }
            });
        }, 100);
    }
    /**
     * @method getVisitsCount
     * @param {string} locationId
     * @return {void}
     */
    getVisitsCount(locationId: string): void {
        let today = moment().toDate()
        let sevenDaysBefore = moment().subtract(7, 'days').toDate();
        let where: LoopBackFilter = {
            where: {
                and: [
                    { locationId: locationId },
                    { validity: 100 },
                    {
                        createdAt: {
                            between: [
                                sevenDaysBefore,
                                today
                            ]
                        }
                    }
                ]

            }
        }
        console.log('filter', JSON.stringify(where))
        this.subscriptions.push(
            this.visitApi.find(where).subscribe(
                (visits: Array<Visit>) => {
                    this.visitCount = uniqBy(visits, 'accountId').length;
                    console.log("getVisitsCount");
                    console.dir(visits);
                },
                this.error
            )
        );
    }
    /**
     * @method getUrlParam
     * @return {void}
     */
    goToProfile(accountId: string): void {
        this.routerExtensions.navigate(['/user-profile', accountId]);
    }
    /**
     * @method getUrlParam
     * @return {void}
     */
    getUrlParam(): void {
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                let id = params['id'];
                this.getEstablishment(id);
            })
    }
    /**
     * @method getLatestVisits
     * @param {string} locationId
     * @return {void}
     */
    getLatestVisits(locationId: string) {
        let today = moment().toDate()
        let sevenDaysBefore = moment().subtract(700, 'days').toDate();
        let filter: LoopBackFilter = {
            where: {
                and: [
                    { locationId: locationId },
                    { validity: 100 },
                    {
                        createdAt: {
                            between: [
                                sevenDaysBefore,
                                today
                            ]
                        }
                    }
                ]
            },
            include: ['account'],
            order: 'createdAt DESC',
            limit: 5
        }
        
        console.log('+++++++++++++++++++++++++++++++++++++++++getLatestVisits filter', JSON.stringify(filter))
        this.subscriptions.push(
            this.visitApi.find(filter).subscribe(
                (visits: Array<Visit>) => {
                    console.log("getLatestVisits");
                    this.visitCount = uniqBy(visits, 'accountId').length;
                    this.visits = uniqBy(visits, 'accountId');
                    
//                    this.visitsThereNow.push(visits.filter(function(itm){
//                        return itm.visible == 1;
//                    }));
                    //console.dir(this.visits);
//                    console.log("this.visitCount START");
//                    console.dir(this.visitsThereNow);
//                    console.log(this.visitCount);
//                    console.log("this.visitCount END");
                }
            )
        )
    }
    /**
     * @method displayGallery
     * @param {Number} index
     * @return {void}
     */
    displayGallery(index: number): void {
        let self = this;
        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: {
                index: index,
                establishment: this.establishment
            },
            fullscreen: true
        }
        this.modalService.showModal(GalleryComponent, options)
            .then(() => {
                console.log('closed');
            })
    }
    // .....Edited By Gopal....START.....

    /** @type {Boolean} **/
    private isVisitView: boolean = false;

    onTapForUserList(optionTxt: string): void {
        console.log('Share Option Tap : ' + optionTxt);
        let userListView = <View>this.page.getViewById('user-list-view');
        let usersLabelTxt = <Label>this.page.getViewById('users-label-text');

        if (optionTxt == "Visited") {
            usersLabelTxt.text = "Visited"
            userListView.visibility = 'visible';

        } else if (optionTxt == "ThereNow") {
            usersLabelTxt.text = "There Now"
            userListView.visibility = 'visible';

        } else if (optionTxt == "Cancel") {
            userListView.visibility = 'collapse';
        }

    }

    onTapCheckIn(): void {
        this.showShareDialog();
    }

    onTapShareOptions(optionTxt: string): void {
        console.log('Share Option Tap : ' + optionTxt);
        let checkInTick = <View>this.page.getViewById('check-in-tick');
        if (optionTxt !== "Cancel") {
            checkInTick.color = new Color("#FF1493");
        } else {
            checkInTick.color = new Color("#FFFAFA");
        }
        //this.submitCheckinData();
        this.hideShareDialog();
    }

    /*submitCheckinData(): void {
        console.log("Submit form data to the ");
        let checkInTick = <View>this.page.getViewById('check-in-tick');
        console.log(checkInTick);
    }*/

    /**
     * @method showShareDialog
     * @return {void}
     */
    showShareDialog(): void {
        console.log('Check-In Taped....Show Dialog');
        let screen = this.page.getActualSize();
        let shareDialogBox = <View>this.page.getViewById('share-dialog-view');
        let yPos = screen.height / 60;
        shareDialogBox.visibility = 'visible';
        if (shareDialogBox) {
            shareDialogBox.translateY = yPos;
            shareDialogBox.animate({
                translate: { x: 0, y: yPos },
                duration: 500,
                curve: AnimationCurve.linear
            })
        }
    }
    /**
     * @method hideShareDialog
     * @return {void}
     */
    hideShareDialog(): void {
        console.log('Check-In Taped....Hide Dialog');
        let shareDialogBox = <View>this.page.getViewById('share-dialog-view');
        if (shareDialogBox) {
            let screen = this.page.getActualSize();
            shareDialogBox.animate({
                translate: { x: 0, y: screen.height / 60 },
                duration: 500,
                curve: AnimationCurve.linear
            }).then(() => {
                shareDialogBox.visibility = 'collapse';
            })
        }
    }

    // .....Edited By Gopal....END.......


    /**
     * @method nextScreen
     * @return {void}
     */
    nextScreen(): void {
        this.showScrollView()
    }

    formatString(text: string): string {
        return kebabCase(deburr(text));
    }

    /**
     * @method setCarouselToCurrentDay
     * @param {number} dayOfWeek
     * @return {void}
     */
    setCarouselToCurrentDay(dayOfWeek: number): void {
        let carousel = this.page.getViewById<any>('carousel');
        if (carousel) {
            carousel.selectedPage = dayOfWeek;
        }
    }

    /**
     * @method showScrollView
     * @return {void}
     */
    showScrollView(): void {
        let screen = this.page.getActualSize();
        let scrollView = <View>this.page.getViewById('scroll-view');
        this.displayScrollView = true;
        if (scrollView) {
            scrollView.translateY = screen.height;
            scrollView.animate({
                translate: { x: 0, y: 0 },
                duration: 500,
                curve: AnimationCurve.easeOut
            })
        }
    }
    /**
     * @method hideScrollView
     * @return {void}
     */
    hideScrollView(): void {
        let scrollView = <View>this.page.getViewById('scroll-view');
        if (scrollView) {
            let screen = this.page.getActualSize();
            scrollView.animate({
                translate: { x: 0, y: screen.height },
                duration: 500,
                curve: AnimationCurve.easeOut
            }).then(() => {
                this.displayScrollView = false;
            })
        }
    }
    /**
     * @method previousScreen
     * @return {void}
     */
    previousScreen(): void {
        if (this.displayScrollView) {
            this.hideScrollView();
        } else {
            this.goToPreviousRoute();
        }
    }
    /**
     * @method goToMap
     * @return {void}
     */
    goToMap() {
        this.routerExtensions.navigate(['/map', {
            latitude: this.establishment.geoLocation.coordinates[1],
            longitude: this.establishment.geoLocation.coordinates[0],
            centerEstablishment: true,
            establishment: JSON.stringify(this.establishment)
        }]);
    }
    /**
     * @method goToPreviousRoute
     * @return {void}
     */
    goToPreviousRoute() {
        /*
        let location = new geolocation.Location();
        location.latitude = this.establishment.location.location.lat;
        location.longitude = this.establishment.location.location.lng;
        this.storage.set('current', location);
        this.storage.set('loadCurrent', true);
        */
        this.routerExtensions.back();
    }
    /**
     * @method onCarouselPageChange
     * @param {any} args
     * @return {void}
     */
    onCarouselPageChange(args: any): void {
        this.dayOfWeek = args.index;
    }

    openShare() {
        this.share.enabled = true;
    }

    closeShare() {
        this.share.enabled = false
    }

    /**
     * @method error
     * @param {SwipeGestureEventData} args
     * @return {void}
     */
    onSwipe(args: SwipeGestureEventData): void {
        let scrollView = this.page.getViewById<ScrollView>('scroll-view');
        if (args.direction === 4 && !this.displayScrollView) {
            this.nextScreen();
        } else if (
            args.direction === 8 && this.displayScrollView
            && (scrollView.verticalOffset <= 3)
        ) {
            this.previousScreen();
        } else if (args.direction === 8 && !this.displayScrollView) {
            this.goToPreviousRoute();
        }
    }
    /**
     * @method error
     * @param {Error} error
     * @return {void}
     */
    error(error: Error) {
        console.log('Error: ', error.message);
    }
    /**
     * @method getBiggestHeat
     * @return {number}
     */
    getBiggestHeat(index: number): number {
        /*
        let biggest= this.stats.heatHours[index].reduce((biggest, heat) => {
            return (heat.usersCount > biggest) ? heat.usersCount : biggest;
        }, 0)
        biggest += 1.2;
        return biggest;
        */
        return 3;
    }
    /**
     * @method getBiggestAge
     * @return {number}
     */
    getBiggestAge(): number {
        let biggest = this.stats.ages.reduce((biggest, age) => {
            return (age.ageCount > biggest) ? age.ageCount : biggest;
        }, 0)
        biggest += 4;
        return biggest;
    }
}
