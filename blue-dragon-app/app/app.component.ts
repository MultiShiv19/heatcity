import { Component, NgZone, OnDestroy, OnInit } from "@angular/core";
import * as app from "application";
import { Page } from "ui/page";
import { topmost } from "ui/frame";
import { AccessToken, Notification } from './shared/sdk/models';
import { LoopBackAuth, RealTime, NotificationApi, AccountApi } from './shared/sdk/services';
import { registerElement } from 'nativescript-angular/element-registry';
import geolocation = require('nativescript-geolocation');
import * as firebase from "nativescript-plugin-firebase";
import { StorageNative } from './shared/sdk/storage/storage.native';
import connectivity = require("connectivity");
import { DisconnectionService } from './disconnection/disconnection.service';
import { NotificationsService } from './notifications/notifications.service';
const orientationModule = require("nativescript-screen-orientation");
import { RouterExtensions } from "nativescript-angular/router";
import { AppNotificationsService } from "./app-notifications/app-notifications.service";

import { isIOS, isAndroid } from "platform";
import timer = require('timer');
import { LoopBackConfig } from './shared/sdk';
import { BASE_URL, API_VERSION } from './shared/base.api';
import { BottomBar, BottomBarItem, TITLE_STATE, SelectedIndexChangedEventData } from 'nativescript-bottombar';
import { Subscription } from "rxjs";
import { BackgroundGeolocation } from "nativescript-background-geolocation-lt";
import { BackgroundFetch } from "nativescript-background-fetch";
import { VisitDetectionService } from "./map/visit-detection/visit.detection.service";
import { AuthGuard } from "./auth-guard.service";
import { Background } from "tns-core-modules/ui/styling/background";
declare var GMSServices;
declare var com: any;
declare var FBSDKLoginManager: any;

declare var UIImage: any;
declare var UIBarMetrics: any;

registerElement('MapView', () => require('nativescript-google-maps-sdk').MapView);

@Component({
    selector: "app-main",
    templateUrl: "app.component.html",
})
export class AppComponent implements OnDestroy, OnInit {

    public subscription: Subscription;

    public userid = "";
    constructor(
        private page: Page,
        private auth: LoopBackAuth,
        private storage: StorageNative,
        private zone: NgZone,
        private disconnectionService: DisconnectionService,
        private notificationsService: NotificationsService,
        private routerextensions: RouterExtensions,
        private notificationApi: NotificationApi,
        private accountApi: AccountApi,
        private appNotificationsService: AppNotificationsService,
        private visitDetectionService: VisitDetectionService,
        private authGuard: AuthGuard,
    ) {
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        this.watchConnection();
        this.firebaseInit();

        // If IOS Set GMaps Token, for Android is set in values xml
        if (app.ios) {
            GMSServices.provideAPIKey('AIzaSyAaGsoyNTNWIgJJwCUn9sagg3bvyvQbRfc');
            //topmost().ios.controller.navigationBar.barStyle = 1;
        }
        // Enable GeoLocation
        if (!geolocation.isEnabled()) {
            geolocation.enableLocationRequest(true).then();
        }
        // Force Orientation to LandScape (Only android for now)
        if (app.android) {
            orientationModule.setCurrentOrientation('portrait', () => {
                console.log("Portrait orientation set");
            });
        }
        this.closeHangingVisits();
    }

    ngOnInit(): void {

        this.userid = this.accountApi.getCurrentId();
        let connection = connectivity.getConnectionType();
        if (app.ios) {
            //topmost().ios.controller.navigationBar.barStyle = 1;
            this.page.backgroundColor = "black";
            this.page.backgroundSpanUnderStatusBar = true;
            var navigationBar = topmost().ios.controller.navigationBar;
            this.page.actionBar.flat = true;
            navigationBar.translucent = false;
            navigationBar.setBackgroundImageForBarMetrics(UIImage.new(), UIBarMetrics.Default);
            navigationBar.shadowImage = UIImage.new();
        }
        console.log('connection ', connection, connectivity.connectionType.none);

        if (connection === connectivity.connectionType.none) {
            console.log('here')
            // this.routerextensions.navigate(['/disconnect'], {
            //     clearHistory: true
            // });
            // this.disconnectionService.setConnectionStatus(false);
        }
        this.islogin();
        this.authGuard.loginAccount(this.userid);
        // this.disconnectionService.setConnectionStatus(true);
    }

    watchConnection(): void {
        connectivity.startMonitoring((connectionType: number) => {
            this.zone.run(() => {
                console.log("**************" + connectionType);
                let isConnected = connectionType === connectivity.connectionType.none ?
                    false : true;
                this.disconnectionService.setConnectionStatus(isConnected);
                if (isConnected) {
                    this.routerextensions.navigate(['/posts'], {
                        clearHistory: true
                    });
                }

                /*switch (connectionType) {
                    case connectivity.connectionType.none:
                        console.log("No connection");
                        // this.routerextensions.navigate(['/disconnect'], {
                        //     clearHistory: true
                        // });
                        this.disconnectionService.setConnectionStatus(false);

                        break;
                    case connectivity.connectionType.wifi:
                        console.log("WiFi connection");
                        // alert(this.routerextensions.router.url);
                        this.disconnectionService.setConnectionStatus(true);
                        // this.routerextensions.navigate(['/posts'], {
                        //     clearHistory: true
                        // });
                        break;
                    case connectivity.connectionType.mobile:
                        console.log("Mobile connection");
                        // this.routerextensions.navigate(['/posts'], {
                        //     clearHistory: true
                        // });
                        this.disconnectionService.setConnectionStatus(true);
                        break;
                }*/
            })
        })
    }

    ngOnDestroy(): void {
        connectivity.stopMonitoring();
    }

    public firebaseInit() {
        firebase.init({
            iOSEmulatorFlush: true,
            onPushTokenReceivedCallback: (token) => this.listenToPushTokenReceived(token),
            onMessageReceivedCallback: (message) => this.listenToPushMessageReceived(message)
        }).then(
            instance => {
                console.log('firebase init done');
            },
            error => {
                console.log('firebase init error: ', error);
            }
            );
    }

    listenToPushTokenReceived(token): void {
        console.log('Firebase token: ', token);
        this.storage.set('deviceToken', token);
    }


    listenToPushMessageReceived(message): void {
        if (isAndroid) {
            console.log('FIREBASE MESSAGE: ' + JSON.stringify(message));
        }

        console.dir(message);
        // console.log("-----------=========================================-----------")
        if (isIOS) {
            console.log("ios notification working");

        }
        message.type = message.type || message && message.data && message.data.type ? message.data.type : '';
        if (message && message.instance) {
            message.instance = this.isJSON(message.instance) ? JSON.parse(message.instance) : message.instance;
            timer.setTimeout(() => {
                if (message.foreground == true) {
                    // console.log("In Visit foreground" + JSON.stringify(message.instance));
                    this.notificationsService.addNewNotification(message);
                } else {
                    this.zone.run(() => {
                        // console.log("*****************************************************");
                        // console.log("Background Notifications comeing....");

                        this.redirectNotication(message);
                    });
                }
            }, 1000);
        }
    }

    public redirectNotication(message) {
        switch (message.type) {
            case 'VISIT':
                // console.log("In Visit" + JSON.stringify(message));
                const data = JSON.stringify({ type: 'self' });
                this.routerextensions.navigate(['/posts', { data: data }]);
                break;
            // this.notificationApi.updateAttributes(message.instance.id, { viewed: true }).subscribe(
            //     (notification: Notification) => {
            //         let data = { visitNotification: JSON.stringify(message) };
            //         this.routerextensions.navigate(['/map', data])
            //     },
            //     (err: Error) => console.log(err.message)
            // );
            case 'ESTABLISHMENT_SHARE':
                if (message.instance.establishmentId)
                    this.routerextensions.navigate(['/establishment', message.instance.establishmentId]);
                break;
            case 'FRIEND_VISIT':
                const data1 = JSON.stringify({ type: 'following' });
                this.routerextensions.navigate(['/posts', { data: data1 }]);
                // if (message.instance.establishmentId)
                // this.routerextensions.navigate(['/establishment', message.instance.establishmentId]);
                break;
            case 'FRIEND_RESPONSE':
                // this.routerextensions.navigate(['/user-profile', message.instance.account.id]);
                this.routerextensions.navigate(['/notification']);
                break;
            case 'FRIEND_REQUEST':
                this.routerextensions.navigate(['/notification']);
                // this.notificationsService.pushNotification(message);
                break;
            case 'COMMENT':
            case 'LIKES':
            case 'LIKE':
                if (message.instance.visitId)
                    this.routerextensions.navigate(['/comment', message.instance.visitId]);
                break;
            default:
                console.log('Invalid notification type!');
                break;
        }
    }

    closeHangingVisits() {
        let isLogged = this.accountApi.isAuthenticated();
        if (isLogged) {
            let id = this.accountApi.getCurrentId();
            this.accountApi.closeHanginVisits(id).subscribe(
                res => console.log('Successfully closed hanging visits of : ', id),
                err => console.log(err)
            );
        } else {
            console.log('NOT LOGGED IN');
        }
    }

    private isJSON(data: string) {
        try {
            JSON.parse(data);
        } catch (e) {
            return false;
        }
        return true;
    }

    private islogin() {

        this.authGuard.onLoginAccount().subscribe((res) => {
            const accountId = this.userid ? this.userid : res.accountId;
            if (accountId) {
                this.startGeoLocation();
            } else {
                BackgroundGeolocation.stop();
            }
        })
    }

    private startGeoLocation(): void {
        BackgroundGeolocation.on("location", this.onLocation.bind(this));

        // BackgroundGeolocation.on('heartbeat', (params) => {
        //     var lastKnownLocation = params.location;
        //     console.log('- heartbeat: ', lastKnownLocation);
        //     // Or you could request a new location
        //     this.onHeartBeat();
        //     BackgroundGeolocation.getCurrentPosition(function(location) {
        //       console.log('- HEARTBEAT current position: ', location);
        //     });
        //   });

        // 2. Configure it.
        BackgroundGeolocation.configure({
            desiredAccuracy: 0,
            distanceFilter: 50,
            // activityRecognitionInterval: 10000,
            //useSignificantChangesOnly: true,
            //heartbeatInterval: 60,
            //preventSuspend: true,
            //stopOnTerminate: false,
            // autoSync: true,

            stopOnTerminate: false,
            stopOnStationary: false,
            stopTimeout: 100,
            stationaryRadius: 10,
            activityRecognitionInterval: 10,
            // pausesLocationUpdatesAutomatically:false,


            preventSuspend: true,
            // heartbeatInterval:10

        }, (state) => {
            // 3. Plugin is now ready to use.
            // alert("state: " + state.stopOnTerminate);
            //state.stopOnTerminate = false;
            //console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa => " + (state.stopOnTerminate));
            if (!state.enabled) {
                BackgroundGeolocation.start();
            }
            //Add a comment to this line
        });



        // Use #setConfig if you need to change options after you've executed #configure

        /*  BackgroundGeolocation.setConfig({
             desiredAccuracy: 0,
             distanceFilter: 10
         }, function () {
             console.log('set config success-----------------------------');
         }, function () {
             console.log('failed to setConfig');
         }); */

    }

    private onHeartBeat() {
        console.log("ONHEARTBEAT CALLED");
    }
    private onLocation(location: any) {
        if (!location || !location.coords) {
            return;
        }
        this.visitDetectionService.newUserCurrentPosition(location);
        console.log("$$$$$$$$$$$$$$$$$$$$$$$  LOCATION --> " + JSON.stringify(location));
        BackgroundGeolocation.logger.debug("$$$$$$$$$$$$$$$$$$$$$$$  LOCATION --> " + JSON.stringify(location));

    }
}
