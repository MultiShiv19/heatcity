import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, SimpleChange, OnChanges } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AccountApi, RealTime, FriendApi, NotificationApi } from '../shared/sdk/services';
import { LoopBackConfig, LoopBackFilter } from '../shared/sdk';
import { BASE_URL, API_VERSION } from '../shared/base.api';
import { Friend, Establishment, Notification } from '../shared/sdk/models';
import { StorageNative } from '../shared/sdk/storage/storage.native';
import { NotificationsService } from './notifications.service';
import { ObservableArray } from 'data/observable-array';
import { RouterExtensions } from "nativescript-angular/router";
import * as moment from 'moment';
import { Page } from 'ui/page';
import * as googleAnalytics from "nativescript-google-analytics";
import { MenuInfoInterface } from '../shared/interfaces/menu-info.interface';
import { LogoutService } from '../shared/logout.service';
import { View } from 'ui/core/view';
import { BottomBar, BottomBarItem, TITLE_STATE, SelectedIndexChangedEventData } from 'nativescript-bottombar';
import * as frameModule from "ui/frame";
import { isIOS } from "platform";
import { LoadingIndicator } from "nativescript-loading-indicator";

declare var UITableViewCellSelectionStyle: any;

@Component({
    moduleId: module.id,
    selector: "NotificationsComponent",
    templateUrl: "notifications.component.html",
    styleUrls: ['notifications-common.css', 'notifications.component.css']
})
export class NotificationsComponent implements OnInit, OnDestroy, OnChanges {
    /** @type {Subscription[]} **/
    private subscriptions: Subscription[] = new Array<Subscription>();

    /** @type {Notification[]} **/
    private notifications: Notification[] = new Array<Notification>();

    private show: boolean = true;

    private showList: boolean = true;;
    private loader: {
        instance: LoadingIndicator,
        options: any,
        loading: boolean
    } = {
            loading: false,
            instance: new LoadingIndicator(),
            options: {
                message: 'Loading...',
                progress: 0,
                android: {
                    indeterminate: true,
                    cancelable: false,
                    max: 100,
                    progressNumberFormat: "%1d/%2d",
                    progressPercentFormat: 0.53,
                    progressStyle: 1,
                    secondaryProgress: 1
                },
                ios: {
                    square: false,
                    margin: 10,
                    dimBackground: true,
                    color: "#fff",
                    mode: 'MBProgressHUDModeText'// see iOS specific options below
                }
            }
        };

    @Input('showNotifications') showNotifications: boolean;

    @Output('close') onClose: EventEmitter<any> = new EventEmitter<any>();

    @Output('notificationCount') notificationsCount: EventEmitter<any> = new EventEmitter<any>();

    @Output('history') history: EventEmitter<Notification> = new EventEmitter<Notification>();

    /**
     * @method constructor
     * @param {AccountApi} accountApi Fireloop Account Service
     * @param {FriendApi} friendApi Fireloop Account Service
     * @param {NotificationService} ns NoficationsService Service
     * @param {StorageNative} storage NativeScript Local Storage
     **/
    constructor(
        private accountApi: AccountApi,
        private friendApi: FriendApi,
        private notificationApi: NotificationApi,
        private page: Page,
        private routerExtensions: RouterExtensions,
        private logoutService: LogoutService,
        private notificationService: NotificationsService
    ) {


        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        googleAnalytics.logView('notifications-list');
        // this.page.on('navigatedTo', () => {
        //     if (this.showList == false) {
        //         this.getNotifications();
        //         this.showList = true;
        //     }
        // });

        this.subscriptions.push(
            this.notificationService.onNewNotification().subscribe(
                (notification: any) => {
                    this.getNotifications();
                }
            )
        )
    }

    /**
     * @method ngOnInit
     **/
    ngOnInit() {

        this.getNotifications();
        /**  This 3 line are Hide Back Button From ActionBar */
        if (isIOS) {
            const controller = frameModule.topmost().ios.controller;
            // get the view controller navigation item
            const navigationItem = controller.visibleViewController.navigationItem;
            // hide back button
            navigationItem.setHidesBackButtonAnimated(true, false);
        }

    }

    /**
     * @method ngOnChanges
     **/
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['showNotifications']) {
            this.showNotifications = changes['showNotifications'].currentValue;
            if (this.showNotifications) {
                this.getNotifications();
                this.open();
            } else {
                this.close();
            }
        }
    }

    /**
     * @method getNotifications
     **/
    getNotifications(): void {
        this.loader.instance.show(this.loader.options);
        let currentId = this.accountApi.getCurrentId();
        this.subscriptions.push(
            this.accountApi.getNotifications(currentId, {
                order: 'createdAt DESC',
                limit: 20,
                include: ['owner', 'account', 'share', 'friend', 'visit', 'establishment']
            }).subscribe(
                (notifications) => {

                    this.notifications = notifications.filter((notification) => notification.account);
                    this.showList = true;
                    this.loader.instance.hide();
                    // this.countNotViewed();

                }, (error) => {
                    // alert(error.message);
                   this.errorHandler(error)
                    // console.error("Error While Getting Notifications -> " + error.message);
                    this.loader.instance.hide();
                }

                )
        );
    }

    /**
     * @method getNotifications
     **/
    countNotViewed(): void {
        let count: MenuInfoInterface = {
            newNotifications: 0,
            enableNotifications: true
        };
        this.notifications.forEach((notification: Notification) => {
            if (!notification.viewed) {
                count.newNotifications++;
            }
        });

        if (this.notifications && this.notifications.length === 0) {
            count.enableNotifications = false;
        }
        console.log('Count ! ', count.newNotifications);
        this.notificationsCount.emit(count);
    }

    /**
     * @method updateNotification
     **/
    updateNotification(notificationId: string, text?: string): void {
        let data: any = {
            viewed: true
        }
        if (text) {
            data.text = text;
        }
        this.subscriptions.push(
            this.notificationApi.updateAttributes(notificationId, data).subscribe(
                (notification: Notification) => this.getNotifications(),
                error => this.errorHandler(error),

            )
        )
    }

    /**
     * @method templateSelector
     **/
    templateSelector = (item: Notification, index: number, items: Notification[]) => {
        if (item && item.type) {
            return item.type;
        }
        return 'NO_ITEM';
    }

    /**
     * @method acceptFriendRequest
     **/
    acceptFriendRequest(notification: Notification): void {
        console.log(" acceptFriendRequest - this should not be called. check the code");
        let friend: Friend = notification.friend;
        if (!notification.viewed) {
            this.subscriptions.push(
                this.friendApi.updateAttributes(friend.id, {
                    approved: true
                }).subscribe(
                    //res => this.createFriend(friend.accountId, notification.id),
                    res => this.findFriend(friend.accountId, notification.id),
                    error => this.errorHandler(error)
                    )
            );
        }
    }

    findFriend(friendId: string, notificationId: string): void {
        let currentId = this.accountApi.getCurrentId();
        let filter: LoopBackFilter = {
            where: {
                accountId: currentId,
                friendId: friendId
            }
        }
        this.subscriptions.push(
            this.friendApi.find(filter).subscribe(
                (res: Array<Friend>) => {
                    if (res && res.length > 0) {
                        this.updateFriend(res[0].id, notificationId)
                    } else {
                        this.createFriend(friendId, notificationId)
                    }
                }, (error) => {
                    alert(error.message);
                    console.error("Error While Getting Friends -> " + error.message);
                    this.loader.instance.hide();
                }
            )
        )
    }

    updateFriend(id: string, notificationId: string): void {
        console.log("update friend");
        this.subscriptions.push(
            this.friendApi.updateAttributes(id, { approved: true }).subscribe(
                (friend) => {
                    alert('Friend request accepted');
                    this.updateNotification(notificationId);
                }, (error) => {
                    alert(error.message);
                    this.errorHandler(error)
                    console.error("Error While Updating Friend -> " + error.message);
                    this.loader.instance.hide();
                }
            )
        )
    }

    /**
     * @method createFriend
     **/
    createFriend(accountId: string, notificationId: string): void {
        console.log("create friend");
        let currentId = this.accountApi.getCurrentId();
        this.subscriptions.push(
            this.accountApi.linkFriends(currentId, accountId, { approved: true }).subscribe(
                res => {
                    alert('Friend request accepted');
                    this.updateNotification(notificationId);
                }, (error) => {
                    alert(error.message);
                    this.errorHandler(error)
                    console.error("Error While Creating Friends -> " + error.message);
                    this.loader.instance.hide();
                }
            )
        );
    }

    /**
     * @method openEstablishment
     **/
    openEstablishment(notification: Notification): void {
        let establishment = notification.establishment;
        // if (!notification.viewed) {
        //     this.updateNotification(notification.id);
        // }
        if (establishment && establishment.id) {
            this.showList = false;
            this.routerExtensions.navigate([
                '/establishment',
                establishment.id
            ]);
        }
    }

    openPost(notification: Notification): void {
        console.log(JSON.stringify(notification));
        let visit = notification.visitId;
        // if (!notification.viewed) {
        //     this.updateNotification(notification.id);
        // }
        if (visit) {
            this.showList = false;
            this.routerExtensions.navigate([
                '/comment', visit
            ], {
                    transition: {
                        name: 'slideLeft',
                        duration: 500,
                        curve: 'linear'
                    }
                });
        }
    }

    /**
     * @method goToFriendProfile
     **/
    goToFriendProfile(notification: Notification): void {
        let friendAccountId = notification.accountId;
        // if (!notification.viewed) {
        //     this.updateNotification(notification.id);
        // }
        if (friendAccountId) {
            this.showList = false;
            this.routerExtensions.navigate(['/user-profile/' + friendAccountId]);
        }
    }

    openHistory(notification: Notification) {
        // if (!notification.viewed && notification.visit && notification.visit.validity < 100) {
        //     this.updateNotification(notification.id);
            this.history.next(notification);
        // } else {
            this.openEstablishment(notification);
        // }
    }

    /**
     * @method open
     **/
    open(): void {
        this.show = true;
        let gridLayout = <View>this.page.getViewById('notifications-component');
        if (gridLayout) {
            gridLayout.opacity = 0;
            gridLayout.translateY = 200;
            gridLayout.animate({
                opacity: 1,
                translate: { x: 0, y: 0 },
                duration: 500
            });
        }
    }

    /**
     * @method close
     **/
    close() {
        let gridLayout = <View>this.page.getViewById('notifications-component');
        if (gridLayout) {
            gridLayout.animate({
                opacity: 0,
                translate: { x: 0, y: 600 },
                duration: 500
            }).then(() => {
                this.show = false;
            });
        }
    }

    /**
     * @method getTime
     **/
    getTime(date: string) {
        return moment(date).fromNow();
    }

    /**
     * @method ngOnDestroy
     **/
    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    errorHandler(error: any): void {
        console.log('Notifications error handler: ', JSON.stringify(error))
        if (error && error.statusCode == 401) {
            this.logoutService.logout();
        }
    }


    /**Bottom Bar */

    public onClickUser(args) {
        this.routerExtensions.navigate(['/user-profile/' + this.accountApi.getCurrentId()], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickPosts(args) {
        this.routerExtensions.navigate(['/posts'], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickDiscover(args) {
        this.routerExtensions.navigate(['/map'], {
            clearHistory: true,
            animated: false
        });
    }
    public onClickNotification(args) {
        this.routerExtensions.navigate(['/notification'], {
            clearHistory: true,
            animated: false
        });
    }

    public getAccountDetail(id) {
        this.routerExtensions.navigate(["/user-profile/" + id]);
    }

    public listViewItemLoading(args): void {
        if (isIOS) {
            args.ios.selectionStyle = UITableViewCellSelectionStyle.UITableViewCellSelectionStyleNone;
        }
    }
}
