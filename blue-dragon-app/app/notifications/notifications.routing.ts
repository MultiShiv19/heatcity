import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth-guard.service';
import { NotificationsComponent } from './notifications.component';

const mapRoutes: Routes = [
    { path: 'notification', component: NotificationsComponent, canActivate: [AuthGuard] },

];

export const notificationRouting: ModuleWithProviders = RouterModule.forChild(mapRoutes);
export const checkInNavigatableComponents = [
    NotificationsComponent
]