import { Component, OnInit, OnDestroy, Input, Output, OnChanges, SimpleChange, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AccountApi, FriendApi } from '../shared/sdk/services';
import { StorageNative } from '../shared/sdk/storage/storage.native';
import { LoopBackConfig } from '../shared/sdk';
import { BASE_URL, API_VERSION } from '../shared/base.api';
import { Account, Friend } from '../shared/sdk/models';
import { isIOS } from "platform";
import { ObservableArray } from 'data/observable-array';
import { Page}  from "ui/page";
import * as googleAnalytics from "nativescript-google-analytics";
import { LogoutService } from '../shared/logout.service';
import {View} from 'ui/core/view';

@Component({
    moduleId: module.id,
    selector: "FriendsListComponent",
    templateUrl: "friends-list.component.html",
    styleUrls: ['friends-list-common.css', 'friends-list.component.css']
})
export class FriendsListComponent implements OnInit, OnDestroy, OnChanges {
    /** @type {Subscription[]} **/
    private subscriptions: Subscription[] = new Array<Subscription>();

    /** @type {Subscription[]} **/
    private friends: Friend[] = [];

    private account: Account;

    @Input('show')show: boolean;

    @Input('accountId')accountId: string;

    @Output('close') onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

    private isIos: boolean;

    private showList: boolean = true;
    
    /**
     * @method constructor
     * @param {AccountApi} accountApi Fireloop Account Service
     * @param {FriendApi} friendApi Fireloop Friend Service
     **/
    constructor(
        private accountApi: AccountApi,
        private friendApi: FriendApi,
        private router: Router,
        private logoutService: LogoutService,
        private storage: StorageNative,
        private page: Page
    ) {
        LoopBackConfig.setBaseURL(BASE_URL);
        LoopBackConfig.setApiVersion(API_VERSION);
        googleAnalytics.logView('friend-list');
        this.isIos = isIOS;
        this.page.on('navigatedTo', () => {
            if(this.showList == false) {
                this.showList = true;
                this.getFriends();
            }
        });
    }

    /**
     * @method ngOnInit
     **/
    ngOnInit() {
        this.getAccount();
        this.getFriends();
    }

    /**
     * @method ngOnChanges
     **/
    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        if(changes['show']) {
            this.show = changes['show'].currentValue;
            if(this.show) {
                this.getAccount();
                this.getFriends();
            }
        }
        
        if(changes['accountId']) {
            if (typeof changes['accountId'].currentValue == 'string') {
                this.accountId = changes['accountId'].currentValue;
                // this.accountId = this.accountId == this.accountApi.getCurrentId() ? null : this.accountId;
            }else {
                this.accountId = null;
            }
        }
    }

    /**
     * @method getAccount
     **/
    getAccount(): void {
        this.accountApi.getCurrent().subscribe(
            (account: Account) => {
                this.account = account;
            },
            error => this.errorHandler(error)
        )
    }

    /**
     * @method getFriends
     **/
    getFriends(): void {
        let listStorage = this.storage.get('friendListStorage') || [];
        // let friendActualization = this.storage.get('friendActulization') || false;
        if (listStorage.length && (!this.accountId || this.accountId == this.accountApi.getCurrentId())) {
            this.friends = listStorage;
            console.log('FRIENDS FROM LS');
        }else {
            console.log('No FRIENDS FROM LS');
        }
        let id = this.accountApi.getCurrentId();
        this.subscriptions.push(
            this.friendApi.find({
                where: {
                    accountId: id,
                    approved: true
                },
                include: {
                    relation: 'friend',
                    scope: {
                        include: {
                            relation: 'visits',
                            scope: {
                                where: {
                                    validity: 100
                                },
                                limit: 1,
                                order: 'entry DESC',
                                fields: ['id','accountId','locationId', 'entry', 'exit'],
                                include: {
                                    location: {
                                        relation: 'establishment',
                                        scope: {
                                            fields: [
                                                'id',
                                                'name',
                                                'description',
                                                'address',
                                                'locationId'
                                            ],
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }).subscribe(
                (friends: Friend[]) => {
                    this.showList = true;
                    /*
                    if (this.accountId && this.accountId != this.accountApi.getCurrentId()) {
                        this.friends = friends;
                        console.log('FRIEND LIST FROM FRIEND');
                        return;
                    }
                    */
                    if (friends && friends.length !== this.friends.length) {
                        this.friends = friends;
                        this.storage.set('friendListStorage', this.friends);
                        console.log('FRIEND LIST UPDATE')
                    }else {
                        console.log('NO FRIEND LIST UPDATE')
                        this.storage.set('friendListStorage', friends);
                    }
                },
                (err: Error) => {
                    console.log('Is this the invalid? ', JSON.stringify(err));
                    this.errorHandler(err);
                } 
            )
        );
    }

    /**
     * @method onItemTap
     **/
    onItemTap(account: Account): void {
        this.showList = false;
        if (this.accountId) {
            // this.show = false;
            // this.storage.set('showFriendsID', this.accountApi.getCurrentId())
            this.onClose.emit(true);
        }
        this.router.navigate(['/user-profile/' + account.id, {account: JSON.stringify(this.account)} ]);
    }

    goToAddFriends(): void {
        let id = this.accountApi.getCurrentId();
        this.showList = false;
        this.router.navigate([`/user-profile/${id}`, {showAddFriends: true, account: JSON.stringify(this.account)}])
    }

    /**
     * @method templateSelector
     **/
    templateSelector = (item: Friend, index: number, items: Friend[]) => {
        if(item && item.friend && item.friend.firstName) {
            return 'FRIEND';
        }
        return 'NO_ITEM';
    }

    /**
     * @method close
     **/
    close(): void {
        let gridLayout = <View>this.page.getViewById('friends-list');
        if(gridLayout) {
            gridLayout.animate({
                opacity: 0,
                translate: { x: 0, y: 600},
                duration:500
            }).then(() => {
                this.onClose.emit(true);
            });
        }
    }

    /**
     * @method ngOnDestroy
     **/
    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    errorHandler(error: any): void {
        console.log('Friend list error handler: ', JSON.stringify(error))
        if (error && error.statusCode === 401) {
            this.logoutService.logout();
        }
    }
}
